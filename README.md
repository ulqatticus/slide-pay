# slidepay_android


# Installation
Clone this repository and import into Android Studio

```
git clone https://github.com/stalko/slidepay_android.git
```
----

# Build variants
Use the Android Studio Build Variants button to choose between production and staging flavors combined with debug and release build types

----
# Generating signed APK
From Android Studio:

- Build menu
- Generate Signed APK...
- Fill in the keystore information (you only need to do this once manually and then let Android Studio remember it)
----
# Build menu
Generate Signed APK...
Fill in the keystore information (you only need to do this once manually and then let Android Studio remember it)