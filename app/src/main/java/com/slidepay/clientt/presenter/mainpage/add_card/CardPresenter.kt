package com.slidepay.clientt.presenter.mainpage.add_card

import android.os.Handler
import com.slidepay.clientt.application.utils.ConnectionHelper
import com.slidepay.clientt.contracts.main.addcard.CardContract
import com.slidepay.clientt.model.database.TokenDataBase
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.model.objects.main.RenameCardBody
import com.slidepay.clientt.model.service.ClientCardsService
import io.reactivex.observers.DisposableObserver
import retrofit2.Response

class CardPresenter(private var activity: CardContract.View) : CardContract.Presenter {


    private var currentRunnable: Runnable? = null
    private val handler: Handler = Handler()
    private var position = -1
    private var isUpdate = false


    override fun viewCreated() {
        activity.initRecyclerView()
        activity.initStateListener()

        getUserCards()

    }

    fun getUserCards() {

        currentRunnable = Runnable {
            ClientCardsService().getUserCards()?.subscribe(getAllCardsObserver())
        }

        if (!ConnectionHelper().isOnline()) {
            activity.showNoInternetConnection()
            return
        }

        handler.post(currentRunnable)
    }

    private fun getAllCardsObserver(): DisposableObserver<Response<ArrayList<CardBody>>> {
        return object : DisposableObserver<Response<ArrayList<CardBody>>>() {
            override fun onStart() {
                activity.showLoading()
            }

            override fun onNext(response: Response<ArrayList<CardBody>>) {

                response.let { it ->
                    when (it.code()) {
                        in 200..299 -> {
                            it.body()!!.add(CardBody(0, false, "", "", "", 2))
                            if (isUpdate) {
                                activity.updateItems(it.body()!!)
                                isUpdate = false
                            } else {
                                activity.addItems(it.body()!!)

                            }


                            activity.showContent()
                        }
                        in 400..599 -> {
                            activity.showSomethingWentWrong()
                        }
                    }

                }
            }

            override fun onError(error: Throwable) {
                error.printStackTrace()
                activity.showSomethingWentWrong()

            }

            override fun onComplete() {

            }
        }
    }

    override fun removeCard(id: Long, position: Int) {
        this.position = position
        if (!ConnectionHelper().isOnline()) {
            activity.showNoInternetConnection()
            return
        }
        activity.showLoading()
        ClientCardsService().deleteCard(id)?.subscribe(removeObservable())
    }


    private fun removeObservable(): DisposableObserver<Response<Void>> {
        return object : DisposableObserver<Response<Void>>() {
            override fun onStart() {}
            override fun onNext(response: Response<Void>) {
                response.let { it ->
                    when (it.code()) {
                        in 200..299 -> {
                            if (position >= 0) {
                                activity.removeCard(position)
                            }
                            activity.showContent()
                        }
                        in 400..599 -> {
                            activity.showSomethingWentWrong()
                        }
                    }


                }
            }

            override fun onError(error: Throwable) {
                error.printStackTrace()
                activity.showSomethingWentWrong()

            }

            override fun onComplete() {

            }
        }
    }

    override fun setDefaultCard(id: Long) {
        if (!ConnectionHelper().isOnline()) {
            activity.showNoInternetConnection()
            return
        }
        ClientCardsService().setDefaultCard(id)?.subscribe(getDefaultObservable())
    }


    override fun renameCard(id: Long, text: String?) {
        if (!ConnectionHelper().isOnline()) {
            activity.showNoInternetConnection()
            return
        }
        val renameBody = RenameCardBody(text)
        ClientCardsService().renameCard(id, renameBody)?.subscribe(getRenameObservable())
    }


    private fun getRenameObservable(): DisposableObserver<Response<Void>> {
        return object : DisposableObserver<Response<Void>>() {
            override fun onStart() {}
            override fun onNext(response: Response<Void>) {
                response.let { it ->
                    when (it.code()) {
                        in 400..599 -> {
                            activity.showSomethingWentWrong()
                        }
                    }
                }
            }

            override fun onError(error: Throwable) {
                error.printStackTrace()
                activity.showSomethingWentWrong()

            }

            override fun onComplete() {}
        }

    }


    private fun getDefaultObservable(): DisposableObserver<Response<Void>> {
        return object : DisposableObserver<Response<Void>>() {
            override fun onStart() {}
            override fun onNext(response: Response<Void>) {
                response.let { it ->
                    when (it.code()) {
                        in 200..299 -> {
                            isUpdate = true
                            getUserCards()

                        }
                        in 400..599 -> {
                            activity.showSomethingWentWrong()
                        }
                    }
                }
            }

            override fun onError(error: Throwable) {
                error.printStackTrace()
                activity.showSomethingWentWrong()

            }

            override fun onComplete() {
            }
        }

    }

    override fun retryBtnClick() {
        if (!ConnectionHelper().isOnline()) {
            return
        }
        handler.post(currentRunnable)
    }


    override fun createCard() {
        activity.createCard()
    }

    fun showKeyboard() {
        activity.showKeyboard()

    }

    fun hideKeyboard() {
        activity.hideKeyboard()
    }


    override fun viewDestroyed() {
        if (!getDefaultObservable().isDisposed) {
            getDefaultObservable().dispose()
        }
        if (!getRenameObservable().isDisposed) {
            getRenameObservable().dispose()
        }
        if (!removeObservable().isDisposed) {
            removeObservable().dispose()
        }
        if (!getAllCardsObserver().isDisposed) {
            getAllCardsObserver().dispose()
        }
    }

}