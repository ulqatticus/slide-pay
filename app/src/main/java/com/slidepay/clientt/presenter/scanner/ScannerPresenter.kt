package com.slidepay.clientt.presenter.scanner

import com.slidepay.clientt.contracts.main.scanner.ScannerContract


class ScannerPresenter(var view: ScannerContract.View) : ScannerContract.Presenter {

    override fun viewCreated() {
        view.run {
            initFlashLight()
            initCloseBtn()
        }
    }

    override fun getQrCode(value: String?) {
        value?.let {
            view.finishWithSuccessResult(it)
        }
    }

    override fun viewDestroyed() {}
}