package com.slidepay.clientt.presenter.start.registration.password

import android.os.Build
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.slidepay.clientt.application.LocalData
import com.slidepay.clientt.application.utils.ConnectionHelper
import com.slidepay.clientt.model.objects.main.PhoneNumberBody
import com.slidepay.clientt.model.service.ClientService
import com.slidepay.clientt.model.service.ClientTransactionService
import com.slidepay.clientt.presenter.start.registration.password.fingerprint_detection.FingerprintScanner
import com.slidepay.clientt.view.start_app.registration.password.PinCodeContract
import io.reactivex.observers.DisposableObserver
import retrofit2.Response


class PinCodeCheckPresenter(private var activity: PinCodeContract.View) : PinCodeContract.Presenter {

    private var currentRunnable: Runnable? = null
    private val handler: Handler = Handler()
    private var fingerScanner: FingerprintScanner? = null


    override fun viewIsReady() {
        activity.setLabelText("Введите ПИН-код")
        activity.initStateListener()

        activity.setListenerToEditText()
        activity.showForgotPass()
        initFingerPrintScanner()
    }

    private fun initFingerPrintScanner() {
        if (fingerScanner == null) {
            fingerScanner = FingerprintScanner(this)
        }
    }


    override fun next(password: String) {
        if (password.equals(LocalData.pinCode)) {
            activity.startNextActivity()
        } else {
            activity.showError("Введен неверный ПИН-код")
        }
    }

    override fun onResume() {
        initFingerPrintScanner()
        /* if (fingerScanner!!.isDialogIsShowing()) {
             if (fingerScanner == null) {
                 fingerScanner = FingerprintScanner(this)
             } else {
                 fingerScanner!!.initScanner()
             }
         }*/
    }


    override fun forgotPassClick() {
        currentRunnable = Runnable {
            ClientService().restorePass()?.subscribe(getDefaultObservable())
        }

        if (!ConnectionHelper().isOnline()) {
            activity.showNoInternetConnection()
            return
        }

        handler.post(currentRunnable)
    }

    private fun getDefaultObservable(): DisposableObserver<Response<PhoneNumberBody>> {
        return object : DisposableObserver<Response<PhoneNumberBody>>() {
            override fun onStart() {
                activity.showLoading()
            }

            override fun onNext(response: Response<PhoneNumberBody>) {

                response.let { it ->
                    when (it.code()) {
                        in 200..299 -> {
                            activity.hideKeyBoard()
                            activity.startConfirmCodeForRestorePass(it.body()?.phone!!)
                        }
                        in 400..599 -> {
                            activity.showSomethingWentWrong()
                        }
                    }

                }
            }

            override fun onError(error: Throwable) {
                activity.showSomethingWentWrong()
                error.printStackTrace()

            }

            override fun onComplete() {
                activity.hideLoading()

            }
        }
    }
    /*override fun onTextFirst() {
        view.let {
            if (it.textFirst.equals(LocalData.pinCode)) {
                nextAction()

            } else {
                it.showValidationError(R.string.wrong_pass)
                it.clearTextInEditText()
            }
        }
    }*/

    override fun startScanFingerPrint() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerScanner!!.startListening()
        }
    }

    fun nextAction() {
        activity.hideKeyBoard()
        activity.startNextActivity()
    }


    override fun onStop() {
        fingerScanner?.closeDialog()
    }

    fun getActivity(): AppCompatActivity {
        return activity.getActivity()
    }

    fun showFingerPrintBtn() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.showDivider()
            activity.showFingerPrintBtn()
        }
    }

    override fun onBackPressed() {
        activity.closeActivity()

    }

    fun showKeyboard() {
        activity.showKeyBoard()
    }


}
