package com.slidepay.clientt.presenter.mainpage.qrcode

import android.graphics.Bitmap
import android.os.Handler
import android.support.v4.util.LruCache
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.slidepay.clientt.application.LocalData
import com.slidepay.clientt.application.utils.ConnectionHelper
import com.slidepay.clientt.contracts.main.qrcode.QrCodeContract
import com.slidepay.clientt.model.objects.main.ProfileBody
import com.slidepay.clientt.model.objects.main.ScannedBody
import com.slidepay.clientt.model.service.ClientProfileService
import com.slidepay.clientt.view.scanner.ScannerFragment
import io.reactivex.observers.DisposableObserver
import retrofit2.Response
import java.util.*


class QrCodePresenter(private var activity: QrCodeContract.View) : QrCodeContract.Presenter {


    private var currentRunnable: Runnable? = null
    private val handler: Handler = Handler()
    private var contentForQrCode: String? = null
    private lateinit var mMemoryCache: LruCache<String, Bitmap>

    //region Start
    init {
        initMemoryCash()
    }

    private fun initMemoryCash() {
        val maxMemory = Runtime.getRuntime().maxMemory().toInt()
        val cacheSize = maxMemory / 8
        mMemoryCache = object : LruCache<String, Bitmap>(cacheSize) {
            override fun sizeOf(key: String, value: Bitmap): Int {
                return value.byteCount / 1024
            }

        }
    }

    override fun viewCreated() {
        activity.initStateListener()

        contentForQrCode = LocalData.qrContent
        if (contentForQrCode.isNullOrEmpty()) {
            currentRunnable = Runnable {
                ClientProfileService().getInfo()?.subscribe(getObserver())
            }
            if (!ConnectionHelper().isOnline()) {
                activity.showNoInternetConnection()
                return
            }
            handler.post(currentRunnable)
        } else {
            Handler().postDelayed({
                activity.generateQRImage()
            }, 50)
        }
    }
    //endregion

    //region response from server
    private fun getObserver(): DisposableObserver<Response<ProfileBody>> {
        return object : DisposableObserver<Response<ProfileBody>>() {
            override fun onStart() {
                activity.showLoading()
            }

            override fun onNext(response: Response<ProfileBody>) {
                response.let { it ->

                    when (it.code()) {
                        in 200..299 -> {
                            contentForQrCode = response.body()!!.code!!
                            LocalData.qrContent = contentForQrCode!!
                            activity.generateQRImage()
                            activity.showContent()

                        }
                        in 400..599 -> {
                            activity.showSomethingWentWrong()
                        }
                    }
                }
            }

            override fun onError(error: Throwable) {
                error.printStackTrace()
                activity.showSomethingWentWrong()


            }

            override fun onComplete() {
            }
        }
    }

    override fun retryBtnClick() {
        if (!ConnectionHelper().isOnline()) {
            return
        }
        handler.post(currentRunnable)
    }

    //endregion

    //region work with bitmap and cache
    override fun createQrBitmapImg(): Bitmap? {

        val bitmap = getBitmapFromMemCache(bitmapKey)
        if (bitmap == null) {
            var qrBitmap: Bitmap? = null

            val multiFormatWriter = MultiFormatWriter()

            val w = 200
            val h = 200
            val hints = EnumMap<EncodeHintType, Any>(EncodeHintType::class.java)
            hints[EncodeHintType.MARGIN] = 0 /* default = 4 */

            try {
                val bitMatrix = multiFormatWriter.encode(contentForQrCode, BarcodeFormat.QR_CODE, w, h, hints)
                val barcodeEncoder = BarcodeEncoder()
                qrBitmap = barcodeEncoder.createBitmap(bitMatrix)
                addBitmapToMemoryCache(bitmapKey, qrBitmap)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return qrBitmap
        }
        return bitmap
    }

    private fun addBitmapToMemoryCache(key: String, bitmap: Bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap)
        }
    }




    private fun getBitmapFromMemCache(key: String): Bitmap? {
        return mMemoryCache.get(key)
    }

    // endregion
    override fun viewDestroyed() {

    }




    companion object {
        const val bitmapKey = "QR_CODE"
    }

}