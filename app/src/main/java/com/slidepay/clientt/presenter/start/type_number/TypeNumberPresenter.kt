package com.slidepay.clientt.presenter.start.type_number

import android.os.Handler
import android.support.v4.content.ContextCompat
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import com.google.firebase.iid.FirebaseInstanceId
import com.slidepay.clientt.R
import com.slidepay.clientt.application.utils.ConnectionHelper
import com.slidepay.clientt.contracts.start.TypeNumberContract
import com.slidepay.clientt.model.database.TokenDataBase
import com.slidepay.clientt.model.objects.UserToken
import com.slidepay.clientt.model.objects.registration.UserLoginBody
import com.slidepay.clientt.model.service.ClientService
import io.reactivex.observers.DisposableObserver
import retrofit2.Response
import java.util.regex.Pattern


class TypeNumberPresenter(private var activity: TypeNumberContract.View) : TypeNumberContract.Presenter {


    private var currentRunnable: Runnable? = null
    private val handler: Handler = Handler()
    override fun startWebFragment() {
        activity.startWebFragment()
    }

    internal var isClick: Boolean = false
    private val phonePattern = Pattern.compile("[0-9]{9}")


    override fun viewCreated() {
        activity.initStateListener()

        activity.showKeyBoard()
    }


    override fun watchOnTextView(text: String) {
        if (validateInputs(text)) {
            showError(false)
        }
    }

    override fun settingNumberField(event: KeyEvent?, actionId: Int) {
        if ((event != null && (event.keyCode == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
            clickNextBtn()
        }
    }

    override fun clickNextBtn() {
        val text = activity.getNumber()
        if (validateInputs(text)) {
            if (activity.checkBoxIsChecked()) {
                sendSignIn()
            } else {
                activity.checkBoxShowError()
            }

        } else {
            activity.showErrorPhone()
        }
    }

    private fun sendSignIn() {
        currentRunnable = Runnable {
            ClientService().login(getUserLoginBody())?.subscribe(getObserver())
        }

        if (!ConnectionHelper().isOnline()) {
            activity.showNoInternetConnection()
            return
        }

        handler.post(currentRunnable)

    }

    override fun retryBtnClick() {
        if (!ConnectionHelper().isOnline()) {
            return
        }
        handler.post(currentRunnable)
    }

    private fun getObserver(): DisposableObserver<Response<UserToken>> {
        return object : DisposableObserver<Response<UserToken>>() {
            override fun onStart() {
                activity.hideKeyBoard()
                activity.showLoading()
            }

            override fun onNext(response: Response<UserToken>) {
                response.let { it ->

                    when (it.code()) {
                        in 200..299 -> {
                            TokenDataBase.setClientToken(response.body()!!)
                            activity.nextActivity()
                        }
                        in 400..599 -> {
                            activity.showSomethingWentWrong()
                        }
                    }

                }
            }

            override fun onError(error: Throwable) {
                activity.showSomethingWentWrong()

            }

            override fun onComplete() {
                activity.hideLoading()

            }
        }
    }


    override fun getSpanString(): SpannableString {
        val spannableString = SpannableString(activity.getContext().getString(R.string.type_number_activity_span_text))

        val color = ForegroundColorSpan(ContextCompat.getColor(activity.getContext(), R.color.blue))
        spannableString.setSpan(TermsClickableSpan(this), 33, spannableString.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannableString.setSpan(color, 33, spannableString.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        return spannableString
    }

    private fun getUserLoginBody(): UserLoginBody {
        val loginBody = UserLoginBody()
        loginBody.phone = "380" + activity.getNumber()
        loginBody.device = FirebaseInstanceId.getInstance().token
        loginBody.os = getOs()
        return loginBody
    }


    private fun getOs(): String {
        return "Android"
    }

    private fun validateInputs(phone: String): Boolean {
        if (!phonePattern.matcher(phone).matches()) {
            showError(true)
            return false
        }
        return true
    }


    private fun showError(isShow: Boolean) {
        when (isShow) {
            true -> activity.showErrorPhone()
            false -> activity.hideErrorPhone()
        }

    }

    override fun viewDestroyed() {
    }

    fun getCorrectPhoneNumber(): String {
        val stringBuilder = StringBuilder("+380")
        stringBuilder.append(activity.getNumber())
        return stringBuilder.toString()
    }

}