package com.slidepay.clientt.presenter.invoice.bill

import android.graphics.Bitmap
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.slidepay.clientt.contracts.invoice.BillContract
import com.slidepay.clientt.model.objects.main.InvoiceBodyResponse
import java.util.*


class BillPresenter(var activity: BillContract.View) : BillContract.Presenter {


    override fun viewDestroyed() {

    }



    override fun viewCreated(invoiceBody: InvoiceBodyResponse) {
        activity.setDescription(invoiceBody.description)
        activity.setQrImage(getQRImage(invoiceBody.code))
        activity.setName(invoiceBody.payerFullName)
        activity.setSum(invoiceBody.sum)
        activity.initCloseBtn()
    }

    private fun getQRImage(code: String?): Bitmap? {

        var qrBitmap: Bitmap? = null

        val multiFormatWriter = MultiFormatWriter()

        val w = 200
        val h = 200
        val hints = EnumMap<EncodeHintType, Any>(EncodeHintType::class.java)
        hints[EncodeHintType.MARGIN] = 0

        try {
            val bitMatrix = multiFormatWriter.encode(code, BarcodeFormat.QR_CODE, w, h, hints)
            val barcodeEncoder = BarcodeEncoder()
            qrBitmap = barcodeEncoder.createBitmap(bitMatrix)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return qrBitmap

    }

    override fun viewCreated() {


    }

}