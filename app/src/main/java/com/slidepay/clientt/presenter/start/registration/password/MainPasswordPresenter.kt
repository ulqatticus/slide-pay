package com.slidepay.clientt.presenter.start.registration.password

import com.slidepay.clientt.view.start_app.registration.password.PinCodeContract

class MainPasswordPresenter(activity: PinCodeContract.View) {

/*
    fun showFirst(text: String) {
        // set text to label and show it
        textViewFirstLabel.text = text
        textViewFirstLabel.visibility = View.VISIBLE

        // show field
        editTextFirstValue.visibility = View.VISIBLE

        // set textChange listener
        RxTextView.afterTextChangeEvents(editTextFirstValue)
                .skipInitialValue()
                .filter { textViewAfterTextChangeEvent -> textViewAfterTextChangeEvent.editable()!!.toString().length == 4 }
                .subscribe { presenter?.onTextFirst() }
    }

    fun showSecond(text: String) {
        textViewSecondLabel.text = text
        textViewSecondLabel.visibility = View.VISIBLE
        editTextSecondValue.visibility = View.VISIBLE
        RxTextView.afterTextChangeEvents(editTextSecondValue)
                .skipInitialValue()
                .filter { textViewAfterTextChangeEvent -> textViewAfterTextChangeEvent.editable()!!.toString().length == 4 }
                .subscribe { presenter?.onTextSecond() }
    }

    fun showThird(text: String) {
        textViewThirdLabel.text = text
        textViewThirdLabel.visibility = View.VISIBLE
        editTextThirdValue.visibility = View.VISIBLE
        RxTextView.afterTextChangeEvents(editTextThirdValue)
                .skipInitialValue()
                .filter { textViewAfterTextChangeEvent -> textViewAfterTextChangeEvent.editable()!!.toString().length == 4 }
                .subscribe { presenter?.onTextThird() }

    }

    fun clearAll() {
        editTextFirstValue.setText("")
        editTextSecondValue.setText("")
        editTextThirdValue.setText("")
    }

    fun focusFirst() {
        editTextFirstValue.requestFocus()
        editTextFirstValue.isFocusableInTouchMode = true
        editTextFirstValue.isFocusable = true
    }

    fun focusSecond() {
        editTextSecondValue.requestFocus()
    }

    fun focusThird() {
        editTextThirdValue.requestFocus()


    }

    fun next() {
        LocalData.isAuthorizationIsEnded = true

        view.startActivity(Intent(view, PageActivity::class.java))


        view.finish()
    }

    fun textFirst(): String {
        return editTextFirstValue.text.toString()
    }

    fun textSecond(): String {
        return editTextSecondValue.text.toString()
    }

    fun textThird(): String {
        return editTextThirdValue.text.toString()
    }

    fun showErrorMessage(res: Int) {
        Toast.makeText(view, res, Toast.LENGTH_SHORT).show()
    }


    fun onResume() {
        presenter?.onResume()
        //showKeyboard()
    }

    fun showFingerPrintBtn() {
        presenter?.showFingerPrintBtn(fingerPrintBtn)


    }

    fun clearFirstFocus() {
        editTextFirstValue.isFocusableInTouchMode = false
        editTextFirstValue.isFocusable = false

    }*/


}