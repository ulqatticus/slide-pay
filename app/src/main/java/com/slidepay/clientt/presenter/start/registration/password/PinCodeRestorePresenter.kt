package com.slidepay.clientt.presenter.start.registration.password

import android.os.Handler
import com.slidepay.clientt.application.LocalData
import com.slidepay.clientt.view.start_app.registration.password.PasswordActivity
import com.slidepay.clientt.view.start_app.registration.password.PinCodeContract

class PinCodeRestorePresenter(var activity: PinCodeContract.View) : PinCodeContract.Presenter {


    private var doubleBackToExitPressedOnce = false


    private var isNextActivity = false
    private lateinit var pass: String
    private lateinit var passRepeat: String

    override fun onResume() {}
    override fun startScanFingerPrint() {}
    override fun forgotPassClick() {}


    override fun viewIsReady() {
        activity.showKeyBoard()
        activity.hideForgotPass()
        activity.setListenerToEditText()
        activity.setLabelText("Установите новый ПИН-код")
    }


    override fun next(password: String) {
        activity.hideKeyBoard()
        if (!isNextActivity) {
            activity.clearTextInEditText()
            activity.setLabelText("Подтвердите новый ПИН-код")
            isNextActivity = true
            pass = password
        } else {
            passRepeat = password
            if (isValidate()) {
                LocalData.pinCode = password

                activity.finishActivity()
            } else {
                activity.showError("Указанные пароли не совпадают")
            }
        }

    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            activity.closeActivity()
            return
        } else {
            activity.setLabelText("Установите новый ПИН-код")
            activity.clearTextInEditText()
            pass = ""
            isNextActivity = false
        }
        doubleBackToExitPressedOnce = true
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)


    }

    override fun onStop() {}

    private fun isValidate(): Boolean {
        if (pass.equals(passRepeat)) {
            return true
        }
        return false
    }

}

