package com.slidepay.clientt.presenter.invoice

import com.slidepay.clientt.application.utils.ConnectionHelper
import com.slidepay.clientt.contracts.sendmoney.SendMoneyContract
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.model.objects.main.PaymentBody
import com.slidepay.clientt.model.objects.main.ScannedBody
import com.slidepay.clientt.model.service.ClientCardsService
import com.slidepay.clientt.model.service.ClientSendMoneyService
import com.slidepay.clientt.presenter.sendmoney.SendMoneyType
import io.reactivex.observers.DisposableObserver
import retrofit2.Response


class SendMoneyPresenter(var view: SendMoneyContract.View) : SendMoneyContract.Presenter {

    override fun sendMoney(sendMoneyType: SendMoneyType) {

        val body = createBody(sendMoneyType)

        if (isValid(body)) {
            when (sendMoneyType) {
                SendMoneyType.CREATE -> createSendMoney(body)
                SendMoneyType.PAY -> paySendMoney(body)
                SendMoneyType.FROM_TRANSACTION -> payFromTransaction(body)
            }
            if (!ConnectionHelper().isOnline()) {
                view.showNoInternetConnection()
                return
            }
        }

    }


    private fun createSendMoney(body: PaymentBody) {
        ClientSendMoneyService().сreate(body)?.subscribe(createPaymentObservable())

    }


    private fun paySendMoney(body: PaymentBody) {
        ClientSendMoneyService().pay(body)?.subscribe(createPaymentObservable())
    }


    private fun payFromTransaction(body: PaymentBody) {
        ClientSendMoneyService().updatePayment(body)?.subscribe(createPaymentObservable())
    }


    private

    fun isValid(body: PaymentBody): Boolean {
        if (body.sum == null) {
            view.showError()
            return false
        }
        if (body.sum!! <= 0) {
            view.showError()
            return false
        }
        if (body.cardId == null) {
            return false
        }

        if (body.recipientNumber.isNullOrBlank()) {
            return false
        }
        return true
    }

    private fun createBody(sendMoneyType: SendMoneyType): PaymentBody {

        val body = PaymentBody()
        view.let {
            val amount = it.getAmount()
            body.sum = if (amount.isNullOrBlank()) null else amount.toDouble()
            body.cardId = it.getCardId()
            if (sendMoneyType == SendMoneyType.PAY) body.billNumber = it.getBillNumber()
            body.description = it.getDescription()
            if (sendMoneyType == SendMoneyType.CREATE) body.recipientNumber = it.getRecipientNumber()
            if (sendMoneyType == SendMoneyType.FROM_TRANSACTION) body.transactionNumber = it.getBillNumber()
        }

        return body
    }


    private fun createPaymentObservable(): DisposableObserver<Response<Void>> {
        return object : DisposableObserver<Response<Void>>() {
            override fun onStart() {
                view.showLoading()

            }

            override fun onNext(response: Response<Void>) {

                response.let { it ->
                    when (it.code()) {
                        in 200..299 -> {
                            view.showContent()
                            view.startSuccessFragment()
                            // view.closeFragment()
                        }
                        in 400..599 -> {
                            view.showSomethingWentWrong()
                        }
                    }

                }
            }

            override fun onError(error: Throwable) {
                error.printStackTrace()
                view.showSomethingWentWrong()


            }

            override fun onComplete() {
                view.hideLoading()

            }
        }
    }


    override fun viewCreated() {
        view.initStateListener()

    }

    override fun viewCreated(body: ScannedBody?) {
        view.initViews(body)
        view.initMultiStateListener()

        getUserCardsCard()
    }

    private fun getUserCardsCard() {
        if (!ConnectionHelper().isOnline()) {
            return
        }
        ClientCardsService().getUserCards()?.subscribe(getAllCardsObserver())

    }


    private fun getAllCardsObserver(): DisposableObserver<Response<ArrayList<CardBody>>> {
        return object : DisposableObserver<Response<ArrayList<CardBody>>>() {
            override fun onStart() {
                view.showLoading()
            }

            override fun onNext(response: Response<ArrayList<CardBody>>) {

                response.let { it ->
                    when (it.code()) {
                        in 200..299 -> {
                            if (it.body()!!.isNotEmpty())
                                view.initUserCard(it.body()!!)

                        }
                        in 400..599 -> {
                            view.showSomethingWentWrong()
                        }
                    }

                }
            }

            override fun onError(error: Throwable) {
                view.showSomethingWentWrong()
            }

            override fun onComplete() {
                view.hideLoading()

            }
        }
    }

    override fun viewDestroyed() {
    }


}