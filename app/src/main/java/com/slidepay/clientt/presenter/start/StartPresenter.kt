package com.slidepay.clientt.presenter.start

import android.content.Intent
import com.slidepay.clientt.application.LocalData
import com.slidepay.clientt.model.database.TokenDataBase
import com.slidepay.clientt.view.BaseActivity
import com.slidepay.clientt.view.start_app.onboarding.OnBoardingActivity
import com.slidepay.clientt.view.start_app.registration.password.PasswordActivity

class StartPresenter(var activity: BaseActivity)  {


    init {
        activity.hideActionBar()
        startCheckToken()
    }


    /*  companion object {
          const val time_splash_animation: Long = 2500
      }*/


    /**
     * check token on start of game
     *  - if we have then start Map
     *   else start Auth
     *   @see checkToken
     */
    private fun startCheckToken() {
        /*       Handler().postDelayed({
                   checkToken()
               }, time_splash_animation)*/
        checkToken()
    }


    private fun checkToken() {
        val token = TokenDataBase.getUserTokenToken()
        if (token != null && !token.token?.isEmpty()!! && LocalData.isAuthorizationIsEnded) {
            PasswordActivity.checkPinCode(activity)
        } else {
            activity.startActivity(Intent(activity, OnBoardingActivity::class.java))
        }
        activity.finish()
    }
}