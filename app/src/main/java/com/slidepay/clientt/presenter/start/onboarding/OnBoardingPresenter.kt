package com.slidepay.clientt.presenter.start.onboarding

import com.slidepay.clientt.application.LocalData
import com.slidepay.clientt.contracts.start.OnBoardingContract
import com.slidepay.clientt.view.start_app.onboarding.OnBoardingActivity
import com.slidepay.clientt.view.start_app.onboarding.OnBoardingBenefit
import java.util.*

class OnBoardingPresenter(private var activity: OnBoardingContract.View) : OnBoardingContract.Presenter {


    private var mCurrentItem = 0

    val benefits: List<OnBoardingBenefit>
        get() = object : ArrayList<OnBoardingBenefit>() {
            init {
                add(OnBoardingBenefit.ADD_CARD)
                add(OnBoardingBenefit.ADD_QR_CODE)
                add(OnBoardingBenefit.ADD_INVOICE)
                add(OnBoardingBenefit.ADD_EMPTY_PAGE)
            }
        }

    private val isLastBenefit: Boolean
        get() = mCurrentItem == OnBoardingActivity.PAGES_COUNT - 1

    fun btnClick() {
        onBoardingIsFinish()
    }

    private fun onBoardingIsFinish() {
        LocalData.isWalkthroughPassed = true
        activity.nextActivity()
    }

    override fun showBenefit(selectedPage: Int) {
        mCurrentItem = selectedPage

        if (selectedPage == 0) {
            activity.showSkipBtn()
        } else {
            activity.hideSkipBtn()

        }
        if (isLastBenefit) {
            onBoardingIsFinish()

            return
        }
        if (mCurrentItem == activity.getCurrentItemPager()) {
            return
        }
        activity.showBenefit()
    }


    override fun viewDestroyed() {

    }

    override fun viewCreated() {
        if (LocalData.isWalkthroughPassed) {
            activity.nextActivity()
        }
    }
}