package com.slidepay.clientt.presenter.start.registration.password.fingerprint_detection

import android.Manifest
import android.content.pm.PackageManager
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.CancellationSignal
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import com.slidepay.clientt.application.utils.Config


@RequiresApi(Build.VERSION_CODES.M)
class FingerprintListener(private val scanner: FingerprintScanner) : FingerprintManager.AuthenticationCallback() {
    private var cancellationSignal: CancellationSignal? = null
    private var fingerprintManager: FingerprintManager? = null


    fun startAuthentication(fingerprintManager: FingerprintManager, cryptoObject: FingerprintManager.CryptoObject) {
        this.fingerprintManager = fingerprintManager
        cancellationSignal = CancellationSignal()
        if (ActivityCompat.checkSelfPermission(scanner.presenter.getActivity(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED)
            return
        fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null)

    }

    override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
        if (errorCode == Config.ErrorCode.LIMIT_PASS_ENTER_TRYING) {
            scanner.closeDialog()
            scanner.presenter.showKeyboard()
        }
    }

    override fun onAuthenticationFailed() {
        super.onAuthenticationFailed()
        scanner.showErrorFingerPrintText()
    }

    override fun onAuthenticationSucceeded(result: FingerprintManager.AuthenticationResult) {
        super.onAuthenticationSucceeded(result)
        scanner.nextAction()
    }


    fun stopFingerAuth() {
        if (cancellationSignal != null && !cancellationSignal!!.isCanceled) {
            cancellationSignal!!.cancel()
            cancellationSignal = null
        }
    }


    private fun isFingerprintAuthAvailable(): Boolean {
        // The line below prevents the false positive inspection from Android Studio

        return fingerprintManager!!.isHardwareDetected && fingerprintManager!!.hasEnrolledFingerprints()
    }

    fun startListening(cryptoObject: FingerprintManager.CryptoObject) {
        if (!isFingerprintAuthAvailable()) {
            return
        }
        cancellationSignal = CancellationSignal()

        fingerprintManager!!.authenticate(cryptoObject, cancellationSignal, 0, this, null)

    }

}