package com.slidepay.clientt.presenter.mainpage.profile

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.os.Handler
import android.os.Parcelable
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.widget.Toast
import com.slidepay.clientt.application.utils.Config
import com.slidepay.clientt.application.utils.ConnectionHelper
import com.slidepay.clientt.contracts.main.profile.ProfileContract
import com.slidepay.clientt.model.objects.main.ProfileBody
import com.slidepay.clientt.model.service.ClientProfileService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.File
import java.io.IOException


class ProfilePresenter(private var activity: ProfileContract.View) : ProfileContract.Presenter {

    //TODO fix image sent
    //TODO make multi part
    private var fileToUpload: MultipartBody.Part? = null
    private var currentRunnable: Runnable? = null
    private val handler: Handler = Handler()
    override fun viewCreated() {
        activity.setListenerToLogout()
        activity.initStateListener()
        getProfileInfo()
    }

    private fun getProfileInfo() {

        currentRunnable = Runnable {
            ClientProfileService().getInfo()?.subscribe(getProfileObserver())
        }
        if (!ConnectionHelper().isOnline()) {
            activity.showNoInternetConnection()
            return
        }
        handler.post(currentRunnable)

    }


    private fun getProfileObserver(): DisposableObserver<Response<ProfileBody>> {
        return object : DisposableObserver<Response<ProfileBody>>() {
            override fun onStart() {
                activity.showLoading()
            }

            override fun onNext(response: Response<ProfileBody>) {

                response.let { it ->

                    when (it.code()) {
                        in 200..299 -> {
                            activity.setInformation(response.body()!!)
                            activity.showContent()
                        }
                        in 400..599 -> {
                            activity.showSomethingWentWrong()
                        }
                    }

                }
            }

            override fun onError(error: Throwable) {
                error.printStackTrace()
                activity.showSomethingWentWrong()


            }

            override fun onComplete() {

            }
        }
    }





    override fun addImageVia() {
        val file = File(Environment.getExternalStorageDirectory(), "photo.jpg")
        val uri = FileProvider.getUriForFile(activity.getActivityContext(), activity.getActivityContext().applicationContext.packageName + ".provider", file)

        val cameraIntents = ArrayList<Intent>()
        val captureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val packageManager = activity.getActivityContext().packageManager
        val listCam = packageManager.queryIntentActivities(captureIntent, 0)
        for (res in listCam) {
            val intent = Intent(captureIntent)
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri)
            intent.putExtra("return-data", true)
            cameraIntents.add(intent)
        }

        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        galleryIntent.type = "image/*"
        val chooserIntent = Intent.createChooser(galleryIntent, "Выбрать ресурс")
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(arrayOfNulls<Parcelable>(cameraIntents.size)))

        activity.getActivityContext().startActivityForResult(chooserIntent, Config.RequestCode.LOAD_IMG_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            Config.RequestCode.LOAD_IMG_REQUEST_CODE -> if (resultCode == Activity.RESULT_OK) {
                val uri = data?.data
                if (uri != null) {
                    val filePath = getRealPathFromURIPath(uri, activity.getActivityContext())
                    val file = File(filePath)
                    val mFile = RequestBody.create(MediaType.parse("image/*"), file)
                    fileToUpload = MultipartBody.Part.createFormData("image", file.name, mFile)

                    try {
                        val bitmap = MediaStore.Images.Media.getBitmap(activity.getActivityContext().contentResolver, uri)
                        activity.setImageToProfile(bitmap)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
                else {
                    Toast.makeText(activity.getActivityContext(), "Was error", Toast.LENGTH_LONG).show()
                }
            }
        }
    }


    private fun getRealPathFromURIPath(contentURI: Uri, activity: Activity): String {
        val cursor = activity.contentResolver.query(contentURI, null, null, null, null)
        return if (cursor == null) {
            contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            cursor.getString(idx)
        }
    }

    override fun saveChangesToServer(body: ProfileBody) {
        if (!ConnectionHelper().isOnline()) {
            activity.showNoInternetConnection()
            return
        }
        if (fileToUpload == null) {
            ClientProfileService().editProfile(body)?.subscribe(getUpdateProfileObservable())
        } else {
            Observable.merge(ClientProfileService().editProfile(body),
                    ClientProfileService().setImageProfile(fileToUpload))
                    .subscribe(getUpdateProfileObservable())
        }
    }


    override fun retryBtnClick() {
        if (!ConnectionHelper().isOnline()) {
            return
        }
        handler.post(currentRunnable)
    }

    private fun getUpdateProfileObservable(): DisposableObserver<Response<Void>> {
        return object : DisposableObserver<Response<Void>>() {
            override fun onStart() {
                activity.showLoading()
            }

            override fun onNext(response: Response<Void>) {

                response.let { it ->
                    when (it.code()) {
                        in 400..599 -> {
                            activity.showSomethingWentWrong()
                        }
                        in 200..399 -> {
                            activity.showContent()
                        }
                    }
                }
            }

            override fun onError(error: Throwable) {
                activity.showSomethingWentWrong()
                error.printStackTrace()

            }

            override fun onComplete() {

            }
        }
    }

    override fun viewDestroyed() {


    }

    fun showKeyboard() {
        activity.showKeyboard()

    }

    fun hideKeyboard() {
        activity.hideKeyboard()
    }


}