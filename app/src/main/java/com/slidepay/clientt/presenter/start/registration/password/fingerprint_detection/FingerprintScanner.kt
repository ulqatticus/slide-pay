package com.slidepay.clientt.presenter.start.registration.password.fingerprint_detection

import android.app.Dialog
import android.app.KeyguardManager
import android.content.Context
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.support.annotation.RequiresApi
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.slidepay.clientt.R
import com.slidepay.clientt.presenter.start.registration.password.PinCodeCheckPresenter
import com.slidepay.clientt.view.widgets.TextTypeface
import java.io.IOException
import java.security.*
import java.security.cert.CertificateException
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.NoSuchPaddingException
import javax.crypto.SecretKey


class FingerprintScanner(var presenter: PinCodeCheckPresenter) {
    private var keyStore: KeyStore? = null
    private val KEY_NAME = "EDMTDev"
    private var cipher: Cipher? = null
    private var dialog: Dialog? = null
    private var fingerPrintListener: FingerprintListener? = null
    private var cryptoObject: FingerprintManager.CryptoObject? = null

    private var fingerText: TextTypeface? = null
    private lateinit var fingerImg: ImageView
    /// private var simpleBlock: LinearLayout? = null
    private var cancelButton: TextTypeface? = null

    init {
        initScanner()
    }

    private fun initScanner() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val fingerprintManager = presenter.getActivity().getSystemService(Context.FINGERPRINT_SERVICE) as FingerprintManager
            if (!fingerprintManager.isHardwareDetected) {
                presenter.showKeyboard()

            } else {
                if (!fingerprintManager.hasEnrolledFingerprints()) {
                    presenter.showKeyboard()
                } else {
                    presenter.showFingerPrintBtn()
                    val keyguardManager = presenter.getActivity().getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
                    if (!keyguardManager.isKeyguardSecure) {
                        Toast.makeText(presenter.getActivity(), "Lock screen security not enabled in Settings", Toast.LENGTH_SHORT).show()
                    } else {
                        genKey()
                    }

                    if (cipherInit()) {
                        showFingerprintDialog()
                        fingerPrintListener = FingerprintListener(this)
                        cryptoObject = FingerprintManager.CryptoObject(cipher)
                        fingerPrintListener!!.startAuthentication(fingerprintManager, cryptoObject!!)

                    }
                }
            }
        }
    }


    private fun showFingerprintDialog() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            dialog = Dialog(presenter.getActivity(), R.style.MyDialogTheme)
            dialog!!.let { dialog ->
                dialog.setContentView(getPromptView())
                dialog.setCancelable(false)
                dialog.show()
            }

        }

    }


    private fun getPromptView(): View {
        val layoutInflater = LayoutInflater.from(presenter.getActivity())
        val promptView = layoutInflater.inflate(R.layout.finger_print_dialog, null)

        cancelButton = promptView.findViewById(R.id.fingerprint_cancel_btn) as TextTypeface
        cancelButton?.setOnClickListener {
            closeDialog()
            presenter.showKeyboard()
        }

        // simpleBlock = promptView.findViewById(R.id.fingerprint_simple_block)

        fingerText = promptView.findViewById(R.id.fingerprintdialog_text)
        fingerImg = promptView.findViewById(R.id.fingerprint_img)
        return promptView
    }


    fun closeDialog() {
        if (dialog != null) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    fingerPrintListener!!.stopFingerAuth()
                }
            }
        }
    }


    fun showErrorFingerPrintText() {
        if (dialog!!.isShowing) {
            changeImgAndText(R.drawable.ic_finger_print_err, "Ошибка при распознавании")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                fingerPrintListener?.stopFingerAuth()
            }
            cancelButton?.text = "Продолжить"
            /* Handler().postDelayed({
                 changeImgAndText(R.drawable.ic_fingerprint, "Приложите палец к сканеру для \nввода пин-кода")
                 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                     fingerPrintListener?.startListening(cryptoObject!!)
                 }

             }, 1700)*/


        }
    }

    fun nextAction() {
        if (dialog!!.isShowing) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                fingerPrintListener!!.stopFingerAuth()
            }
            //  changeImgAndText(R.drawable.done_white, "Отпечаток пальца распознан")
            presenter.nextAction()

            /*     simpleBlock!!.postDelayed({
                     closeDialog()
                     presenter.nextAction()
                 }, 1000)*/
        }

    }

    private fun changeImgAndText(imgRes: Int, text: String) {
        Glide.with(presenter.getActivity()).load(imgRes).into(fingerImg)
        fingerText?.text = text
    }

    private fun cipherInit(): Boolean {

        try {
            if (cipher == null) {
                cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7)
            }
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        }

        try {
            keyStore?.load(null)
            val key = keyStore?.getKey(KEY_NAME, null) as SecretKey
            cipher?.init(Cipher.ENCRYPT_MODE, key)
            return true


        } catch (e1: IOException) {

            e1.printStackTrace()
            return false
        } catch (e1: NoSuchAlgorithmException) {

            e1.printStackTrace()
            return false
        } catch (e1: CertificateException) {

            e1.printStackTrace()
            return false
        } catch (e1: UnrecoverableKeyException) {

            e1.printStackTrace()
            return false
        } catch (e1: KeyStoreException) {

            e1.printStackTrace()
            return false
        } catch (e1: InvalidKeyException) {

            e1.printStackTrace()
            return false
        }


    }

    private fun genKey() {
        try {
            if (keyStore == null) {
                keyStore = KeyStore.getInstance("AndroidKeyStore")
            }
        } catch (e: KeyStoreException) {
            e.printStackTrace()
        }

        var keyGenerator: KeyGenerator? = null

        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore")
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchProviderException) {
            e.printStackTrace()
        }

        try {
            keyStore?.load(null)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                keyGenerator!!.init(KeyGenParameterSpec.Builder(KEY_NAME, KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT).setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                        .setUserAuthenticationRequired(true)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7).build()
                )
            }
            keyGenerator!!.generateKey()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: CertificateException) {
            e.printStackTrace()
        } catch (e: InvalidAlgorithmParameterException) {
            e.printStackTrace()
        }


    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun startListening() {
        if (!dialog!!.isShowing) {
            showFingerprintDialog()
            fingerPrintListener!!.startListening(cryptoObject!!)
        }
    }
}