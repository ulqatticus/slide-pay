package com.slidepay.clientt.presenter.start.registration

import android.Manifest
import com.slidepay.clientt.R
import com.slidepay.clientt.contracts.start.RegistrationModalContract
import com.slidepay.clientt.view.start_app.registration.RegistrationModalActivity


class RegistrationModalPresenter(private var activity: RegistrationModalContract.View) : RegistrationModalContract.Presenter {


    private lateinit var rule: RegistrationModalActivity.Rules
    private lateinit var ruleText: String
    private var imageResource: Int? = null
    override fun setRule(rule: RegistrationModalActivity.Rules) {
        this.rule = rule
    }


    override fun viewCreated() {
        getDataByRule()
        activity.setRuleImage(imageResource!!)
        activity.setRuleText(ruleText)
        activity.acceptButtonPermission()
        activity.laterButtonPermission()

    }

    private fun getDataByRule() {

        when (rule) {

            RegistrationModalActivity.Rules.CAMERA -> {
                ruleText = "Разрешите доступ к камере для\nвозможности сканирования\nQR-кодов "
                imageResource = R.drawable.ic_rule_camera

            }
            RegistrationModalActivity.Rules.CONTACTS -> {
                ruleText = "Разрешите доступ к контактам,\nдля того чтоб обмениваться\nплатежами со своими друзьями"
                imageResource = R.drawable.ic_rule_contacts
                activity.setButtonText("Разрешить")


            }
            RegistrationModalActivity.Rules.ADDCARD -> {
                ruleText = "Для совершения переводов\nнеобходимо добавить карту"
                imageResource = R.drawable.onborading_card_img
                activity.setButtonText("Добавить")
            }
        }

    }


    override fun clickAcceptBtn() {
        when (rule) {
            RegistrationModalActivity.Rules.CAMERA -> {
                activity.checkAppPermission(Manifest.permission.CAMERA)

            }
            RegistrationModalActivity.Rules.CONTACTS -> {
                activity.checkAppPermission(Manifest.permission.READ_CONTACTS)

            }
            RegistrationModalActivity.Rules.ADDCARD -> {
                activity.startCreateCardActivity()

            }
        }
    }

    override fun clickNextBtn() {
        when (rule) {
            RegistrationModalActivity.Rules.CAMERA -> {
                RegistrationModalActivity.startContactsRules(activity.getContext())
            }
            RegistrationModalActivity.Rules.CONTACTS -> {
                activity.startAddCardActivity()
            }
            RegistrationModalActivity.Rules.ADDCARD -> {
                activity.startCreatePasswordActivity()

            }
        }
    }

    override fun viewDestroyed() {}


}