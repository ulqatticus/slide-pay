package com.slidepay.clientt.presenter.contacts

import android.os.Handler
import com.slidepay.clientt.application.utils.ConnectionHelper
import com.slidepay.clientt.contracts.contacts.ContactContract
import com.slidepay.clientt.model.objects.main.ContactsBody
import com.slidepay.clientt.model.objects.main.ContactsBodySend
import com.slidepay.clientt.model.service.ClientProfileService
import io.reactivex.observers.DisposableObserver
import retrofit2.Response


class ContactsPresenter(var view: ContactContract.View) : ContactContract.Presenter {
    override fun getContacts(contactList: ArrayList<ContactsBodySend>) {
        currentRunnable = Runnable {
            ClientProfileService().getContacts(contactList)?.subscribe(getObserver())
        }

        if (!ConnectionHelper().isOnline()) {
            view.showNoInternetConnection()
            return
        }

        handler.post(currentRunnable)    }

    private var currentRunnable: Runnable? = null
    private val handler: Handler = Handler()

    override fun viewCreated() {
        view.initContactList()
        view.initCloseBtnListener()
        view.initMultiStateListener()
        view.getContacts()
    }



    fun pickedContact(number: String) {
        view.pickedContact(number)

    }

    override fun retryBtnClick() {
        if (!ConnectionHelper().isOnline()) {
            return
        }
        handler.post(currentRunnable)
    }

    private fun getObserver(): DisposableObserver<Response<ArrayList<ContactsBody>>> {
        return object : DisposableObserver<Response<ArrayList<ContactsBody>>>() {
            override fun onStart() {
                view.showLoading()
            }

            override fun onNext(response: Response<ArrayList<ContactsBody>>) {

                response.let { it ->
                    when (it.code()) {
                        in 200..299 -> {
                            view.addItems(it.body()!!)
                            view.showContent()
                        }
                        in 400..599 -> {
                            view.showSomethingWentWrong()
                        }
                    }
                }
            }

            override fun onError(error: Throwable) {
                error.printStackTrace()
                view.showSomethingWentWrong()

            }

            override fun onComplete() {
                view.hideLoading()

            }
        }
    }

    override fun viewDestroyed() {
        if (!getObserver().isDisposed)
            getObserver().dispose()
    }


}