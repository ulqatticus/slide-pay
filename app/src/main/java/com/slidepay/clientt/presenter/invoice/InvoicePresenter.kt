package com.slidepay.clientt.presenter.invoice

import android.os.Handler
import com.slidepay.clientt.application.utils.ConnectionHelper
import com.slidepay.clientt.contracts.invoice.InvoiceContract
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.model.objects.main.InvoiceBody
import com.slidepay.clientt.model.objects.main.InvoiceBodyResponse
import com.slidepay.clientt.model.objects.main.ScannedBody
import com.slidepay.clientt.model.service.ClientCardsService
import com.slidepay.clientt.model.service.ClientInvoiceService
import com.slidepay.clientt.model.service.ClientProfileService
import com.slidepay.clientt.view.scanner.ScannerFragment
import io.reactivex.observers.DisposableObserver
import retrofit2.Response


class InvoicePresenter(var view: InvoiceContract.View) : InvoiceContract.Presenter {


    private var currentRunnable: Runnable? = null
    private val handler: Handler = Handler()

    override fun viewCreated() {
        view.getExtra()
        view.initStateListener()
        view.setDescription("")
        view.initViews()
        getUserCardsCard()

    }

    override fun createSendMoney() {

        if (validate()) {
            currentRunnable = Runnable {
                ClientInvoiceService().сreateInvoice(creteBody())?.subscribe(createInvoiceObserver())
            }

            if (!ConnectionHelper().isOnline()) {
                view.showNoInternetConnection()
                return
            }

            handler.post(currentRunnable)
        } else {
            view.showError()

        }
    }

    private fun creteBody(): InvoiceBody {
        val invoiceBody = InvoiceBody()
        invoiceBody.cardId = view.getCardId()
        invoiceBody.description = view.getDescription()
        invoiceBody.payerPhoneNumber = "380" + view.getPhoneNumber()
        invoiceBody.sum = view.getAmount()?.toDouble()
        return invoiceBody
    }

    override fun retryBtnClick() {
        if (!ConnectionHelper().isOnline()) {
            return
        }
        handler.post(currentRunnable)
    }

    private fun createInvoiceObserver(): DisposableObserver<Response<InvoiceBodyResponse>> {
        return object : DisposableObserver<Response<InvoiceBodyResponse>>() {
            override fun onStart() {
                view.showLoading()
            }

            override fun onNext(response: Response<InvoiceBodyResponse>) {

                response.let { it ->
                    when (it.code()) {
                        in 200..299 -> {
                            view.closeActivity()
                            view.startBillActivity(response.body()!!)
                        }
                        in 400..599 -> {
                            view.showSomethingWentWrong()
                        }
                    }

                }
            }

            override fun onError(error: Throwable) {
                error.printStackTrace()
                view.showSomethingWentWrong()


            }

            override fun onComplete() {
                view.hideLoading()

            }
        }
    }




    private fun validate(): Boolean {
        if (!view.getAmount().isNullOrEmpty()) {
            if (view.getAmount()?.trim()?.toDouble()!! > 0) {
                return true
            }
        }
        return false

    }

    override fun viewDestroyed() {
        if (!getAllCardsObserver().isDisposed) {
            getAllCardsObserver().dispose()
        }

        if (!getScannedInfoObserver().isDisposed) {
            getScannedInfoObserver().dispose()
        }
    }


    private fun getUserCardsCard() {
        if (!ConnectionHelper().isOnline()) {
            return
        }
        ClientCardsService().getUserCards()?.subscribe(getAllCardsObserver())


    }


    private fun getAllCardsObserver(): DisposableObserver<Response<ArrayList<CardBody>>> {
        return object : DisposableObserver<Response<ArrayList<CardBody>>>() {
            override fun onStart() {
                view.showLoading()
            }

            override fun onNext(response: Response<ArrayList<CardBody>>) {

                response.let { it ->

                    when (it.code()) {
                        in 200..299 -> {
                            if (it.body()?.isNotEmpty()!!) {
                                view.initUserCard(it.body()!!)
                            }
                        }
                        in 400..599 -> {
                            view.showSomethingWentWrong()
                        }
                    }

                }
            }

            override fun onError(error: Throwable) {
                error.printStackTrace()
                view.showSomethingWentWrong()

            }

            override fun onComplete() {
                view.hideLoading()

            }
        }
    }


    override fun getScannedContent(content: String) {
        currentRunnable = Runnable {
            ClientProfileService().scan(ScannerFragment.ContentBody(content))?.subscribe(getScannedInfoObserver())
        }

        if (!ConnectionHelper().isOnline()) {
            view.showNoInternetConnection()
            return
        }

        handler.post(currentRunnable)


    }

    private fun getScannedInfoObserver(): DisposableObserver<Response<ScannedBody>> {
        return object : DisposableObserver<Response<ScannedBody>>() {
            override fun onStart() {
                view.showLoading()
            }

            override fun onNext(response: Response<ScannedBody>) {


                response.let { it ->

                    when (it.code()) {
                        in 200..299 -> {
                            val number = validateNumber(it.body()?.phoneNumber!!)
                            view.setPhone(number)


                        }
                        in 400..599 -> {
                            view.showSomethingWentWrong()
                        }
                    }

                }
            }

            override fun onError(error: Throwable) {
                error.printStackTrace()

            }

            override fun onComplete() {
                view.hideLoading()

            }
        }
    }

    private fun validateNumber(number: String): String {
        return when {
            number.startsWith("+380") -> number.substring(4)
            number.startsWith("380") -> number.substring(3)
            number.startsWith("0") -> number.substring(1)
            else -> number
        }
    }
}