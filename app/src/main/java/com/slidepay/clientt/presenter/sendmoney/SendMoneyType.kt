package com.slidepay.clientt.presenter.sendmoney

enum class SendMoneyType {
    CREATE, PAY, FROM_TRANSACTION
}