package com.slidepay.clientt.presenter.mainpage.transaction

import android.os.Handler
import com.slidepay.clientt.application.utils.ConnectionHelper
import com.slidepay.clientt.contracts.main.transaction.TransactionContract
import com.slidepay.clientt.model.objects.main.ScannedBody
import com.slidepay.clientt.model.objects.main.TransactionBody
import com.slidepay.clientt.model.service.ClientInvoiceService
import com.slidepay.clientt.model.service.ClientSendMoneyService
import com.slidepay.clientt.model.service.ClientTransactionService
import com.slidepay.clientt.view.BaseActivity
import com.slidepay.clientt.view.invoice.InvoiceActivity
import io.reactivex.observers.DisposableObserver
import retrofit2.Response

class TransactionPresenter(private var activity: TransactionContract.View) : TransactionContract.Presenter {


    private var currentRunnable: Runnable? = null
    private val handler: Handler = Handler()


    override fun updateData() {
        getUserTransaction()
    }


    override fun viewDestroyed() {}

    override fun viewCreated() {
        activity.initTransactionsView()
        activity.initStateListener()
        getUserTransaction()
    }

    private fun getUserTransaction() {
        currentRunnable = Runnable {
            ClientTransactionService().getUserTransaction()?.subscribe(getUserTransactionObserver())
        }

        if (!ConnectionHelper().isOnline()) {
            activity.showNoInternetConnection()
            return
        }

        handler.post(currentRunnable)

    }


    override fun retryBtnClick() {
        if (!ConnectionHelper().isOnline()) {
            return
        }
        handler.post(currentRunnable)
    }


    override fun scrollToPosition(position: Int) {
        activity.scrollToPosition(position)
    }

    private fun getUserTransactionObserver(): DisposableObserver<Response<ArrayList<TransactionBody>>> {
        return object : DisposableObserver<Response<ArrayList<TransactionBody>>>() {
            override fun onStart() {
                activity.showLoading()
            }

            override fun onNext(response: Response<ArrayList<TransactionBody>>) {

                response.let { it ->

                    when (it.code()) {
                        in 200..299 -> {
                            activity.addItems(it.body()!!)
                            activity.showContent()

                        }
                        in 400..599 -> {
                            activity.showSomethingWentWrong()
                        }
                    }

                }
            }

            override fun onError(error: Throwable) {
                activity.showSomethingWentWrong()
            }

            override fun onComplete() {

            }
        }
    }


    override fun retryInvoice(id: String?) {

        if (!ConnectionHelper().isOnline()) {
            activity.showNoInternetConnection()
            return
        }
        ClientInvoiceService().retryInvoice(id)?.subscribe(getRetryInvoiceObserver())

    }


    private fun getRetryInvoiceObserver(): DisposableObserver<Response<Void>> {
        return object : DisposableObserver<Response<Void>>() {
            override fun onStart() {
                activity.showLoading()
            }

            override fun onNext(response: Response<Void>) {
                response.let { it ->
                    when (it.code()) {
                        in 200..299 -> {
                            getUserTransaction()
                            activity.showContent()

                        }
                        in 400..599 -> {
                            activity.showSomethingWentWrong()
                        }
                    }

                }
            }

            override fun onError(error: Throwable) {
                activity.showSomethingWentWrong()
                error.printStackTrace()
            }

            override fun onComplete() {
            }
        }
    }


    override fun cancelInvoice(id: String?) {


        if (!ConnectionHelper().isOnline()) {
            activity.showNoInternetConnection()
            return
        }
        ClientInvoiceService().cancelInvoice(id)?.subscribe(getRetryInvoiceObserver())
    }

    fun payInvoice(transaction: TransactionBody) {
        val payInvoiceBody = getBody(transaction)
        activity.startPayActivity(payInvoiceBody)


    }


    override fun cancelGetMoney(number: String?) {

        if (!ConnectionHelper().isOnline()) {
            activity.showNoInternetConnection()
            return
        }
        ClientSendMoneyService().cancelGetMoney(number)?.subscribe(getRetryInvoiceObserver())
    }

    override fun retryGetMoney(number: String?) {

        if (!ConnectionHelper().isOnline()) {
            activity.showNoInternetConnection()
            return
        }
        ClientSendMoneyService().retryGetMoney(number)?.subscribe(getRetryInvoiceObserver())
    }


    private fun getBody(transaction: TransactionBody): ScannedBody {
        val body = ScannedBody()
        body.sum = transaction.sum?.toDouble()
        body.name = transaction.name
        body.number = transaction.number
        body.description = transaction.description
        body.isFromTransacton = true
        return body
    }

    fun changeInvoice(transaction: TransactionBody) {
        InvoiceActivity.startInvoiceActivity(activity.getTContext(), getBody(transaction))
    }
}


