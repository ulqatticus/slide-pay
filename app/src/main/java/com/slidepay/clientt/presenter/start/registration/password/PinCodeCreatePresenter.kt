package com.slidepay.clientt.presenter.start.registration.password

import android.os.Handler
import com.slidepay.clientt.application.LocalData
import com.slidepay.clientt.view.start_app.registration.password.PinCodeContract

class PinCodeCreatePresenter(var activity: PinCodeContract.View) : PinCodeContract.Presenter {
    override fun forgotPassClick() {}


    private var doubleBackToExitPressedOnce = false

    override fun startScanFingerPrint() {}

    private var isNextActivity = false
    private lateinit var pass: String
    private lateinit var passrepeat: String

    override fun onResume() {}


    override fun viewIsReady() {
        activity.showKeyBoard()
        activity.setListenerToEditText()
        activity.setLabelText("Установите пин-код для безопасных платежей")
    }


    override fun next(password: String) {
        if (!isNextActivity) {
            activity.clearTextInEditText()
            activity.setLabelText("Подтвердите пин-код")
            isNextActivity = true
            pass = password
        } else {
            passrepeat = password
            if (isValidate()) {
                LocalData.isAuthorizationIsEnded = true
                LocalData.pinCode = password
                activity.startNextActivity()
            } else {
                activity.showError("Указанные пароли не совпадают")
            }
        }

    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            activity.closeActivity()
            return
        } else {
            activity.setLabelText("Установите пин-код для безопасных платежей")
            activity.clearTextInEditText()
            pass = ""
            isNextActivity = false
        }
        doubleBackToExitPressedOnce = true
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)


    }

    override fun onStop() {
    }

    private fun isValidate(): Boolean {
        if (pass.equals(passrepeat)) {
            return true
        }
        return false
    }

}

