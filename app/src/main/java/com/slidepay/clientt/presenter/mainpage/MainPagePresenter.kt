package com.slidepay.clientt.view.mainpage.pager

import android.os.Handler
import com.slidepay.clientt.application.utils.ConnectionHelper
import com.slidepay.clientt.contracts.main.MainPageContract
import com.slidepay.clientt.model.objects.main.ScannedBody
import com.slidepay.clientt.model.service.ClientProfileService
import com.slidepay.clientt.view.scanner.ScannerFragment
import io.reactivex.observers.DisposableObserver
import retrofit2.Response


class MainPagePresenter(var view: MainPageContract.View) : MainPageContract.Presenter {
    override fun getScannedContent(content: String) {
        currentRunnable = Runnable {
            ClientProfileService().scan(ScannerFragment.ContentBody(content))?.subscribe(getScannedInfoObserver())
        }

        if (!ConnectionHelper().isOnline()) {
            view.showNoInternetConnection()
            return
        }

        handler.post(currentRunnable)
    }


    private fun getScannedInfoObserver(): DisposableObserver<Response<ScannedBody>> {
        return object : DisposableObserver<Response<ScannedBody>>() {
            override fun onStart() {
                view.showLoader()
            }

            override fun onNext(response: Response<ScannedBody>) {
                response.let { it ->
                    when (it.code()) {
                        in 200..299 -> {
                            view.showContent()
                            when (it.body()?.type) {
                                0 -> {
                                    //TODO FIX THIS  WHEN SCAN ANOTHER QR-CODE response TYPE= 0
                                    it.body()?.let {
                                        if (!it.number.isNullOrBlank()) view.startPayActivity(it) else view.showScannError()
                                    }
                                }

                                1 -> it.body()?.let { view.startPaymentActivity(it) }
                                else -> {
                                    view.showScannError()

                                }
                            }
                            view.hideLoader()

                        }
                        in 400..599 -> {
                            view.showSomethingWentWrong()
                            view.hideLoader()
                        }
                        else -> {
                        }
                    }
                }
            }

            override fun onError(error: Throwable) {
                view.showSomethingWentWrong()
                error.printStackTrace()
            }

            override fun onComplete() {
                view.hideLoader()
            }
        }
    }

    private var currentRunnable: Runnable? = null
    private val handler: Handler = Handler()
    override fun viewDestroyed() {

    }

    override fun viewCreated() {
        view.initStateListener()

    }


    override fun retryBtnClick() {
        if (!ConnectionHelper().isOnline()) {
            return
        }
        handler.post(currentRunnable)
    }


}