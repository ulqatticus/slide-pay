package com.slidepay.clientt.presenter.start.type_number

import android.text.style.ClickableSpan
import android.view.View

class TermsClickableSpan(private var presenter: TypeNumberPresenter) : ClickableSpan() {

    override fun onClick(widget: View?) {


        if (!presenter.isClick) {
            presenter.isClick = true
            presenter.startWebFragment()
          //  view.startActivity(Intent(view, TermsOfUseDetailedActivity::class.java))
        }


        /*  if (!ConnectionHelper().isOnline()) {
              presenter.showViewNoInternet()
          } else {
              presenter.startCurrentRunnable()

          }*/
    }
}
