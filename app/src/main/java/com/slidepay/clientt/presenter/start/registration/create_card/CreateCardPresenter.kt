package com.slidepay.clientt.presenter.start.registration.create_card

import android.os.Handler
import com.slidepay.clientt.R
import com.slidepay.clientt.application.utils.ConnectionHelper
import com.slidepay.clientt.contracts.start.AddCardContract
import com.slidepay.clientt.model.objects.registration.CardUrl
import com.slidepay.clientt.model.service.ClientCardsService
import com.slidepay.clientt.view.start_app.registration.create_card.CreateCardActivity
import io.reactivex.observers.DisposableObserver
import retrofit2.Response
import java.util.regex.Pattern


class CreateCardPresenter(private var view: AddCardContract.View) : AddCardContract.Presenter {


    private val cardNUmberMask = "\\d{13,16}"
    private var currentRunnable: Runnable? = null
    private val handler: Handler = Handler()
    override fun loadWebView(url: String) {
        view.configWebView()
        if (ConnectionHelper().isOnline()) {
            view.loadUrl(url)
        }
    }


    override fun startSuccessFragment() {
        view.startSuccessFragment()
    }

    override fun viewCreated() {

    }

    override fun viewCreated(type: CreateCardActivity.Type) {
        view.initAddCardBtn()
        view.initCloseBtn()
        view.initTextWatcher()
        view.showKeyBoard()
        view.initTextWatcher()

        when (type) {
            CreateCardActivity.Type.CREATE_FROM_MENU_CARD -> {
                val skipText = view.getActivity().getString(R.string.cancel)
                view.showTextToSkip(skipText)

            }
            CreateCardActivity.Type.CREATE_ON_START_APP -> {
                val skipText = view.getActivity().getString(R.string.skip)

                view.showTextToSkip(skipText)

            }
        }
    }

    private fun isValidNumber(cardNumber: String) = cardNumber.isNotBlank() && Pattern.matches(cardNUmberMask, cardNumber)


    override fun sendRequest(cardNumber: String) {

        if (isValidNumber(cardNumber)) {
            currentRunnable = Runnable {
                ClientCardsService().createCard(cardNumber)?.subscribe(getCreateCardObserver())
            }
            if (!ConnectionHelper().isOnline()) {
                view.showNoInternetConnection()
                return
            }
            handler.post(currentRunnable)

        } else {
            view.showValidationError()
        }
    }


    private fun getCreateCardObserver(): DisposableObserver<Response<CardUrl>> {
        return object : DisposableObserver<Response<CardUrl>>() {
            override fun onStart() {
                view.showLoading()
            }

            override fun onNext(response: Response<CardUrl>) {

                response.let { it ->

                    when (it.code()) {
                        in 200..299 -> {
                            view.showFirstBlock()
                            view.hideSecondBlock()
                            it.body()?.url?.let {
                                view.loadWebView(it)

                            }
                        }
                        in 400..599 -> {
                            view.showSomethingWentWrong()
                        }
                        else -> view.showSomethingWentWrong()

                    }

                }
            }

            override fun onError(error: Throwable) {
                error.printStackTrace()
                view.showSomethingWentWrong()


            }

            override fun onComplete() {

            }
        }
    }

    override fun retryBtnClick() {
        if (!ConnectionHelper().isOnline()) {
            return
        }
        handler.post(currentRunnable)
    }


    override fun checkUrlToRedirect(url: String) {
        val redirectUrl = "slidepay://"
        if (url == redirectUrl) {
            startSuccessFragment()
        }
    }

    override fun viewDestroyed() {
        view.hideKeyBoard()
    }

}