package com.slidepay.clientt.presenter.mainpage.profile

import com.slidepay.clientt.application.LocalData
import com.slidepay.clientt.contracts.main.profile.ChangePasswordContract

class ChangePasswordPresenter(private var view: ChangePasswordContract.View) : ChangePasswordContract.Presenter {


    override fun viewDestroyed() {
    }

    override fun viewCreated() {
        view.initCloseBtn()
        view.initKeyListeners()
    }

    override fun next(oldPassText: String, newPasstext: String, newPassTextRepeat: String) {

        if (isBaseValidation(newPasstext.length)) {
            if (oldPassText != LocalData.pinCode) {
                view.showErrorMessage("Старый пароль введен неверно")
                return

            } else if (newPasstext != newPassTextRepeat) {
                view.showErrorMessage("Новые пароли не совпадают")
                return
            }
            LocalData.pinCode = newPasstext
            view.startSuccessFragment()

        } else {
            view.showErrorMessage("Новый пароль должен содержать не менее 4 символов")

        }
    }

    private fun isBaseValidation(length: Int): Boolean {
        return length > 3
    }
}