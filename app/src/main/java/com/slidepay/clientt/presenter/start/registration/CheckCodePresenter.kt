package com.slidepay.clientt.presenter.start.registration

import android.graphics.Color
import android.os.CountDownTimer
import android.os.Handler
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import com.google.gson.Gson
import com.slidepay.clientt.application.utils.Config
import com.slidepay.clientt.application.utils.ConnectionHelper
import com.slidepay.clientt.contracts.start.CheckCodeContract
import com.slidepay.clientt.model.database.TokenDataBase
import com.slidepay.clientt.model.objects.ErrorData
import com.slidepay.clientt.model.objects.RefreshTokenBody
import com.slidepay.clientt.model.objects.UserToken
import com.slidepay.clientt.model.objects.registration.UserLoginBody
import com.slidepay.clientt.model.objects.registration.UserVerifyBody
import com.slidepay.clientt.model.service.ClientService
import io.reactivex.observers.DisposableObserver
import retrofit2.Response


class CheckCodePresenter(private var activity: CheckCodeContract.View) : CheckCodeContract.Presenter {
    private var doubleBackToExitPressedOnce = false




    private var myPreviousText: String? = null
    private lateinit var text: String
    private var cTimer: CountDownTimer? = null
    private var currentRunnable: Runnable? = null
    private val handler: Handler = Handler()

    //private lateinit var disposable: DisposableObserver<Response<ResponseBody>>

    companion object {
        private const val LENGTH_CODE = 4
        private const val LETTER_SPACING = "    "
    }

    override fun closeActivity() {
        activity.initStateListener()

        cancelTimer()
        activity.hideKeyBoard()
    }


    override fun afterTextChanged(text: String) {
        this.text = text
        if (this.text != myPreviousText) {
            this.text = this.text.replace(" ", "")

            // Add space between each character
            val newText = StringBuilder()
            for (i in 0 until this.text.length) {
                if (i == this.text.length - 1) {
                    newText.append(Character.toUpperCase(this.text[this.text.length - 1]))
                } else {
                    newText.append(Character.toUpperCase(this.text[i])).append(LETTER_SPACING)
                }
            }

            myPreviousText = newText.toString()

            activity.editCodeSetText(newText)
            activity.editCodeSetSelection(newText.length)

            val code = this.text.replace(" ".toRegex(), "")

            if (code.length == LENGTH_CODE) {
                checkCode(code)
            }
        }
        activity.hideError()
    }


    override fun viewDestroyed() {
        //  disposable.dispose()
        activity.hideKeyBoard()
        cancelTimer()
    }

    override fun viewCreated() {
        activity.showKeyBoard()
        //  disposable = getSentCodeObserver()

    }


    fun checkCode(code: String) {


        if (code.length >= 4) {
            val body = UserVerifyBody()
            body.setCode(code)
            currentRunnable = Runnable {
                ClientService().checkCode(body)?.subscribe(getSentCodeObserver())
            }

            if (!ConnectionHelper().isOnline()) {
                activity.showNoInternetConnection()
                return
            }
            handler.post(currentRunnable)
        } else {
            activity.showError()

        }
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            activity.closeApp()
            return
        }
        doubleBackToExitPressedOnce = true
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }


    private fun getSentCodeObserver(): DisposableObserver<Response<RefreshTokenBody>> {
        return object : DisposableObserver<Response<RefreshTokenBody>>() {
            override fun onStart() {
                activity.showLoading()
            }

            override fun onNext(response: Response<RefreshTokenBody>) {

                when (response.code()) {
                    in 200..399 -> {
                        TokenDataBase.setRefreshToken(response.body()!!)

                        activity.hideKeyBoard()
                        activity.nextActivity()
                    }
                    in 400..499 -> {
                        val string = response.errorBody()?.string()
                        val errorData = Gson().fromJson(string, ErrorData::class.java)

                        if (errorData == null) {
                            activity.showSomethingWentWrong()
                        } else if (errorData.errorCode == Config.ErrorCode.ERROR_CHECK_CODE) {
                            activity.showError()
                            activity.showKeyBoard()
                        }

                    }
                    in 500..599 -> {
                        activity.hideKeyBoard()
                        activity.showSomethingWentWrong()

                    }

                }
            }


            override fun onError(error: Throwable) {
                error.printStackTrace()

            }

            override fun onComplete() {
                activity.hideLoading()

            }
        }
    }

    fun sentCodeAgain() {
        currentRunnable = Runnable {
            ClientService().resendCode()?.subscribe(getCodeAgainObserver())
        }

        if (!ConnectionHelper().isOnline()) {
            activity.showNoInternetConnection()
            return
        }
        handler.post(currentRunnable)
    }


    private fun getCodeAgainObserver(): DisposableObserver<Response<Void>> {
        return object : DisposableObserver<Response<Void>>() {
            override fun onStart() {
            }

            override fun onNext(response: Response<Void>) {
                when (response.code()) {
                    in 200..399 -> {
                        activity.hideKeyBoard()
                        // view.startSuccessFragment()
                    }
                    in 400..599 -> {
                        activity.showSomethingWentWrong()
                    }

                }
            }


            override fun onError(error: Throwable) {
                error.printStackTrace()
                activity.showSomethingWentWrong()
            }

            override fun onComplete() {

            }
        }
    }

    override fun retryBtnClick() {
        if (!ConnectionHelper().isOnline()) {
            return
        }
        handler.post(currentRunnable)
    }

    //start timer function
    fun startTimer() {
        activity.setResendBtnInvisible()
        activity.setTimerVisible()
        cTimer = object : CountDownTimer(120000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val time = millisUntilFinished / 1000
                val spannable = SpannableString("Повторная отправка кода возможна через $time секунд ")
                spannable.setSpan(ForegroundColorSpan(Color.parseColor("#FF6A6A6B")), 38, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                activity.setTimerSeconds(spannable)
            }

            override fun onFinish() {
                activity.setResendBtnVisible()
                activity.setTimerInvisible()
            }
        }
        cTimer?.start()
    }


    //cancel timer
    private fun cancelTimer() {
        if (cTimer != null)
            cTimer?.cancel()
    }


    /*  private fun getUserLoginBody(): UserLoginBody {
          val loginBody = UserLoginBody()
          loginBody.phone = view.getPhoneNUmber().replace("+", "")
          loginBody.device = "1"
          loginBody.os = getOs()
          return loginBody
      }*/

    private fun getOs(): String {
        return "Android"
    }
}