package com.slidepay.clientt.view.mainpage.pager

import com.slidepay.clientt.contracts.main.payment.PaymentContract


class PaymentPresenter(var activity: PaymentContract.View) : PaymentContract.Presenter {


    override fun viewCreated() {
        activity.initClickListeners()
        activity.setClientInformation()
    }

    override fun viewDestroyed() {}




}