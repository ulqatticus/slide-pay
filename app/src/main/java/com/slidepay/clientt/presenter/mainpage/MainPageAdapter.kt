package com.slidepay.clientt.presenter.mainpage.pager

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.slidepay.clientt.view.mainpage.add_card.PageCardFragment
import com.slidepay.clientt.view.mainpage.qrcode.PageQrCodeFragment
import com.slidepay.clientt.view.mainpage.profile.PageProfileFragment
import com.slidepay.clientt.view.mainpage.transaction.PageTransactionFragment

class MainPageAdapter(fm: FragmentManager, private val numberOfTabs: Int) : FragmentPagerAdapter(fm) {



    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return PageCardFragment()
            1 -> return PageTransactionFragment()
            2 -> return PageQrCodeFragment()
            3 -> return PageProfileFragment()
        }
        return PageQrCodeFragment()
    }

    override fun getCount(): Int {
        return numberOfTabs
    }
}