package com.slidepay.clientt.model.service

import com.slidepay.clientt.model.api.ServerApi
import com.slidepay.clientt.model.objects.main.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class ClientSendMoneyService {

    fun сreate(body: PaymentBody): Observable<Response<Void>>? {
        return ServerApi.clientPayment.createPayment(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }


    fun pay(body: PaymentBody): Observable<Response<Void>>? {
        return ServerApi.clientPayment.pay(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }

    fun updatePayment(body: PaymentBody): Observable<Response<Void>>? {
        return ServerApi.clientPayment.updatePayment(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }

    fun cancelGetMoney(id: String?): Observable<Response<Void>>? {
        return ServerApi.clientPayment.cancelGetMoney(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun retryGetMoney(id: String?): Observable<Response<Void>>? {
        return ServerApi.clientPayment.retryGetMoney(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

}