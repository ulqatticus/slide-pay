package com.slidepay.clientt.model.service

import com.slidepay.clientt.model.api.ServerApi
import com.slidepay.clientt.model.objects.main.ContactsBody
import com.slidepay.clientt.model.objects.main.ContactsBodySend
import com.slidepay.clientt.model.objects.main.ProfileBody
import com.slidepay.clientt.model.objects.main.ScannedBody
import com.slidepay.clientt.view.scanner.ScannerFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import retrofit2.Response

class ClientProfileService {


    fun getInfo(): Observable<Response<ProfileBody>>? {
        return ServerApi.profile.getInfo()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun editProfile(body: ProfileBody): Observable<Response<Void>>? {
        return ServerApi.profile.editProfile(body)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

    }

    fun setImageProfile(image: MultipartBody.Part?): Observable<Response<Void>>? {
        return ServerApi.profile.setImageProfile(image)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

    }

    fun scan(value: ScannerFragment.ContentBody): Observable<Response<ScannedBody>>? {
        return ServerApi.profile.scan(value)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }


    fun getContacts(contacts: ArrayList<ContactsBodySend>): Observable<Response<ArrayList<ContactsBody>>>? {
        return ServerApi.profile.getContacts(contacts)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

}

