package com.slidepay.clientt.model.objects

import com.google.gson.annotations.SerializedName

class ErrorData {

    @SerializedName("errorCode")
    val errorCode: Int = 0

    @SerializedName("error")
    val message: String? = null

    @SerializedName("fields")
    val fields: List<String>? = null

    override fun toString(): String {
        return "ErrorData{" +
                "errorCode='" + errorCode + '\''.toString() +
                ", message='" + message + '\''.toString() +
                ", fields=" + fields +
                '}'.toString()
    }
}