package com.slidepay.clientt.model.server.repository

import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.model.objects.main.RenameCardBody
import com.slidepay.clientt.model.objects.registration.AddCardBody
import com.slidepay.clientt.model.objects.registration.CardNumber
import com.slidepay.clientt.model.objects.registration.CardUrl
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ClientCardRepository {

    @GET("create")
    fun addCardView(@Query("name") cardName: String): Observable<Response<AddCardBody>>

    @GET("list")
    fun getUserCards(): Observable<Response<ArrayList<CardBody>>>

    @POST("delete")
    fun deleteCard(@Query("id") id: Long): Observable<Response<Void>>

    @POST("default")
    fun setDefaultCard(@Query("id") id: Long): Observable<Response<Void>>

    @POST("name")
    fun renameCard(@Query("id") id: Long, @Body renameBody: RenameCardBody): Observable<Response<Void>>

    @POST("create")
    fun createCard(@Body cardNumberStr: CardNumber): Observable<Response<CardUrl>>


}