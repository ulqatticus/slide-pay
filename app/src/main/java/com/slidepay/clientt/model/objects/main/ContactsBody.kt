package com.slidepay.clientt.model.objects.main

import com.google.gson.annotations.SerializedName


open class ContactsBody(

        @SerializedName("IsExists")
        var isInSystem: Boolean? = null,

        @SerializedName("Phone")
        var number: String? = null,

        @SerializedName("Name")
        var name: String? = null

)

open class ContactsBodySend(

        @SerializedName("Phone")
        var number: String? = null,

        @SerializedName("Name")
        var name: String? = null

)