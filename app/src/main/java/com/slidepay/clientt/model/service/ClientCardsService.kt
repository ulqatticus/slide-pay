package com.slidepay.clientt.model.service

import com.slidepay.clientt.model.api.ServerApi
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.model.objects.main.RenameCardBody
import com.slidepay.clientt.model.objects.registration.AddCardBody
import com.slidepay.clientt.model.objects.registration.CardNumber
import com.slidepay.clientt.model.objects.registration.CardUrl
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class ClientCardsService {

    fun addCardView(cardName: String): Observable<Response<AddCardBody>>? {
        return ServerApi.cards.addCardView(cardName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }

    fun getUserCards(): Observable<Response<ArrayList<CardBody>>>? {
        return ServerApi.cards.getUserCards()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }

    fun deleteCard(id: Long): Observable<Response<Void>>? {
        return ServerApi.cards.deleteCard(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun setDefaultCard(id: Long): Observable<Response<Void>>? {
        return ServerApi.cards.setDefaultCard(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun renameCard(id: Long, renameBody: RenameCardBody): Observable<Response<Void>>? {
        return ServerApi.cards.renameCard(id, renameBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun createCard(cardNumberStr: String): Observable<Response<CardUrl>>? {
        val cardNumber = CardNumber()
        cardNumber.number = cardNumberStr
        return ServerApi.cards.createCard(cardNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }

}