package com.slidepay.clientt.model.service

import com.slidepay.clientt.model.api.ServerApi
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.model.objects.main.InvoiceBody
import com.slidepay.clientt.model.objects.main.InvoiceBodyResponse
import com.slidepay.clientt.model.objects.registration.AddCardBody
import com.slidepay.clientt.presenter.mainpage.add_card.CardPresenter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class ClientInvoiceService {

    fun сreateInvoice(invoiceBody: InvoiceBody): Observable<Response<InvoiceBodyResponse>>? {
        return ServerApi.invoice.createInvoice(invoiceBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }

    fun retryInvoice(id: String?): Observable<Response<Void>>? {
        return ServerApi.invoice.retryInvoice(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }

    fun cancelInvoice(id: String?): Observable<Response<Void>>? {
        return ServerApi.invoice.cancelInvoice(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }

}