package com.slidepay.clientt.model.objects.main


import com.google.gson.annotations.SerializedName

class ProfileBody {


    @SerializedName("Email")
    var email: String? = null

    @SerializedName("FullName")
    var fullname: String? = null

    @SerializedName("Phone")
    var phone: String? = null

    @SerializedName("Birthday")
    var birthday: String? = null

    @SerializedName("Code")
    var code: String? = null

    @SerializedName("ImageURL")
    var imageURL: String? = null


}


