package com.slidepay.clientt.model.objects.main

import com.google.gson.annotations.SerializedName


open class PaymentBodyUpdate(
        @SerializedName("TransactionNumber")
        var transactionNumber: String? = null,

        @SerializedName("Sum")
        var sum: Double? = null,

        @SerializedName("CardID")
        var cardId: Long? = null
)


