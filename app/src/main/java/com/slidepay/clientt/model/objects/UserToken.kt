package com.slidepay.clientt.model.objects

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class UserToken() : RealmObject() {
    @SerializedName("Token")
    var token: String? = null

    override fun toString(): String {
        return "UserToken(token='$token')"
    }
}


open class RefreshTokenBody : RealmObject() {

    @SerializedName("RefreshToken")
    var refreshToken: String? = null


    override fun toString(): String {
        return "RefreshTokenBody{" +
                "refreshToken='" + refreshToken + '\''.toString() +
                '}'.toString()
    }
}