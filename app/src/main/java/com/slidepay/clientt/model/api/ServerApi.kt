package com.slidepay.clientt.model.api

import com.slidepay.clientt.application.App
import com.slidepay.clientt.application.utils.Config
import com.slidepay.clientt.model.server.repository.*

object ServerApi {

    val token: TokenRepository
        get() = App.retrofitNotToken(Config.Server.clientAuth).create(TokenRepository::class.java)

    val userLogin: ClientRegistrationRepository
        get() = App.retrofitNotToken(Config.Server.clientAuth).create(ClientRegistrationRepository::class.java)

    val auth: ClientRegistrationRepository
        get() = App.retrofitExpiredToken(Config.Server.clientAuth).create(ClientRegistrationRepository::class.java)

    val cards: ClientCardRepository
        get() = App.retrofitExpiredToken(Config.Server.clientCard).create(ClientCardRepository::class.java)

    val transactions: ClientTransactionRepository
        get() = App.retrofitExpiredToken(Config.Server.clientTransaction).create(ClientTransactionRepository::class.java)


    val invoice: ClientInvoiceRepository
        get() = App.retrofitExpiredToken(Config.Server.clientInvoice).create(ClientInvoiceRepository::class.java)


    val clientPayment: ClientPaymentRepository
        get() = App.retrofitExpiredToken(Config.Server.clientPayment).create(ClientPaymentRepository::class.java)


    val profile: ClientProfileRepository
        get() = App.retrofitExpiredToken(Config.Server.clientProfile).create(ClientProfileRepository::class.java)
}