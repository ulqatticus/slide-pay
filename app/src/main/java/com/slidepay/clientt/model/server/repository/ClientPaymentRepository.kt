package com.slidepay.clientt.model.server.repository

import com.slidepay.clientt.model.objects.main.PaymentBody
import com.slidepay.clientt.model.objects.main.PaymentBodyUpdate
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface ClientPaymentRepository {

    @POST("create")
    fun createPayment(@Body body: PaymentBody): Observable<Response<Void>>

    @POST("pay")
    fun pay(@Body body: PaymentBody): Observable<Response<Void>>


    @POST("update")
    fun updatePayment(@Body body: PaymentBody): Observable<Response<Void>>

    @POST("cancel")
    fun cancelGetMoney(@Query("id") id: String?): Observable<Response<Void>>

    @POST("retry")
    fun retryGetMoney(@Query("id") id: String?): Observable<Response<Void>>

}