package com.slidepay.clientt.model.service

import com.slidepay.clientt.model.api.ServerApi
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.model.objects.main.TransactionBody
import com.slidepay.clientt.model.objects.registration.AddCardBody
import com.slidepay.clientt.presenter.mainpage.add_card.CardPresenter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class ClientTransactionService {


    fun getUserTransaction(): Observable<Response<ArrayList<TransactionBody>>>? {
        return ServerApi.transactions.getUserTransaction()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }


}