package com.slidepay.clientt.model.objects.registration

import com.google.gson.annotations.SerializedName


class UserLoginBody {

    @SerializedName("Phone")
    var phone: String? = null


    @SerializedName("DeviceId")
    var device: String? = null

    @SerializedName("DeviceType")
    var os: String? = null

    override fun toString(): String {
        return "UserLoginBody(phone=$phone, device=$device, os=$os)"
    }


}