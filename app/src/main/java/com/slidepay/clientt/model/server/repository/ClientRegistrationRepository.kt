package com.slidepay.clientt.model.server.repository


import com.slidepay.clientt.model.objects.RefreshTokenBody
import com.slidepay.clientt.model.objects.UserToken
import com.slidepay.clientt.model.objects.main.PhoneNumberBody
import com.slidepay.clientt.model.objects.registration.UserLoginBody
import com.slidepay.clientt.model.objects.registration.UserVerifyBody
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ClientRegistrationRepository {
    @POST("SignIn")
    fun login(@Body userLoginBody: UserLoginBody): Observable<Response<UserToken>>

    @POST("ResendCode")
    fun resendCode(): Observable<Response<Void>>

    @POST("CheckCode")
    fun verify(@Body userVerifyBody: UserVerifyBody): Observable<Response<RefreshTokenBody>>


    @POST("restorepincode")
    fun restorePass(): Observable<Response<PhoneNumberBody>>
}