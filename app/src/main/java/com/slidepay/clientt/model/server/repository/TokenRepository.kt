package com.slidepay.clientt.model.server.repository


import com.slidepay.clientt.model.objects.RefreshTokenBody
import com.slidepay.clientt.model.objects.UserToken
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface TokenRepository {

    @POST("NewToken")
    fun getNewAccessToken(@Body refreshTokenBody: RefreshTokenBody): Call<UserToken>

}