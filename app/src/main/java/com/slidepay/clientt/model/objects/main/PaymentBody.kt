package com.slidepay.clientt.model.objects.main

import com.google.gson.annotations.SerializedName


open class PaymentBody(
        @SerializedName("BillNumber")
        var billNumber: String? = null,

        @SerializedName("RecipientNumber")
        var recipientNumber: String? = null,

        @SerializedName("Description")
        var description: String? = null,

        @SerializedName("Sum")
        var sum: Double? = null,

        @SerializedName("TransactionNumber")
        var transactionNumber: String? = null,

        @SerializedName("CardID")
        var cardId: Long? = null


) {
  /*  fun isValid(): Boolean =  (sum == null && cardId == null) &&
            (!transactionNumber.isNullOrBlank() || !billNumber.isNullOrBlank()
                    || !recipientNumber.isNullOrBlank())*/

}


