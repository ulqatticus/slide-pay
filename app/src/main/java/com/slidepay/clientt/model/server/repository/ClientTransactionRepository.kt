package com.slidepay.clientt.model.server.repository

import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.model.objects.main.TransactionBody
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

interface ClientTransactionRepository {


    @GET("list")
    fun getUserTransaction(): Observable<Response<ArrayList<TransactionBody>>>


}