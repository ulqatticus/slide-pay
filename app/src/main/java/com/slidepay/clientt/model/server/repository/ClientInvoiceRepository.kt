package com.slidepay.clientt.model.server.repository

import com.slidepay.clientt.model.objects.main.InvoiceBody
import com.slidepay.clientt.model.objects.main.InvoiceBodyResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface ClientInvoiceRepository {

    @POST("create")
    fun createInvoice(@Body invoiceBody: InvoiceBody): Observable<Response<InvoiceBodyResponse>>


    @POST("retry")
    fun retryInvoice(@Query("id") id: String?): Observable<Response<Void>>

    @POST("cancel")
    fun cancelInvoice(@Query("id") id: String?): Observable<Response<Void>>
}