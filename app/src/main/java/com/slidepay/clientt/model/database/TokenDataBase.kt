package com.slidepay.clientt.model.database

import com.slidepay.clientt.application.realm
import com.slidepay.clientt.model.objects.RefreshTokenBody

import com.slidepay.clientt.model.objects.UserToken

object TokenDataBase {

    fun getUserTokenToken(): UserToken? {
        var token: UserToken? = null
        realm {
            token = it.where(UserToken::class.java).findFirst()
        }
        return token
    }

    fun getRefreshToken(): String? {
        var token: RefreshTokenBody? = null
        realm {
            token = it.where(RefreshTokenBody::class.java).findFirst()
        }
        return token?.refreshToken
    }


    fun updateClientToken(token: String) = realm {
        it.where(UserToken::class.java).findFirst()?.let {
            it.token = token
        }
    }

    fun setClientToken(token: UserToken) = realm {
        if (!token.isManaged) {
            it.where(UserToken::class.java).findAll().deleteAllFromRealm()
            it.copyToRealm(token)
        }
    }

    fun setRefreshToken(token: RefreshTokenBody) = realm {
        it.where(RefreshTokenBody::class.java).findAll().deleteAllFromRealm()
        it.copyToRealm(token)
    }


    fun removeClientToken() = realm {
        it.where(UserToken::class.java).findAll().deleteAllFromRealm()
    }
}