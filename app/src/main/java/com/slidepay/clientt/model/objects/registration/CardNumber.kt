package com.slidepay.clientt.model.objects.registration

import com.google.gson.annotations.SerializedName

 class CardNumber {
    @SerializedName("Number")
    var number: String? = null
}