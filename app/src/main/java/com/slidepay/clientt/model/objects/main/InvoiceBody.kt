package com.slidepay.clientt.model.objects.main

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


open class InvoiceBody(
        @SerializedName("PayerPhoneNumber")
        var payerPhoneNumber: String? = null,

        @SerializedName("Sum")
        var sum: Double? = null,

        @SerializedName("Description")
        var description: String? = null,

        @SerializedName("CardID")
        var cardId: Long? = null,

        @SerializedName("SumType")
        var sumType: String = "Fixed"


)
@Parcelize
open class InvoiceBodyResponse(
        @SerializedName("PayerFullName")
        var payerFullName: String? = null,

        @SerializedName("Sum")
        var sum: Double? = null,

        @SerializedName("Description")
        var description: String? = null,

        @SerializedName("Code")
        var code: String? = null


) : Parcelable


/*
 enum class CardType {
    CARD, ADD_NEW_CARD
}*/
