package com.slidepay.clientt.model.objects.registration

import com.google.gson.annotations.SerializedName

 class CardUrl {
    @SerializedName("URL")
    var url: String? = null
}