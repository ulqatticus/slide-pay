package com.slidepay.clientt.model.objects.registration

import com.google.gson.annotations.SerializedName

class UserVerifyBody {

    @SerializedName("Code")
    private var code: String? = null


    fun setCode(code: String) {
        this.code = code
    }

    override fun toString(): String {
        return "UserVerifyBody{" +
                "code='" + code + '\''.toString() +
                '}'.toString()
    }
}