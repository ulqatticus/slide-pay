package com.slidepay.clientt.model.objects.main

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ScannedBody(
        @SerializedName("Type")
        var type: Int? = null,

        @SerializedName("Number")
        var number: String? = null,

        @SerializedName("Phone")
        var phoneNumber: String? = null,

        @SerializedName("FullName")
        var name: String? = null,

        @SerializedName("Description")
        var description: String? = null,

        @SerializedName("Sum")
        var sum: Double? = null,

        @SerializedName("SumType")
        var sumType: SumType? = null,

        @SerializedName("ImageURL")
        var image: String? = null,

        @SerializedName("isFromTransacton")
        var isFromTransacton: Boolean? = null) : Parcelable



enum class SumType {
    @SerializedName("0")
    Fixed,
    @SerializedName("1")
    Optional,
    @SerializedName("2")
    NotLess
}