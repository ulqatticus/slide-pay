package com.slidepay.clientt.model.objects.main


import com.google.gson.annotations.SerializedName

class PhoneNumberBody {
    @SerializedName("Phone")
    var phone: String? = null
}
