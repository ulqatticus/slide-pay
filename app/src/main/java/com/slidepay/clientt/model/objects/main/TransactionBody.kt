package com.slidepay.clientt.model.objects.main

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
open class TransactionBody(
        @SerializedName("Number")
        var number: String? = null,
        @SerializedName("Date")
        var date: String? = null,
        @SerializedName("Time")
        var time: String? = null,
        @SerializedName("IsInvoice")
        var isInvoice: Boolean? = null,
        @SerializedName("Name")
        var name: String? = null,
        @SerializedName("Description")
        var description: String? = null,
        @SerializedName("Sum")
        var sum: String? = null,
        @SerializedName("Status")
        var status: Int? = null,
        @SerializedName("ErrorDescription")
        var errorDescription: String? = null,
        @SerializedName("IsSpent")
        var isSpent: Boolean? = null,
        @SerializedName("IsEditable")
        var isEditable: Boolean? = null
) : Parcelable

