package com.slidepay.clientt.model.objects.main

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject


open class CardBody(
        @SerializedName("Id")
        var id: Long,

        @SerializedName("IsDefault")
        var isPrimary: Boolean? = null,

        @SerializedName("Number")
        var number: String? = null,

        @SerializedName("Name")
        var name: String? = null,

        @SerializedName("Type")
        var cardBankName: String? = null,


        var type: Int? = null

) {
    override fun toString(): String {
        return "$name"
    }
}
class RenameCardBody(var name: String? = null)

/*
 enum class CardType {
    CARD, ADD_NEW_CARD
}*/
