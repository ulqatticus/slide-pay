package com.slidepay.clientt.model.service

import com.slidepay.clientt.model.api.ServerApi
import com.slidepay.clientt.model.objects.RefreshTokenBody
import com.slidepay.clientt.model.objects.UserToken
import com.slidepay.clientt.model.objects.main.PhoneNumberBody
import com.slidepay.clientt.model.objects.registration.UserLoginBody
import com.slidepay.clientt.model.objects.registration.UserVerifyBody
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class ClientService {

    fun login(userLoginBody: UserLoginBody): Observable<Response<UserToken>>? {
        return ServerApi.userLogin.login(userLoginBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }


    fun resendCode(): Observable<Response<Void>>? {
        return ServerApi.auth.resendCode()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }

    fun checkCode(body: UserVerifyBody): Observable<Response<RefreshTokenBody>>? {
        return ServerApi.auth.verify(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun restorePass(): Observable<Response<PhoneNumberBody>>? {
        return ServerApi.auth.restorePass()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }


}