package com.slidepay.clientt.model.objects.registration

import com.google.gson.annotations.SerializedName


class AddCardBody {

    @SerializedName("Sign")
    var sign: String? = null

    @SerializedName("Url")
    var url: String? = null

    override fun toString(): String {
        return "AddCardBody(sign=$sign, url=$url)"
    }


}