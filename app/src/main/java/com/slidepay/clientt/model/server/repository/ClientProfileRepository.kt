package com.slidepay.clientt.model.server.repository

import com.slidepay.clientt.model.objects.main.ContactsBody
import com.slidepay.clientt.model.objects.main.ContactsBodySend
import com.slidepay.clientt.model.objects.main.ProfileBody
import com.slidepay.clientt.model.objects.main.ScannedBody
import com.slidepay.clientt.view.scanner.ScannerFragment
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*


interface ClientProfileRepository {

    @GET("Info")
    fun getInfo(): Observable<Response<ProfileBody>>

    @POST("Edit")
    fun editProfile(@Body body: ProfileBody): Observable<Response<Void>>

    @Multipart
    @POST("ProfilePhoto")
    fun setImageProfile(@Part image: MultipartBody.Part?): Observable<Response<Void>>

    @POST("Scan")
    fun scan(@Body value: ScannerFragment.ContentBody): Observable<Response<ScannedBody>>

    @POST("Contacts")
    fun getContacts(@Body contactsBody: ArrayList<ContactsBodySend>): Observable<Response<ArrayList<ContactsBody>>>
}

