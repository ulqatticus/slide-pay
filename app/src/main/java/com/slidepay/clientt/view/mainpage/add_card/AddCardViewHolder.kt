package com.slidepay.clientt.view.mainpage.add_card

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.Toast
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.card_item.view.*


class AddCardViewHolder(itemView: View, private var adapter: CardAdapter) : RecyclerView.ViewHolder(itemView) {


    fun bind() {

        itemView.setOnClickListener {
            adapter.createCard()

        }
    }
}