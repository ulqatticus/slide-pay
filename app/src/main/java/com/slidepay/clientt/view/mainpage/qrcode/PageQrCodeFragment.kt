package com.slidepay.clientt.view.mainpage.qrcode

import android.Manifest
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import cn.refactor.multistatelayout.MultiStateLayout
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.*
import com.karumi.dexter.listener.single.PermissionListener
import com.slidepay.clientt.R
import com.slidepay.clientt.contracts.main.qrcode.QrCodeContract
import com.slidepay.clientt.presenter.mainpage.qrcode.QrCodePresenter
import com.slidepay.clientt.view.BaseActivity
import com.slidepay.clientt.view.invoice.InvoiceActivity
import com.slidepay.clientt.view.scanner.MainPageListener
import com.slidepay.clientt.view.sendmoney.SendMoneyFragment
import com.slidepay.clientt.view.widgets.ButtonTypeface
import kotlinx.android.synthetic.main.fragment_qr_code.view.*
import kotlinx.android.synthetic.main.info_no_internet.view.*
import kotlinx.android.synthetic.main.info_something_went_wrong.view.*


class PageQrCodeFragment : Fragment(), QrCodeContract.View {

    private lateinit var createInvoiceBtn: ButtonTypeface
    private lateinit var scannerImageView: ImageView
    private lateinit var multiStateLayout: MultiStateLayout
    private lateinit var sendMoney: ButtonTypeface
    private var listener: MainPageListener? = null


    var presenter: QrCodePresenter = QrCodePresenter(this)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_qr_code, container, false)
        scannerImageView = rootView.qrcode_item_image
        scannerImageView.setOnClickListener { checkScannerActivity() }

        createInvoiceBtn = rootView.qrcode_item_create_invoice_btn
        createInvoiceBtn.setOnClickListener { createInvoice() }

        sendMoney = rootView.qrcode_item_send_money_btn
        sendMoney.setOnClickListener {
            createSendMoney()
        }

        multiStateLayout = rootView.multi_state_layout

        return rootView
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.viewCreated()

    }


    private fun checkScannerActivity() {
        Dexter.withActivity(requireActivity())
                .withPermission(Manifest.permission.CAMERA)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                        startScannerActivity()
                    }

                    override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                        token?.continuePermissionRequest()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                        showError()
                    }

                }).check()
    }

    override fun startScannerActivity() {
        listener?.openScanner()
    }


    private fun createSendMoney() {
        listener?.openSendMoney()
    }




    override fun showLoading() {
        multiStateLayout.state = MultiStateLayout.State.LOADING
    }


    override fun showSomethingWentWrong() {
        multiStateLayout.state = MultiStateLayout.State.ERROR
    }

    override fun showNoInternetConnection() {
        multiStateLayout.state = MultiStateLayout.State.NETWORK_ERROR

    }

    override fun showContent() {
        multiStateLayout.state = MultiStateLayout.State.CONTENT
    }


    override fun showError() {
        Toast.makeText(requireContext(), "Вам необходмо разрешить доступ к камере, для сканировая qr-когда ", Toast.LENGTH_LONG).show()
    }

    private fun createInvoice() {
        InvoiceActivity.startInvoiceActivity(activity as BaseActivity)
    }

    override fun getQrImgWidth(): Int = scannerImageView.width


    override fun getQrImgHeight(): Int = scannerImageView.height


    override fun generateQRImage() {
        val qrBitmap = presenter.createQrBitmapImg()
        scannerImageView.setImageBitmap(qrBitmap)
    }


    override fun initStateListener() {
        multiStateLayout.setOnStateViewCreatedListener { view, state ->

            when (state) {
                MultiStateLayout.State.NETWORK_ERROR -> {
                    val retry = view.info_no_internet_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }
                MultiStateLayout.State.ERROR -> {
                    val retry = view.something_went_wrong_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }

            }
        }
    }
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            listener = context as MainPageListener
        } catch (castException: ClassCastException) { }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        presenter.viewDestroyed()
    }

}