package com.slidepay.clientt.view.invoice.bill

import android.graphics.Bitmap
import android.os.Bundle
import android.widget.ImageView
import com.slidepay.clientt.R
import com.slidepay.clientt.contracts.invoice.BillContract
import com.slidepay.clientt.model.objects.main.InvoiceBodyResponse
import com.slidepay.clientt.presenter.invoice.bill.BillPresenter
import com.slidepay.clientt.view.BaseActivity
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.bill_activity.*

class BillActivity : BaseActivity(), BillContract.View {


    val presenter = BillPresenter(this)


    private lateinit var qrCode: ImageView
    private val billSumm: TextTypeface by lazy { bill_price }
    private val billDescription: TextTypeface by lazy { bill_description }
    private val billName: TextTypeface by lazy { bill_client_name }
    private val closeBtn: ImageView by lazy { bill_close_btn }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bill_activity)
        qrCode = findViewById(R.id.bill_qrcode_item_image)
        val billObject = intent.getParcelableExtra("body") as InvoiceBodyResponse
        presenter.viewCreated(billObject)

    }


    override fun getQrImgWidth(): Int {
        return qrCode.width
    }

    override fun getQrImgHeight(): Int {
        return qrCode.height
    }

    override fun setDescription(description: String?) {
        billDescription.text = description
    }

    override fun setQrImage(qrImage: Bitmap?) {
        qrCode.setImageBitmap(qrImage)

    }

    override fun setName(payerFullName: String?) {
        billName.text = payerFullName
    }

    override fun setSum(sum: Double?) {
        billSumm.text = sum.toString() + "₴"
    }

    override fun initCloseBtn() {
        closeBtn.setOnClickListener {
            finish()
        }
    }


    override fun showLoading() {
        super.showProgress()
    }

    override fun hideLoading() {
        super.hideProgress()
    }
}