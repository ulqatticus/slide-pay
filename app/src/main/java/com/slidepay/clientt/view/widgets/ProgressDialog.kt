package com.slidepay.clientt.view.widgets

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import com.slidepay.clientt.R

class ProgressDialog : DialogFragment() {

    private var dialog: AlertDialog? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if (dialog == null) {

            dialog = AlertDialog.Builder(activity!!)
                    .setView(activity?.layoutInflater?.inflate(R.layout.preloader, null, false))
                    .create()
            dialog!!.let { dialog ->
                dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.window.setDimAmount(0f)
                dialog.setCanceledOnTouchOutside(false)
                isCancelable = false
            }
        }
        return dialog!!
    }

    fun start(manager: FragmentManager): ProgressDialog {
        show(manager, "loading")
        return this
    }

    override fun onDestroy() {
        super.onDestroy()
        dialog = null
    }
}