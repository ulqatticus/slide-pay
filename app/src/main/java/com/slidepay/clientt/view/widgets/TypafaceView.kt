package com.slidepay.clientt.view.widgets

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.slidepay.clientt.R
import com.slidepay.clientt.view.widgets.TypefaceView.DEFAULT
import com.slidepay.clientt.view.widgets.TypefaceView.ROBOTO_ITALIC
import com.slidepay.clientt.view.widgets.TypefaceView.ROBOTO_BOLD
import com.slidepay.clientt.view.widgets.TypefaceView.ROBOTO_REGULAR

object TypefaceView {
    const val DEFAULT = 0
    const val ROBOTO_ITALIC = 1
    const val ROBOTO_BOLD = 2
    const val ROBOTO_REGULAR = 3

}

fun TextView.setTypeface(attrs: AttributeSet) {
    val a = context.obtainStyledAttributes(attrs, R.styleable.TypefaceView)
    val anInt = a.getInt(R.styleable.TypefaceView_typeface, DEFAULT)
    when (anInt) {
        ROBOTO_ITALIC -> typeface = Typeface.createFromAsset(context.assets, "fonts/Roboto-Italic.ttf")
        ROBOTO_BOLD -> typeface = Typeface.createFromAsset(context.assets, "fonts/Roboto-Bold.ttf")
        ROBOTO_REGULAR -> typeface = Typeface.createFromAsset(context.assets, "fonts/Roboto-Regular.ttf")

    }
    a.recycle()
}



class EditTextTypeface(context: Context?, attrs: AttributeSet) : EditText(context, attrs) {
    init {
        setTypeface(attrs)
    }
}

class TextTypeface(context: Context?, attrs: AttributeSet) : TextView(context, attrs) {
    init {
        setTypeface(attrs)
    }
}

class ButtonTypeface(context: Context?, attrs: AttributeSet) : Button(context, attrs) {
    init {
        setTypeface(attrs)
    }
}