package com.slidepay.clientt.view.contacts

import android.graphics.Color
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.FrameLayout
import com.slidepay.clientt.model.objects.main.ContactsBody
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.item_contacts.view.*

class ContactItemViewHolder(itemView: View, var adapter: ContactAdapter) : RecyclerView.ViewHolder(itemView) {


    private val contactName: TextTypeface by lazy { itemView.contacts_name }
    private val contactPhoneNumber: TextTypeface by lazy { itemView.contacts_number }
    private val mainBlock: ConstraintLayout by lazy { itemView.contact_main_block }
    private val statusBlock: FrameLayout by lazy { itemView.contact_status }


    fun bind(contact: ContactsBody) {
        contactName.text = contact.name
        contactPhoneNumber.text = contact.number
        if (contact.isInSystem!!) {
            statusBlock.setBackgroundColor(Color.parseColor("#53DEB4"))
        } else {
            statusBlock.setBackgroundColor(Color.parseColor("#FFFFFF"))

        }

        mainBlock.setOnClickListener {
            val number = validateNumber(contact.number!!)
            adapter.pickedContact(number)
        }
    }

    private fun validateNumber(number: String): String {
        return when {
            number.startsWith("+380") -> number.substring(4)
            number.startsWith("380") -> number.substring(3)
            number.startsWith("0") -> number.substring(1)
            else -> number
        }
    }


}