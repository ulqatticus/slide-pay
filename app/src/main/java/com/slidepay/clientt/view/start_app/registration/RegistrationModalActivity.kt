package com.slidepay.clientt.view.start_app.registration

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.slidepay.clientt.R
import com.slidepay.clientt.application.utils.Config
import com.slidepay.clientt.contracts.start.RegistrationModalContract
import com.slidepay.clientt.presenter.start.registration.RegistrationModalPresenter
import com.slidepay.clientt.view.BaseActivity
import com.slidepay.clientt.view.start_app.registration.create_card.CreateCardActivity
import com.slidepay.clientt.view.start_app.registration.password.PasswordActivity
import com.slidepay.clientt.view.widgets.ButtonTypeface
import com.slidepay.clientt.view.widgets.TextTypeface
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.rule_view.*


class RegistrationModalActivity : BaseActivity(), RegistrationModalContract.View {


    private val confirmBtn: ButtonTypeface by lazy { rule_btn_agree }
    private val confirmLaterBtn: TextTypeface by lazy { rule_btn_agree_later }
    private val ruleText: TextTypeface by lazy { rule_text }
    private val ruleImage: ImageView by lazy { rule_icon }

    val presenter = RegistrationModalPresenter(this)

    companion object {
        fun startMessagesRules(context: Context) {
            startActivity(context, Rules.CAMERA)
        }

        fun startContactsRules(context: Context) {
            startActivity(context, Rules.CONTACTS)
        }


        fun startAddCardRules(context: Context) {
            startActivity(context, Rules.ADDCARD)
        }

        private fun startActivity(context: Context, pinCodeMode: Rules) {
            val intent = Intent(context, RegistrationModalActivity::class.java)
            intent.putExtra(Config.EXTRA_MODE, pinCodeMode)
            context.startActivity(intent)
        }
    }

    override fun setRuleImage(imageResource: Int) {
        Picasso.get().load(imageResource).into(ruleImage)

    }

    override fun setRuleText(ruleText: String) {
        this.ruleText.text = ruleText
    }


    override fun getContext(): Context {
        return this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.rule_view)
        val rule = intent.getSerializableExtra(Config.EXTRA_MODE) as Rules
        presenter.setRule(rule)
        presenter.viewCreated()
    }


    override fun acceptButtonPermission() {
        confirmBtn.setOnClickListener { presenter.clickAcceptBtn() }

    }

    override fun setButtonText(text:String) {
        confirmBtn.text = text
    }


    override fun laterButtonPermission() {
        confirmLaterBtn.setOnClickListener { presenter.clickNextBtn() }
    }

    override fun checkAppPermission(permission: String) {
        Dexter.withActivity(this)
                .withPermission(permission)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        presenter.clickNextBtn()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                    }

                    override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {/* ... */
                        token.continuePermissionRequest()
                    }
                }).check()

    }

    override fun startCreateCardActivity() {
        val intent =Intent(this, CreateCardActivity::class.java)
        intent.putExtra("type", CreateCardActivity.Type.CREATE_ON_START_APP)
        startActivity(intent)

    }

    override fun startAddCardActivity() {
        startAddCardRules(this)
    }

    override fun startCreatePasswordActivity() {
        PasswordActivity.createPinCode(this)
        finish()
    }

    enum class Rules {
        CONTACTS, CAMERA, ADDCARD
    }
}