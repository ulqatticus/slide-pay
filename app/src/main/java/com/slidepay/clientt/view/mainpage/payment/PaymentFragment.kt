package com.slidepay.clientt.view.mainpage.payment

import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.slidepay.clientt.R
import com.slidepay.clientt.contracts.main.payment.PaymentContract
import com.slidepay.clientt.model.objects.main.ScannedBody
import com.slidepay.clientt.presenter.sendmoney.SendMoneyType
import com.slidepay.clientt.view.BaseActivity
import com.slidepay.clientt.view.invoice.InvoiceActivity
import com.slidepay.clientt.view.mainpage.pager.PaymentPresenter
import com.slidepay.clientt.view.sendmoney.SendMoneyFragment
import com.slidepay.clientt.view.widgets.ButtonTypeface
import com.slidepay.clientt.view.widgets.TextTypeface
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_payment.*

class PaymentFragment : Fragment(), PaymentContract.View {


    private val presenter = PaymentPresenter(this)

    private val closeBtn: ImageView by lazy { payment_close_btn }
    private val sendMoneyBtn: ButtonTypeface by lazy { payment_send_money_btn }
    private val createInvoiceBtn: ButtonTypeface by lazy { payment_create_invoice_btn }
    private val userName: TextTypeface by lazy { payment_client_name }
    private val userAvatar: CircleImageView by lazy { payment_client_img }
    private val userPhoneNumber: TextTypeface by lazy { payment_client_phone }

    private lateinit var scannedBody: ScannedBody

    companion object {
        fun create(scannedBody: ScannedBody): Fragment = PaymentFragment().apply {
            this.scannedBody = scannedBody
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_payment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.viewCreated()

    }


    override fun initClickListeners() {
        closeBtn.setOnClickListener { closePaymentFragment() }
        sendMoneyBtn.setOnClickListener { startSendMoney() }
        createInvoiceBtn.setOnClickListener { startCreateInvoice() }
    }


    private fun startCreateInvoice() {
        InvoiceActivity.startInvoiceActivity(this as BaseActivity, scannedBody)
        closePaymentFragment()
    }

    private fun startSendMoney() {
        childFragmentManager
                .beginTransaction()
                .add(R.id.payment_container, SendMoneyFragment.create(SendMoneyType.CREATE, scannedBody), SendMoneyFragment::class.java.name)
                .addToBackStack(SendMoneyFragment::class.java.name)
                .commit()
        //TODO close when finish
       // closePaymentFragment()
    }

    override fun closePaymentFragment() {
        fragmentManager?.popBackStackImmediate()
    }


    override fun setClientInformation() {
        if (!scannedBody.image.isNullOrEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Glide.with(this).load(scannedBody.image).into(userAvatar).onLoadFailed(requireContext().getDrawable(R.drawable.ic_no_picture_user))
            } else {
                Glide.with(this).load(scannedBody.image).into(userAvatar).onLoadFailed(resources.getDrawable(R.drawable.ic_no_picture_user))
            }
        }
        userName.text = scannedBody.name
        userPhoneNumber.text = scannedBody.phoneNumber
    }
}