package com.slidepay.clientt.view.mainpage.add_card

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import cn.refactor.multistatelayout.MultiStateLayout
import com.slidepay.clientt.R
import com.slidepay.clientt.application.utils.Config
import com.slidepay.clientt.contracts.main.addcard.CardContract
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.presenter.mainpage.add_card.CardPresenter
import com.slidepay.clientt.view.mainpage.transaction.CustomLayoutManager
import com.slidepay.clientt.view.start_app.registration.create_card.CreateCardActivity
import kotlinx.android.synthetic.main.fragment_card.view.*
import kotlinx.android.synthetic.main.info_no_internet.view.*
import kotlinx.android.synthetic.main.info_something_went_wrong.view.*
import android.support.v7.util.DiffUtil


class PageCardFragment : Fragment(), CardContract.View {


    val presenter: CardPresenter = CardPresenter(this)


    private lateinit var cardAdapter: CardAdapter

    private lateinit var cardRecycler: RecyclerView
    private lateinit var multiStateLayout: MultiStateLayout


    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        when (requestCode) {
            Config.RequestCode.CREATE_NEW_CARD_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    presenter.getUserCards()
                }
            }
        }

    }

    override fun showKeyboard() {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    override fun hideKeyboard() {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_card, container, false)
        cardRecycler = rootView.add_card_recycler
        multiStateLayout = rootView.multi_state_layout
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.viewCreated()

    }

    override fun createCard() {
        val intent = Intent(activity, CreateCardActivity::class.java)
        intent.putExtra("type", CreateCardActivity.Type.CREATE_FROM_MENU_CARD)
        activity?.startActivityForResult(intent, Config.RequestCode.CREATE_NEW_CARD_REQUEST_CODE)
    }


    override fun initStateListener() {
        multiStateLayout.setOnStateViewCreatedListener { view, state ->

            when (state) {
                MultiStateLayout.State.NETWORK_ERROR -> {
                    val retry = view.info_no_internet_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }

                MultiStateLayout.State.ERROR -> {
                    val retry = view.something_went_wrong_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }

            }
        }

    }


    override fun updateAdapter() {
        cardAdapter.notifyDataSetChanged()
    }

    override fun showLoading() {
        multiStateLayout.state = MultiStateLayout.State.LOADING
    }


    override fun showContent() {
        multiStateLayout.state = MultiStateLayout.State.CONTENT
    }

    override fun showSomethingWentWrong() {
        multiStateLayout.state = MultiStateLayout.State.ERROR
    }

    override fun showNoInternetConnection() {
        multiStateLayout.state = MultiStateLayout.State.NETWORK_ERROR
    }

    override fun initRecyclerView() {
        val manager = CustomLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        cardAdapter = CardAdapter(presenter)
        cardRecycler.layoutManager = manager
        cardRecycler.adapter = cardAdapter
    }

    override fun removeCard(position: Int) {
        cardAdapter.acceptRemoveCard(position)
    }


    override fun addItems(list: List<CardBody>) {
        cardAdapter.addItem(list)
    }

    override fun updateItems(list: ArrayList<CardBody>) {
        val productDiffUtilCallback = CardDiffUtilCallback(cardAdapter.getData(), list)
        val productDiffResult = DiffUtil.calculateDiff(productDiffUtilCallback)
        cardAdapter.updateData(list)
        productDiffResult.dispatchUpdatesTo(cardAdapter)
    }


}