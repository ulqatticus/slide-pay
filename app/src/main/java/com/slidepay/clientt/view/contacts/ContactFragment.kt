package com.slidepay.clientt.view.contacts

import android.os.Bundle
import android.provider.ContactsContract
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import cn.refactor.multistatelayout.MultiStateLayout
import com.slidepay.clientt.R
import com.slidepay.clientt.contracts.contacts.ContactContract
import com.slidepay.clientt.model.objects.main.ContactsBody
import com.slidepay.clientt.model.objects.main.ContactsBodySend
import com.slidepay.clientt.presenter.contacts.ContactsPresenter
import com.slidepay.clientt.view.mainpage.transaction.CustomLayoutManager
import com.slidepay.clientt.view.widgets.ProgressDialog
import com.slidepay.clientt.view.widgets.TextTypeface
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration
import kotlinx.android.synthetic.main.contacts_activity.*
import kotlinx.android.synthetic.main.info_no_internet.view.*
import kotlinx.android.synthetic.main.info_something_went_wrong.view.*

class ContactFragment : Fragment(), ContactContract.View {


    private val multiStateLayout: MultiStateLayout by lazy { multi_state_layout }
    val presenter = ContactsPresenter(this)
    private lateinit var adapter: ContactAdapter
    private val recycler: RecyclerView by lazy { contacts_list }
    private val closeBtn: ImageView by lazy { contacts_close_btn }
    private lateinit var retry: TextTypeface
    private var progressDialog: ProgressDialog = ProgressDialog()


    var onOkClick: (String) -> Unit = {}


    companion object {
        fun create(onOkClick: (String) -> Unit = {}): Fragment {
            return ContactFragment().apply {
                this.onOkClick = onOkClick
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.contacts_activity, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.viewCreated()

    }

    override fun initContactList() {
        adapter = ContactAdapter(presenter)
        val manager = CustomLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recycler.layoutManager = manager
        recycler.adapter = adapter
        val headersDecor = StickyRecyclerHeadersDecoration(adapter)
        recycler.addItemDecoration(headersDecor)
        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                headersDecor.invalidateHeaders()
            }
        })
    }

    override fun addItems(arrayList: ArrayList<ContactsBody>) {
        adapter.addContacts(arrayList)
    }


    override fun pickedContact(number: String) {
        onOkClick(number)
        finishFragment()
    }

    private fun finishFragment() {
        fragmentManager?.popBackStackImmediate()
    }


    override fun initCloseBtnListener() {
        closeBtn.setOnClickListener { finishFragment() }
    }

    override fun showLoading() {
        progressDialog.start(fragmentManager!!)
    }

    override fun hideLoading() {
        progressDialog.dismiss()
    }

    override fun initMultiStateListener() {
        multiStateLayout.setOnStateViewCreatedListener { view, state ->

            when (state) {
                MultiStateLayout.State.NETWORK_ERROR -> {
                    retry = view.info_no_internet_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }

            }
        }
    }

    override fun getContacts() {
        val list = getSystemContacts()
        presenter.getContacts(list)

    }

    private fun getSystemContacts(): ArrayList<ContactsBodySend> {
        val peopleArray = ArrayList<ContactsBodySend>()

        val uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        val projection = arrayOf(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER)


        /*    val people = view.getContext().contentResolver.query(
                    uri,
                    projection,
                    RawContacts.ACCOUNT_TYPE + "= ?",
                    arrayOf("com.whatsapp"),
                    null)*/

        val people = requireContext().contentResolver.query(uri, projection, null, null, null)

        val indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
        val indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)

        if (people.moveToFirst()) {
            do {
                val name = people.getString(indexName)
                val number = people.getString(indexNumber)
                peopleArray.add(ContactsBodySend(number, name))

            } while (people.moveToNext())
        }
        return peopleArray
    }


    override fun showSomethingWentWrong() {
        multiStateLayout.state = MultiStateLayout.State.ERROR
    }

    override fun showContent() {
        multiStateLayout.state = MultiStateLayout.State.CONTENT
    }

    override fun showNoInternetConnection() {
        multiStateLayout.state = MultiStateLayout.State.NETWORK_ERROR
    }


    override fun initStateListener() {
        multiStateLayout.setOnStateViewCreatedListener { view, state ->

            when (state) {
                MultiStateLayout.State.NETWORK_ERROR -> {
                    retry = view.info_no_internet_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }
                MultiStateLayout.State.ERROR -> {
                    val retry = view.something_went_wrong_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }

            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.viewDestroyed()
    }
}