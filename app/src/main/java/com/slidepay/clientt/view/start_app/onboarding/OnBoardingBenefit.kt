package com.slidepay.clientt.view.start_app.onboarding

import android.support.annotation.DrawableRes
import android.support.annotation.StringRes

import com.slidepay.clientt.R

enum class OnBoardingBenefit constructor(@param:StringRes @get:StringRes val textId: Int?,
                                         @param:DrawableRes @get:DrawableRes val drawableId: Int?) {

    ADD_CARD(R.string.onborading_add_card_text, R.drawable.onborading_card_img),
    ADD_QR_CODE(R.string.onborading_add_scanqr_text, R.drawable.onborading_scan_qr_img),
    ADD_INVOICE(R.string.onborading_add_make_invoice_text, R.drawable.onborading_make_invoice_img),
    ADD_EMPTY_PAGE(null, null)
}
