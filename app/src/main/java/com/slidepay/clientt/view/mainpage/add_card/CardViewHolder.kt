package com.slidepay.clientt.view.mainpage.add_card

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RadioButton
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.view.widgets.EditTextTypeface
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.card_item.view.*


class CardViewHolder(itemView: View, var adapter: CardAdapter) : RecyclerView.ViewHolder(itemView) {


    private val cardName: EditTextTypeface by lazy { itemView.card_name }
    private val cardBankName: TextTypeface by lazy { itemView.card_bank_name }

    val primaryRadioBtn: RadioButton by lazy { itemView.card_select_default }

    private val idCard: TextTypeface by lazy { itemView.card_bank_id }

    private val editCardBtn: ImageView by lazy { itemView.card_change_btn }
    private val deleteCardBtn: ImageView by lazy { itemView.card_delete_btn }
    private val radioButton: RadioButton by lazy { itemView.card_select_default }
    private val confirmEditName: FrameLayout by lazy { itemView.card_change_btn_agree }


    fun bind(card: CardBody) {
        cardName.setText(card.name)
        cardBankName.text = card.cardBankName
        idCard.text = card.number

        radioButton.isChecked = card.isPrimary!!

        /*   if (card.isPrimary!!) {
               adapter.lastSelectedPosition = adapterPosition.minus(1)
           }*/
        primaryRadioBtn.setOnClickListener {
            // adapter.lastSelectedPosition = adapterPosition.minus(1)
            adapter.setDefaultCard(card.id)
        }



        editCardBtn.setOnClickListener {
            cardName.isCursorVisible = true
            cardName.isFocusableInTouchMode = true
            cardName.requestFocus()
            cardName.setSelection(cardName.text.length)
            confirmEditName.visibility = View.VISIBLE

            adapter.showKeyboard()
        }



        confirmEditName.setOnClickListener {
            confirmEditName.visibility = View.GONE
            cardName.isCursorVisible = false
            cardName.isFocusableInTouchMode = false
            adapter.renameCard(card.id, cardName.text.toString())
            adapter.hideKeyboard()

        }

        deleteCardBtn.setOnClickListener {
            adapter.removeCard(card.id, adapterPosition - 1)
        }

    }


}

