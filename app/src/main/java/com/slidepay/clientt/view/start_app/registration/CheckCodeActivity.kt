package com.slidepay.clientt.view.start_app.registration

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.SpannableString
import android.text.TextWatcher
import android.view.View
import android.widget.TextView
import cn.refactor.multistatelayout.MultiStateLayout
import com.slidepay.clientt.R
import com.slidepay.clientt.contracts.start.CheckCodeContract
import com.slidepay.clientt.presenter.start.registration.CheckCodePresenter
import com.slidepay.clientt.view.BaseActivity
import com.slidepay.clientt.view.modals.ModalBenefit
import com.slidepay.clientt.view.modals.PaymentModalFragment
import com.slidepay.clientt.view.start_app.registration.password.PasswordActivity
import com.slidepay.clientt.view.widgets.ButtonTypeface
import com.slidepay.clientt.view.widgets.EditTextTypeface
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.check_code_activity.*
import kotlinx.android.synthetic.main.info_no_internet.view.*
import kotlinx.android.synthetic.main.info_something_went_wrong.view.*


class CheckCodeActivity : BaseActivity(), CheckCodeContract.View {

    override fun initStateListener() {
        multiStateLayout.setOnStateViewCreatedListener { view, state ->

            when (state) {
                MultiStateLayout.State.NETWORK_ERROR -> {
                    val  retry = view.info_no_internet_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }
                MultiStateLayout.State.ERROR -> {
                    val retry = view.something_went_wrong_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }

            }
        }
    }


    private var presenter = CheckCodePresenter(this)


    private val editCode: EditTextTypeface by lazy { confirmation_screen_activity_code }
    private val codeNotComeBtn: TextTypeface by lazy { confirmation_code_didnt_come }
    private val timer: TextTypeface by lazy { confirmation_code_timer }
    private val changeNumberBtn: TextTypeface by lazy { confirmation_change_number }
    private val phoneLabel: TextTypeface by lazy { confirmation_screen_activity_user_phone }
    private val nextBtn: ButtonTypeface by lazy { confirmation_code_next_btn }
    private val textLabel: TextTypeface by lazy { confirmation_screen_activity_text }
    private val multiStateLayout: MultiStateLayout by lazy { multi_state_layout }


    private lateinit var phoneNumber: String
    private lateinit var type: CheckCodeActivity.VereficationType

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.check_code_activity)
        presenter.viewCreated()
        initGetExtra()
        initSetTextLabel()
        initEditCode()
        initCodeNotComeBtnListener(codeNotComeBtn)
        initChangeNumberBtnListener(changeNumberBtn)
        initNextButtonListener(nextBtn)

    }

    private fun initSetTextLabel() {

        if (type == CheckCodeActivity.VereficationType.RESTORE_PASS) {
            textLabel.text = getString(R.string.restore_pass_text)
        }
    }

    private fun initNextButtonListener(nextBtn: ButtonTypeface) {
        val code = editCode.text.toString()
        nextBtn.setOnClickListener { presenter.checkCode(code) }
    }

    private fun initGetExtra() {
        phoneNumber = intent.getStringExtra("phone")
        phoneLabel.text = phoneNumber

        type = intent.getSerializableExtra("type") as CheckCodeActivity.VereficationType
    }

    override fun getPhoneNUmber(): String {
        return phoneNumber
    }

    private fun initChangeNumberBtnListener(changeNumberBtn: TextTypeface) {
        changeNumberBtn.setOnClickListener {
            presenter.closeActivity()
            finish()
        }

    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    private fun initCodeNotComeBtnListener(codeNotComeBtn: TextTypeface) {
        codeNotComeBtn.setOnClickListener {

            presenter.startTimer()
            presenter.sentCodeAgain()

        }
    }

    private fun initEditCode() {
        editCode.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val text = s.toString()
                presenter.afterTextChanged(text)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                hideError()
            }

        })
        editCode.setOnClickListener {
            editCode.text?.length?.let { it1 -> editCode.setSelection(it1) }
        }
    }

    override fun editCodeSetText(newText: StringBuilder) {
        editCode.setText(newText)
    }

    override fun editCodeSetSelection(length: Int) {
        editCode.setSelection(length)
    }

    override fun nextActivity() {

        when (type) {
            CheckCodeActivity.VereficationType.RESTORE_PASS -> {
                PasswordActivity.restorePinCode(this)
                finish()

            }
            CheckCodeActivity.VereficationType.VERIFY_PHONE_NUMBER -> {
                RegistrationModalActivity.startMessagesRules(this)
                finish()
            }
        }

    }



    override fun closeApp() {
        finishAffinity()
    }


    override fun showLoading() {
        super.showProgress()
    }

    override fun hideLoading() {
        super.hideProgress()
    }

    override fun showKeyBoard() {
        super.showKeyboard()
    }

    override fun hideKeyBoard() {
        super.hideKeyboard()
    }

    override fun setTimerSeconds(spannable: SpannableString) {
        timer.setText(spannable, TextView.BufferType.SPANNABLE);
    }

    override fun setTimerVisible() {
        timer.visibility = View.VISIBLE
    }

    override fun setTimerInvisible() {
        timer.visibility = View.GONE
    }

    override fun setResendBtnVisible() {
        codeNotComeBtn.visibility = View.VISIBLE
    }

    override fun setResendBtnInvisible() {
        codeNotComeBtn.visibility = View.INVISIBLE
    }

    override fun hideError() {
        editCode.setBackgroundResource(R.drawable.backround_field_blue)
    }

    override fun showError() {
        editCode.setBackgroundResource(R.drawable.background_error_edit)

    }

    override fun startSuccessFragment() {
       /* supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .add(R.id.confirmation_code_fragment, PaymentModalFragment.create(ModalBenefit.RESEND_CODE))
                .commit()*/

    }

    override fun showSomethingWentWrong() {
        multiStateLayout.state = MultiStateLayout.State.ERROR
    }

    override fun showNoInternetConnection() {
        multiStateLayout.state = MultiStateLayout.State.NETWORK_ERROR
    }

    override fun showContent() {
        multiStateLayout.state = MultiStateLayout.State.CONTENT
    }

    override fun getContext(): Context? {
        return this
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.viewDestroyed()

    }

    enum class VereficationType {
        RESTORE_PASS, VERIFY_PHONE_NUMBER
    }


}