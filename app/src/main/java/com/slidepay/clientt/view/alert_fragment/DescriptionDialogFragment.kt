package com.slidepay.clientt.view.alert_fragment

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.slidepay.clientt.R
import com.slidepay.clientt.util.stringText
import kotlinx.android.synthetic.main.description_activity.*


class DescriptionDialogFragment : DialogFragment() {

    companion object {
        private const val TAG = "DescriptionDialog"

        fun newInstance(description: String?, onOkClick: (String) -> Unit = {}): DescriptionDialogFragment {
            return DescriptionDialogFragment().apply {
                this.onOkClick = onOkClick
                this.description = description
            }
        }
    }


    var onOkClick: (String) -> Unit = {}
    var description: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.description_activity, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!description.isNullOrEmpty()) {
            description_text.setText(description)
            description_text.setSelection(description!!.length)
            description_text.requestFocus()
        }

        description_next_btn.setOnClickListener {
            onOkClick(description_text.stringText())
            dismiss()
        }
        dsc_close_btn.setOnClickListener { dismiss() }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.requestFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            window?.setSoftInputMode(4)
        }
    }

    fun show(childFragmentManager: FragmentManager) {
        super.show(childFragmentManager, TAG)
    }


}
