package com.slidepay.clientt.view.mainpage.profile

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.slidepay.clientt.R
import com.slidepay.clientt.application.utils.Config
import com.slidepay.clientt.model.database.TokenDataBase
import com.slidepay.clientt.view.start_app.StartActivity
import com.slidepay.clientt.view.widgets.ButtonTypeface
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.logout_fragmnet.*

class LogoutFragment : Fragment() {

    private val cancelBtn: TextTypeface by lazy { accept_logout_cancel_btn }
    private val logoutBtn: ButtonTypeface by lazy { accept_logout_btn }


    companion object {
        fun create(): Fragment = LogoutFragment()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.logout_fragmnet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initCancelBtn()
        initLogoutBtn()
    }

    private fun initLogoutBtn() {
        logoutBtn.setOnClickListener {
            logout()
        }
    }

    private fun initCancelBtn() {
        cancelBtn.setOnClickListener {
            fragmentManager?.popBackStackImmediate()
        }
    }

    private fun logout() {
        TokenDataBase.removeClientToken()
        val sharedPrefs = requireContext().getSharedPreferences(Config.Preferences.baseName, Context.MODE_PRIVATE)
        val editor = sharedPrefs.edit()
        editor.clear()
        editor.apply()
        startActivity(Intent(requireContext(), StartActivity::class.java))
        requireActivity().finishAffinity()
    }


}