package com.slidepay.clientt.view.contacts

import android.support.v7.widget.RecyclerView
import android.view.View
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.item_contacts_heder.view.*

class ContactsHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


    private val contactsHeaderLetter: TextTypeface by lazy { itemView.contacts_header_letter }


    fun bind(letter: String) {
        contactsHeaderLetter.text = letter
    }

}