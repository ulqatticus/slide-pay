package com.slidepay.clientt.view

import android.app.Activity
import android.content.Context
import android.support.v4.app.Fragment
import android.view.View
import android.view.inputmethod.InputMethodManager

abstract class BaseFragment : Fragment() {
    private var isShowKeyboard = false

    fun showKeyboard() {
        if (!isShowKeyboard) {
            val imm = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
            isShowKeyboard = true
        }
    }

    fun hideKeyboard() {
        val inputMethodManager = requireContext().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = requireActivity().currentFocus
        if (view == null) {
            view = View(requireContext())
        }
        isShowKeyboard = false
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}