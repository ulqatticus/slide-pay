package com.slidepay.clientt.view.mainpage

import android.support.design.widget.TabLayout
import com.slidepay.clientt.contracts.main.MainPageContract

class TabListener(private var activity: MainPageContract.View) : TabLayout.OnTabSelectedListener {
    override fun onTabReselected(tab: TabLayout.Tab?) {

    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        activity.setTabItem(tab)
    }

}
