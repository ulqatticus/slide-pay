package com.slidepay.clientt.view.mainpage.transaction

import android.annotation.SuppressLint
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.method.ScrollingMovementMethod
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import com.slidepay.clientt.R
import com.slidepay.clientt.model.objects.main.TransactionBody
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.item_transaction_blue.view.*
import kotlinx.android.synthetic.main.item_transaction_green.view.*


class TransactionSendMoneyViewHolder(itemView: View, private var transactionsAdapter: TransactionsAdapter) : RecyclerView.ViewHolder(itemView) {


    private val transactionCircleStatusIcon: ImageView by lazy { itemView.transaction_sendmoney_item_status }


    private val transactionTime: TextTypeface by lazy { itemView.transaction_sendmoney_item_time }
    private val transactionDate: TextTypeface by lazy { itemView.transaction_sendmoney_item_date }
    private val transactionName: TextTypeface by lazy { itemView.transaction_sendmoney_client_name }
    private val transactionPrice: TextTypeface by lazy { itemView.transaction_price }
    private val transactionDescription: TextTypeface by lazy { itemView.transaction_sendmoney_description }
    private val transactionTextStatus: TextTypeface by lazy { itemView.transaction_sendmoney_item_text_status }
    private val transactionErrorText: TextTypeface by lazy { itemView.transaction_sendmoney_error_text }

    private val cancelBtn: TextTypeface by lazy { itemView.transaction_sendmoney_cancel_btn }
    private val retryTransactionBtn: TextTypeface by lazy { itemView.transaction_sendmoney_resend_btn }
    private val payTransactionBtn: TextTypeface by lazy { itemView.transaction_sendmoney_pay_btn }
    private val expandableLayout: LinearLayout by lazy { itemView.transaction_sendmoney_expanded_item_body }


    // private lateinit var builder: StringBuilder


    @SuppressLint("ClickableViewAccessibility")
    fun bind(transaction: TransactionBody) {

        transactionPrice.text = transaction.sum
        transactionDate.text = transaction.date
        transactionTime.text = transaction.time
        transactionName.text = transaction.name

        val isExpanded = adapterPosition.minus(1) == transactionsAdapter?.mExpandedPosition;
        if (isExpanded) {
            expandableLayout.visibility = View.VISIBLE
            if (adapterPosition.minus(1) > 1)
                transactionsAdapter.scrollToPosition(adapterPosition.minus(1))
        } else {
            expandableLayout.visibility = View.GONE
        }
        if (isExpanded) {
            transactionsAdapter.previousExpandedPosition = adapterPosition.minus(1)
        }


        if (transaction.sum == null) {
            transaction.sum = "0.00"

        }

        transactionPrice.text = itemView.context.getString(R.string.price_type, if (transaction.isInvoice!!) "-" else "+", transaction.sum, "грн")



        if (!transaction.errorDescription.isNullOrEmpty()) {
            transactionErrorText.visibility = View.VISIBLE
            transactionErrorText.text = transaction.errorDescription
        }

        if (transaction.description.isNullOrEmpty()) {
            transactionDescription.visibility = View.GONE
        } else {
            transactionDescription.text = transaction.description
        }
        transactionDescription.movementMethod = ScrollingMovementMethod()

        setStatusInfo(transaction.status, transaction.isInvoice!!)

        itemView.setOnClickListener {
            transactionsAdapter.mExpandedPosition = if (isExpanded) -1 else adapterPosition.minus(1)
            transactionsAdapter.notifyItemChanged(transactionsAdapter!!.previousExpandedPosition)
            transactionsAdapter.notifyItemChanged(adapterPosition.minus(1))
        }

        transactionDescription.setOnTouchListener { v, event ->
            when (event?.action) {
                MotionEvent.ACTION_DOWN ->
                    // Disallow ScrollView to intercept touch events.
                    v?.parent?.requestDisallowInterceptTouchEvent(true)

                MotionEvent.ACTION_UP ->
                    // Allow ScrollView to intercept touch events.
                    v?.parent?.requestDisallowInterceptTouchEvent(false)
            }

            v?.onTouchEvent(event)
            true
        }

        cancelBtn.setOnClickListener {
            transactionsAdapter?.cancelGetMoney(transaction.number)
        }

        retryTransactionBtn.setOnClickListener {
            transactionsAdapter?.retryGetMoney(transaction.number)
        }
        payTransactionBtn.setOnClickListener {
            transactionsAdapter.payInvoice(transaction)
        }
    }

    /*
     Wait = 0,
     Success = 1,
     Error = 2,
     Canceled = 3,
    */
    private fun setStatusInfo(status: Int?, spent: Boolean) {
        hideAll()
        when (status) {
            0 -> {
                setTextAndImageColor("Ожидание", "#D5D7D8", "#D5D7D8")
                if (spent) {
                    cancelBtn.visibility = View.VISIBLE
                    payTransactionBtn.visibility = View.VISIBLE
                }
            }

            1 -> {
                setTextAndImageColor("Получено", "#44DC93", "#44DC93")
            }

            2 -> {
                setTextAndImageColor("Ошибка", "#F56D8E", "#D5D7D8")
                if (spent) {
                    cancelBtn.visibility = View.VISIBLE
                    retryTransactionBtn.visibility = View.VISIBLE
                }
            }

            3 -> {
                setTextAndImageColor("Отменен", "#F56D8E", "#D5D7D8")
            }
        }
    }

    private fun hideAll() {
        retryTransactionBtn.visibility = View.GONE
        cancelBtn.visibility = View.GONE
        payTransactionBtn.visibility = View.GONE
    }


    private fun setTextAndImageColor(textStatus: String, colorStatusColor: String, colorText: String) {
        transactionTextStatus.text = textStatus
        transactionCircleStatusIcon.setColorFilter(Color.parseColor(colorStatusColor))
        transactionPrice.setTextColor(Color.parseColor(colorText))
    }


}