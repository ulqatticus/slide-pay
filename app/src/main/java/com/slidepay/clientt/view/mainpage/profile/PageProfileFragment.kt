package com.slidepay.clientt.view.mainpage.profile

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.DatePicker
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import cn.refactor.multistatelayout.MultiStateLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.slidepay.clientt.R
import com.slidepay.clientt.contracts.main.profile.ProfileContract
import com.slidepay.clientt.model.objects.main.ProfileBody
import com.slidepay.clientt.presenter.mainpage.profile.ProfilePresenter
import com.slidepay.clientt.view.BaseFragment
import com.slidepay.clientt.view.mainpage.PageActivity
import com.slidepay.clientt.view.widgets.EditTextTypeface
import com.slidepay.clientt.view.widgets.TextTypeface
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlinx.android.synthetic.main.info_no_internet.view.*
import kotlinx.android.synthetic.main.info_something_went_wrong.view.*


class PageProfileFragment : BaseFragment(), ProfileContract.View, DatePickerDialog.OnDateSetListener {


    val presenter = ProfilePresenter(this)


    private lateinit var saveChangesBtn: TextTypeface
    private lateinit var profileName: EditTextTypeface
    private lateinit var profileBirthday: EditTextTypeface
    private lateinit var profileEmail: EditTextTypeface
    private lateinit var profileImage: CircleImageView
    private lateinit var logoutBtn: ImageView
    private lateinit var multiStateLayout: MultiStateLayout
    private lateinit var progressBar: ProgressBar


    private lateinit var changeProfilePassBtn: TextTypeface


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_profile, container, false)

        logoutBtn = rootView.profile_logout_icon
        profileName = rootView.profile_name
        profileEmail = rootView.profile_email
        saveChangesBtn = rootView.profile_edit_profile_btn
        changeProfilePassBtn = rootView.profile_edit_pass_btn
        profileImage = rootView.profile_image_ic
        profileBirthday = rootView.profile_birthday
        multiStateLayout = rootView.multi_state_layout
        progressBar = rootView.profile_image_progress
        return rootView
    }

    override fun initStateListener() {
        multiStateLayout.setOnStateViewCreatedListener { view, state ->

            when (state) {
                MultiStateLayout.State.NETWORK_ERROR -> {
                    val retry = view.info_no_internet_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }
                MultiStateLayout.State.ERROR -> {
                    val retry = view.something_went_wrong_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }

            }
        }
    }



    override fun setListenerToLogout() {
        logoutBtn.setOnClickListener {
            startLogoutFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.viewCreated()
        initListeners()

    }


    override fun startChangePassFragmnet() {
        childFragmentManager.beginTransaction()
                .add(R.id.profile_container, ChangePasswordFragment.create(), ChangePasswordFragment::class.java.name)
                .addToBackStack(ChangePasswordFragment::class.java.name)
                .commit()
    }

    override fun startLogoutFragment() {
        childFragmentManager
                .beginTransaction()
                .add(R.id.profile_container, LogoutFragment.create(), LogoutFragment::class.java.name)
                .addToBackStack(LogoutFragment::class.java.name)
                .commit()
    }



    private fun initListeners() {
        saveChangesBtn.setOnClickListener {
            val body = createBody()
            presenter.saveChangesToServer(body)
        }
        changeProfilePassBtn.setOnClickListener {
            startChangePassFragmnet()
        }


        profileImage.setOnClickListener {
            hideKeyboard()
            Dexter.withActivity(getActivityContext())
                    .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                    .withListener(object : MultiplePermissionsListener {
                        override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                            token?.continuePermissionRequest()

                        }

                        override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                            presenter.addImageVia()

                        }

                    }).check()

        }

        profileEmail.setOnClickListener {
            profileEmail.isCursorVisible = true
            profileEmail.isFocusableInTouchMode = true
            profileEmail.requestFocus()

            profileEmail.setSelection(profileEmail.length())

        }

        profileName.setOnClickListener {
            profileName.isCursorVisible = true
            profileName.isFocusableInTouchMode = true
            profileName.requestFocus()

            profileName.setSelection(profileName.length())

        }

        profileBirthday.setOnClickListener {
            activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            profileEmail.isCursorVisible = false
            profileEmail.isCursorVisible = false
            hideKeyboard()
            val dataPicker = DatePickerDialog(activity, AlertDialog.THEME_HOLO_LIGHT, this, 2000, 0, 1)
            dataPicker.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dataPicker.show()
        }
    }

    private fun createBody(): ProfileBody {
        val body = ProfileBody()
        body.email = profileEmail.text.toString()
        body.fullname = profileName.text.toString()
        body.birthday = profileBirthday.text.toString()
        return body
    }

    override fun showSomethingWentWrong() {
        multiStateLayout.state = MultiStateLayout.State.ERROR
    }

    override fun showContent() {
        multiStateLayout.state = MultiStateLayout.State.CONTENT
    }

    override fun showNoInternetConnection() {
        multiStateLayout.state = MultiStateLayout.State.NETWORK_ERROR
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
        val sb = StringBuilder()
        val myDay = getCorrectDay(day)
        val myMonth = getCorrectMount(month)

        sb.append(myDay)
                .append(".")
                .append(myMonth)
                .append(".")
                .append(year)
        profileBirthday.setText(sb, TextView.BufferType.EDITABLE)
    }

    private fun getCorrectDay(day: Int): String {
        return if (day < 10) {
            "0$day"
        } else {
            day.toString()
        }

    }

    private fun getCorrectMount(month: Int): String {
        return if (month.plus(1) < 10) {
            "0" + month.plus(1).toString()
        } else {
            month.plus(1).toString()
        }
    }

    override fun getActivityContext(): Activity {
        return activity as PageActivity
    }


    override fun setImageToProfile(bitmap: Bitmap?) {
        profileImage.setImageBitmap(bitmap)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        presenter.onActivityResult(requestCode, resultCode, data)
    }

    override fun setInformation(body: ProfileBody) {
        profileName.setText(body.fullname)
        profileEmail.setText(body.email)
        profileBirthday.setText(body.birthday)

        if (!body.imageURL.isNullOrEmpty()) {
            Glide.with(this).load(body.imageURL).listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                    progressBar.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                    progressBar.visibility = View.GONE
                    return false
                }
            }).into(profileImage)
        } else {
            progressBar.visibility = View.GONE
        }
    }

    override fun showLoading() {
        multiStateLayout.state = MultiStateLayout.State.LOADING

    }

}