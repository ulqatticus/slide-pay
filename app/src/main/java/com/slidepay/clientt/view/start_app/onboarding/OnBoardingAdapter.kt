package com.slidepay.clientt.view.start_app.onboarding



import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class OnBoardingAdapter(fm: FragmentManager, private val mBenefits: List<OnBoardingBenefit>) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {

        return OnBoardingBenefitFragment.create(mBenefits[position])
    }

    override fun getCount(): Int {
        return mBenefits.size
    }
}
