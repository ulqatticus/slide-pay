package com.slidepay.clientt.view.start_app.registration.type_number

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.widget.CompoundButtonCompat
import android.text.Editable
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import cn.refactor.multistatelayout.MultiStateLayout
import com.slidepay.clientt.R
import com.slidepay.clientt.contracts.start.TypeNumberContract
import com.slidepay.clientt.presenter.start.type_number.TypeNumberPresenter
import com.slidepay.clientt.view.BaseActivity
import com.slidepay.clientt.view.start_app.registration.CheckCodeActivity
import com.slidepay.clientt.view.widgets.ButtonTypeface
import com.slidepay.clientt.view.widgets.EditTextTypeface
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.info_no_internet.view.*
import kotlinx.android.synthetic.main.info_something_went_wrong.view.*
import kotlinx.android.synthetic.main.type_number_activity.*


class TypeNumberActivity : BaseActivity(), TypeNumberContract.View {


    private val nextBtn: ButtonTypeface by lazy { type_your_number_next_btn }
    private val numberField: EditTextTypeface by lazy { type_your_number_count_number }
    private val errorView: LinearLayout by lazy { type_your_number_count_edit_num_block }
    private val errorImg: ImageView by lazy { type_your_number_error_img }
    private val checkBox: CheckBox by lazy { type_your_number_checkbox }
    private val spanText: TextTypeface by lazy { type_your_number_text_agree }
    private val multiStateLayout: MultiStateLayout by lazy { multi_state_layout }

    private var presenter = TypeNumberPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.type_number_activity)
        presenter.viewCreated()
        initNumberField()
        initNextBtn()
        initCheckBox(checkBox)
        initSpanText(spanText)
    }

    override fun initStateListener() {
        multiStateLayout.setOnStateViewCreatedListener { view, state ->

            when (state) {
                MultiStateLayout.State.NETWORK_ERROR -> {
                    val retry = view.info_no_internet_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }
                MultiStateLayout.State.ERROR -> {
                    val retry = view.something_went_wrong_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }

            }
        }
    }



    private fun initCheckBox(checkBox: CheckBox) {
        checkBox.setOnClickListener {
            checkBoxHideError()
            hideKeyBoard()
        }
    }

    override fun initSpanText(spanText: TextTypeface) {

        val text = presenter.getSpanString()
        spanText.text = text
        spanText.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun nextActivity() {
        val phone = presenter.getCorrectPhoneNumber()
        val intent = Intent(this, CheckCodeActivity::class.java)
        intent.putExtra("phone", phone)
        intent.putExtra("type", CheckCodeActivity.VereficationType.VERIFY_PHONE_NUMBER)

        startActivity(intent)
    }

    override fun getNumber(): String {
        return numberField.text.toString()
    }

    private fun initNumberField() {
        numberField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable?) {
                presenter.watchOnTextView(text.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, start: Int, before: Int, count: Int) {

                /*      val digits = phoneNumberEditText.getText().toString().length()
                      if (beforeLength < digits && (digits == 3 || digits == 7)) {
                          phoneNumberEditText.append("-")
                      }*/
            }
        })

        numberField.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            presenter.settingNumberField(event, actionId)
            return@OnEditorActionListener false
        })
    }


    override fun initNextBtn() {
        nextBtn.setOnClickListener {
            presenter.clickNextBtn()
        }
    }

    override fun showKeyBoard() {
        super.showKeyboard()
    }

    override fun hideKeyBoard() {
        super.hideKeyboard()
    }

    override fun showLoading() {
        super.showProgress()
    }

    override fun hideLoading() {
        super.hideProgress()
    }



    override fun showSomethingWentWrong() {
        multiStateLayout.state = MultiStateLayout.State.ERROR
    }

    override fun showNoInternetConnection() {
        multiStateLayout.state = MultiStateLayout.State.NETWORK_ERROR
    }

    override fun showContent() {
        multiStateLayout.state = MultiStateLayout.State.CONTENT
    }


    override fun checkBoxIsChecked(): Boolean {
        return checkBox.isChecked
    }

    override fun checkBoxShowError() {
        if (Build.VERSION.SDK_INT < 21) {
            CompoundButtonCompat.setButtonTintList(checkBox, ColorStateList.valueOf(resources.getColor(R.color.red)))
        } else {
            checkBox.buttonTintList = ColorStateList.valueOf(resources?.getColor(R.color.red)!!)

        }

    }

    private fun checkBoxHideError() {
        if (Build.VERSION.SDK_INT < 21) {
            CompoundButtonCompat.setButtonTintList(checkBox, ColorStateList.valueOf(resources.getColor(R.color.blue)))
        } else {
            checkBox.buttonTintList = ColorStateList.valueOf(resources?.getColor(R.color.blue)!!)

        }
    }

    override fun startWebFragment() {
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .add(R.id.type_your_number_web_block, TermsOfUseWebFragment.create())
                .commit()


    }

    override fun getContext(): Context {
        return this
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.viewDestroyed()
    }

    override fun hideErrorPhone() {
        errorImg.visibility = View.GONE
        errorView.background = ContextCompat.getDrawable(this, R.drawable.style_linnerlayout)
    }

    override fun showErrorPhone() {
        errorImg.visibility = View.VISIBLE
        errorView.background = ContextCompat.getDrawable(this, R.drawable.background_error_edit)
    }


}