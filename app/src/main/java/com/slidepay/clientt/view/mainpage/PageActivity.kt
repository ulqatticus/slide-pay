package com.slidepay.clientt.view.mainpage

import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Process
import android.support.design.widget.TabLayout
import android.support.v4.app.FragmentManager
import android.support.v4.view.ViewPager
import android.widget.LinearLayout
import android.widget.Toast
import cn.refactor.multistatelayout.MultiStateLayout
import com.slidepay.clientt.R
import com.slidepay.clientt.application.utils.Config
import com.slidepay.clientt.contracts.main.MainPageContract
import com.slidepay.clientt.model.objects.main.ScannedBody
import com.slidepay.clientt.presenter.mainpage.pager.MainPageAdapter
import com.slidepay.clientt.presenter.sendmoney.SendMoneyType
import com.slidepay.clientt.view.BaseActivity
import com.slidepay.clientt.view.mainpage.pager.MainPagePresenter
import com.slidepay.clientt.view.mainpage.payment.PaymentFragment
import com.slidepay.clientt.view.scanner.MainPageListener
import com.slidepay.clientt.view.scanner.ScannerFragment
import com.slidepay.clientt.view.sendmoney.SendMoneyFragment
import kotlinx.android.synthetic.main.info_no_internet.view.*
import kotlinx.android.synthetic.main.info_something_went_wrong.view.*
import kotlinx.android.synthetic.main.main_page_activity.*


class PageActivity : BaseActivity(), MainPageContract.View, MainPageListener {


    override fun initStateListener() {
        multiStateLayout.setOnStateViewCreatedListener { view, state ->

            when (state) {
                MultiStateLayout.State.NETWORK_ERROR -> {
                    val retry = view.info_no_internet_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }
                MultiStateLayout.State.ERROR -> {
                    val retry = view.something_went_wrong_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }

            }
        }
    }


    private val tabLayout: TabLayout by lazy { main_page_sliding_tabs }
    private val viewPager: ViewPager by lazy { main_page_view_pager }
    private val multiStateLayout: MultiStateLayout by lazy { multi_state_layout }

    private var presenter = MainPagePresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_page_activity)
        initUIComponent()
    }

    override fun closeApp() {
        finishAffinity()
    }

    override fun initUIComponent() {
        viewPager.adapter = MainPageAdapter(supportFragmentManager, tabLayout.tabCount)
        tabLayout.addOnTabSelectedListener(TabListener(this))
        setDivider(tabLayout, resources.getDimension(R.dimen.tab_divider_padding).toInt())
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        viewPager.setCurrentItem(2, false) //set third item by default
    }


    override fun setDivider(tabLayout: TabLayout, padding: Int) {
        val root = tabLayout.getChildAt(0)
        if (root is LinearLayout) {
            root.showDividers = LinearLayout.SHOW_DIVIDER_MIDDLE
            val drawable = GradientDrawable()
            drawable.setColor(resources.getColor(R.color.grey_divider))
            drawable.setSize(3, 1)
            root.dividerPadding = padding
            root.dividerDrawable = drawable
        }
    }


    fun showLoading() {
        super.showProgress()
    }

    fun hideLoading() {
        super.hideProgress()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            Config.RequestCode.CREATE_NEW_CARD_REQUEST_CODE -> {
                val fragment = supportFragmentManager.findFragmentByTag("android:switcher:" + R.id.main_page_view_pager + ":" + viewPager.currentItem)
                fragment?.onActivityResult(requestCode, resultCode, data)
            }

            Config.RequestCode.LOAD_IMG_REQUEST_CODE -> {
                val fragment = supportFragmentManager.findFragmentByTag("android:switcher:" + R.id.main_page_view_pager + ":" + viewPager.currentItem)
                fragment?.onActivityResult(requestCode, resultCode, data)
            }
        }
    }


    override fun showSomethingWentWrong() {
        multiStateLayout.state = MultiStateLayout.State.ERROR
    }

    override fun showNoInternetConnection() {
        multiStateLayout.state = MultiStateLayout.State.NETWORK_ERROR
    }

    override fun showContent() {
        multiStateLayout.state = MultiStateLayout.State.CONTENT
    }

    override fun showLoader() {
        super.showProgress()
    }

    override fun hideLoader() {
        super.hideProgress()
    }

    override fun setTabItem(tab: TabLayout.Tab?) {
        viewPager.currentItem = tab!!.position
    }


    override fun onBackPressed() {
        if (!onBackPressedChildFragment(supportFragmentManager)) Process.killProcess(Process.myPid())
    }

    private fun onBackPressedChildFragment(childFragment: FragmentManager): Boolean {

        val fragList = childFragment.fragments.filter { it != null && it.userVisibleHint }
        fragList.forEach {
            if (onBackPressedChildFragment(it.childFragmentManager)) {
                return true
            }
        }

        if (childFragment.backStackEntryCount > 0) {
            childFragment.popBackStack()
            return true
        }
        return false
    }

    override fun showScannError() {
        Toast.makeText(this, "Ошибка при сканировании кода", Toast.LENGTH_LONG).show()
    }

    override fun startPaymentActivity(content: ScannedBody) {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.main_page_container, PaymentFragment.create(content), PaymentFragment::class.java.name)
                .addToBackStack(PaymentFragment::class.java.name)
                .commit()
    }

    override fun openScanner() {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.main_page_container, ScannerFragment.create { presenter.getScannedContent(it) }, ScannerFragment::class.java.name)
                .addToBackStack(ScannerFragment::class.java.name)
                .commit()
    }

    override fun openSendMoney() {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.main_page_container, SendMoneyFragment.create(SendMoneyType.CREATE, null), SendMoneyFragment::class.java.name)
                .addToBackStack(SendMoneyFragment::class.java.name)
                .commit()
    }


    override fun startPayActivity(content: ScannedBody) {
        val intent = Intent(this, SendMoneyFragment::class.java)
        intent.putExtra("content", content)
        startActivity(intent)
    }
}