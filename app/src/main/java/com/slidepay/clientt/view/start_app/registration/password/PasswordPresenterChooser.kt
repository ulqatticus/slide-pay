package com.slidepay.clientt.view.start_app.registration.password

import com.slidepay.clientt.application.utils.Config
import com.slidepay.clientt.presenter.start.registration.password.PinCodeCheckPresenter
import com.slidepay.clientt.presenter.start.registration.password.PinCodeCreatePresenter
import com.slidepay.clientt.presenter.start.registration.password.PinCodeRestorePresenter

class PasswordPresenterChooser(private var pinCodeMode: Config.PinCodeMode, var activity: PinCodeContract.View) {

    internal fun providePinCodePresenter(): PinCodeContract.Presenter? {
        return when (pinCodeMode) {
            Config.PinCodeMode.CREATE -> PinCodeCreatePresenter(activity)
            Config.PinCodeMode.CHECK -> PinCodeCheckPresenter(activity)
            Config.PinCodeMode.RESTORE -> PinCodeRestorePresenter(activity)
        }
    }
}