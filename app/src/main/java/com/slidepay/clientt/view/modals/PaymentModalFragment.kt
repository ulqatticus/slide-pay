package com.slidepay.clientt.view.modals

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.slidepay.clientt.R
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.modal_payment_view.view.*

class PaymentModalFragment : Fragment() {
    private lateinit var mBenefitIcon: ImageView
    private lateinit var mBenefitText: TextTypeface
    private lateinit var mBenefitBtn: TextTypeface

    private var mBenefit: ModalBenefit? = null
    var clickCloseBtn: () -> Unit = {}


    companion object {
        private const val BENEFIT_KEY = "benefit"

        fun create(benefit: ModalBenefit, clickCloseBtn: () -> Unit = {}): Fragment {
            val bundle = Bundle()
            bundle.putString(BENEFIT_KEY, benefit.name)
            val fragment = PaymentModalFragment()
            fragment.arguments = bundle
            return fragment.apply { this.clickCloseBtn = clickCloseBtn }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val benefit = arguments?.getString(BENEFIT_KEY, ModalBenefit.GET_MONEY.name)
        benefit?.let {
            mBenefit = ModalBenefit.valueOf(it)

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.modal_payment_view, container, false)
        mBenefitIcon = view.modal_payment_image
        mBenefitText = view.modal_payment_text
        mBenefitBtn = view.modal_payment_agree_btn
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBenefitIcon.setImageResource(mBenefit!!.drawableId)
        mBenefitText.setText(mBenefit!!.textId)
        mBenefitBtn.setOnClickListener {
            clickCloseBtn()
            fragmentManager?.popBackStackImmediate()
        }

    }


}