package com.slidepay.clientt.view.sendmoney

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Spinner
import cn.refactor.multistatelayout.MultiStateLayout
import com.slidepay.clientt.R
import com.slidepay.clientt.contracts.sendmoney.SendMoneyContract
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.model.objects.main.ScannedBody
import com.slidepay.clientt.model.objects.main.SumType
import com.slidepay.clientt.presenter.invoice.SendMoneyPresenter
import com.slidepay.clientt.presenter.sendmoney.SendMoneyType
import com.slidepay.clientt.util.stringText
import com.slidepay.clientt.view.BaseFragment
import com.slidepay.clientt.view.alert_fragment.DescriptionDialogFragment
import com.slidepay.clientt.view.modals.ModalBenefit
import com.slidepay.clientt.view.modals.PaymentModalFragment
import com.slidepay.clientt.view.widgets.EditTextTypeface
import com.slidepay.clientt.view.widgets.PaymentFrame
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.info_no_internet.view.*
import kotlinx.android.synthetic.main.info_something_went_wrong.view.*
import kotlinx.android.synthetic.main.fragment_send_money.*


class SendMoneyFragment : BaseFragment(), SendMoneyContract.View, PaymentFrame.PaymentEvents {


    companion object {
        fun create(sendMoneyType: SendMoneyType, content: ScannedBody?): SendMoneyFragment {
            return SendMoneyFragment().apply {
                this.content = content
                this.sendMoneyType = sendMoneyType
            }
        }

    }


    private var description: String? = null
    private var content: ScannedBody? = null
    private lateinit var sendMoneyType: SendMoneyType


    val presenter = SendMoneyPresenter(this)

    private val spinner: Spinner by lazy { sendmoney_spinner }
    private val closeBtn: ImageView by lazy { send_money_close_btn }
    private val amountField: EditTextTypeface by lazy { send_money_summ_field }
    private val clientName: TextTypeface by lazy { send_name_client_name }
    private val descriptionText: TextTypeface by lazy { send_money_description }
    private val descriptionBtn: TextTypeface by lazy { send_money_add_description_btn }
    private val cardNUmber: EditTextTypeface by lazy { send_money_summ_curd_number_field }
    private val multiStateLayout: MultiStateLayout by lazy { multi_state_layout }
    private lateinit var retry: TextTypeface
    private val paymentFrame: PaymentFrame by lazy { payment_frame }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_send_money, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.viewCreated(content)

    }


    override fun isFromTransaction(): Boolean {
        return content?.isFromTransacton == true
    }

    override fun initMultiStateListener() {
        multiStateLayout.setOnStateViewCreatedListener { view, state ->

            when (state) {
                MultiStateLayout.State.NETWORK_ERROR -> {
                    retry = view.info_no_internet_btn
                    retry.setOnClickListener {
                        showContent()
                    }
                }

            }
        }
    }

    override fun startSuccessFragment() {
        childFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .add(R.id.send_money_fragment_sent, PaymentModalFragment.create(ModalBenefit.SENT_MONEY) { closeFragment() })
                .commit()

    }

    override fun initViews(body: ScannedBody?) {

        paymentFrame.subscribePaymentEvent(this)

        if (body == null) {
            showEnterCardNumber()
        }

        if (body?.sum == null) {
            amountField.hint = "0.00"
        } else {
            amountField.setText(body.sum.toString())
        }

        body?.let {
            clientName.text = it.name
            descriptionText.text = it.description

            when {
                it.sumType == SumType.Fixed -> amountField.isEnabled = false
                it.sumType == SumType.Optional -> {
                    amountField.isCursorVisible = true
                    amountField.setSelection(amountField.text.length)
                }
            }
        }


        descriptionBtn.setOnClickListener { openDescription() }

        descriptionText.movementMethod = ScrollingMovementMethod()

        closeBtn.setOnClickListener { closeFragment() }


        amountField.setOnClickListener {
            setCursorVisible(true)
            amountField.setSelection(amountField.text.length)
        }


    }

    override fun setDescription(description: String?) {

        if (description.isNullOrEmpty()) {
            descriptionBtn.text = getString(R.string.add_description)
        } else {
            this.description = description
            descriptionBtn.text = getString(R.string.edit_description)
        }

    }

    private fun openDescription() {
        DescriptionDialogFragment.newInstance(description) { setDescription(it) }.show(childFragmentManager)
    }

    override fun getRecipientNumber(): String? = if (sendMoneyType == SendMoneyType.CREATE) cardNUmber.stringText() else content?.phoneNumber


    override fun showEnterCardNumber() {
        cardNUmber.visibility = View.VISIBLE
    }

    override fun hideEnterCardNumber() {
        cardNUmber.visibility = View.GONE
    }

    override fun getBillNumber(): String? = content?.number ?: content?.phoneNumber //TODO FX IT


    override fun initUserCard(userCards: ArrayList<CardBody>) {
        val spinnerArrayAdapter = ArrayAdapter(requireContext(), R.layout.spinner_item, userCards)
        spinner.adapter = spinnerArrayAdapter
    }

    private fun setCursorVisible(isVisible: Boolean) {
        amountField.isCursorVisible = isVisible
    }

    override fun showSomethingWentWrong() {
        multiStateLayout.state = MultiStateLayout.State.ERROR
    }

    override fun showContent() {
        multiStateLayout.state = MultiStateLayout.State.CONTENT
    }

    override fun showNoInternetConnection() {
        multiStateLayout.state = MultiStateLayout.State.NETWORK_ERROR
    }

    override fun showLoading() {
        multiStateLayout.state = MultiStateLayout.State.LOADING
    }

    override fun hideLoading() {
        multiStateLayout.state = MultiStateLayout.State.CONTENT
    }

    override fun getAmount(): String = amountField.stringText()


    override fun showError() {
        amountField.setBackgroundResource(R.drawable.background_error_edit)
    }

    override fun getCardId(): Long? {
        val st = spinner.selectedItem as CardBody
        return st.id
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.viewDestroyed()
    }

    override fun getDescription(): String? = description


    override fun closeFragment() {
        fragmentManager?.popBackStackImmediate()
    }

    override fun createEvent() {
        presenter.sendMoney(sendMoneyType)
    }


    override fun initStateListener() {
        multiStateLayout.setOnStateViewCreatedListener { view, state ->

            when (state) {
                MultiStateLayout.State.NETWORK_ERROR -> {
                    val retry = view.info_no_internet_btn
                    retry.setOnClickListener {
                        showContent()
                    }
                }
                MultiStateLayout.State.ERROR -> {
                    val retry = view.something_went_wrong_btn
                    retry.setOnClickListener {
                        showContent()
                    }
                }

            }
        }
    }

}