package com.slidepay.clientt.view.mainpage.transaction

import android.annotation.SuppressLint
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.method.ScrollingMovementMethod
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import com.slidepay.clientt.R
import com.slidepay.clientt.model.objects.main.TransactionBody
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.item_transaction_blue.view.*


class TransactionInvoiceViewHolder(itemView: View, private var transactionsAdapter: TransactionsAdapter) : RecyclerView.ViewHolder(itemView) {

    private val transactionCircleStatusIcon: ImageView by lazy { itemView.transaction_invoice_item_status }


    private val transactionTime: TextTypeface by lazy { itemView.transaction_invoice_item_time }
    private val transactionDate: TextTypeface by lazy { itemView.transaction_invoice_item_date }
    private val transactionName: TextTypeface by lazy { itemView.transaction_invoice_client_name }
    private val transactionPrice: TextTypeface by lazy { itemView.transaction_invoice_price }
    private val transactionDescription: TextTypeface by lazy { itemView.transaction_invoice_description }
    private val transactionTextStatus: TextTypeface by lazy { itemView.transaction_invoice_item_text_status }
    private val transactionErrorText: TextTypeface by lazy { itemView.transaction_invoice_error_text }

    private val expandableLayout: LinearLayout by lazy { itemView.transaction_invoice_expanded_item_body }


    private val cancelBtn: TextTypeface by lazy { itemView.transaction_invoice_cancel_btn }
    private val retryBtn: TextTypeface by lazy { itemView.transaction_invoice_resend_btn }
    private val changeBtn: TextTypeface by lazy { itemView.transaction_invoice_change_btn }


    @SuppressLint("ClickableViewAccessibility")
    fun bind(transaction: TransactionBody) {


        transactionDate.text = transaction.date
        transactionTime.text = transaction.time
        transactionName.text = transaction.name


        val isExpanded = adapterPosition.minus(1) == transactionsAdapter.mExpandedPosition;


        if (isExpanded) {
            expandableLayout.visibility = View.VISIBLE
            if (adapterPosition.minus(1) > 1)
                transactionsAdapter.scrollToPosition(adapterPosition.minus(1))

        } else {
            expandableLayout.visibility = View.GONE

        }

        if (isExpanded)
            transactionsAdapter.previousExpandedPosition = adapterPosition.minus(1)


        if (transaction.sum == null) {
            transaction.sum = "0.00"
        }


        transactionPrice.text = itemView.context.getString(R.string.price_type, if (transaction.isInvoice!!) "-" else "+", transaction.sum, "грн")

        setStatusInfo(transaction.status)



        if (!transaction.errorDescription.isNullOrEmpty()) {
            transactionErrorText.visibility = View.VISIBLE
            transactionErrorText.text = transaction.errorDescription
        }


        if (transaction.description.isNullOrEmpty()) {
            transactionDescription.visibility = View.GONE
        } else {
            transactionDescription.text = transaction.description
        }
        transactionDescription.movementMethod = ScrollingMovementMethod()

//TODO REMOVE THIS FROM VIWHOLDER
        itemView.setOnClickListener {
            transactionsAdapter.mExpandedPosition = if (isExpanded) -1 else adapterPosition.minus(1)
            transactionsAdapter.notifyItemChanged(transactionsAdapter.previousExpandedPosition)
            transactionsAdapter.notifyItemChanged(adapterPosition.minus(1))
        }

        retryBtn.setOnClickListener { transactionsAdapter.retryInvoice(transaction.number) }
        cancelBtn.setOnClickListener { transactionsAdapter.cancelInvoice(transaction.number) }
        changeBtn.setOnClickListener { transactionsAdapter.changeInvoice(transaction) }

        transactionDescription.setOnTouchListener { v, event ->
            val action = event?.action
            when (action) {
                MotionEvent.ACTION_DOWN ->
                    // Disallow ScrollView to intercept touch events.
                    v?.parent?.requestDisallowInterceptTouchEvent(true)

                MotionEvent.ACTION_UP ->
                    // Allow ScrollView to intercept touch events.
                    v?.parent?.requestDisallowInterceptTouchEvent(false)
            }

            v?.onTouchEvent(event)
            true
        }
    }

    /*
            Created = 0,
            SelectedForPayment = 1,
            Success = 2,
            Error = 3,
            Canceled = 4,
            CanceledByPayer = 5,
            MultiPayments = 6
    * */


    private fun setStatusInfo(status: Int?) {
        allGone()
        when (status) {
            0 -> {
                setTextAndImageColor("К оплате", "#D5D7D8", "#D5D7D8")

                changeBtn.visibility = View.VISIBLE
                cancelBtn.visibility = View.VISIBLE
            }

            1 -> {
                setTextAndImageColor("К оплате", "#D5D7D8", "#D5D7D8")
                changeBtn.visibility = View.VISIBLE
                cancelBtn.visibility = View.VISIBLE
            }

            2 -> {
                setTextAndImageColor("Оплачено", "#287BF7", "#287BF7")
            }

            3 -> {
                setTextAndImageColor("Ошибка", "#F56D8E", "#D5D7D8")
                retryBtn.visibility = View.VISIBLE
            }

            4 -> {
                setTextAndImageColor("Отменен", "#F56D8E", "#D5D7D8")
            }
            5 -> {
                setTextAndImageColor("Отменен", "#F56D8E", "#D5D7D8")

            }
        }
    }

    private fun allGone() {
        cancelBtn.visibility = View.GONE
        changeBtn.visibility = View.GONE
        retryBtn.visibility = View.GONE
    }

    private fun setTextAndImageColor(textStatus: String, colorStatusColor: String, colorText: String) {
        transactionTextStatus.text = textStatus
        transactionCircleStatusIcon.setColorFilter(Color.parseColor(colorStatusColor))
        transactionPrice.setTextColor(Color.parseColor(colorText))
    }


}