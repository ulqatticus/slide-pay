package com.slidepay.clientt.view.start_app.registration.type_number

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.Toast
import com.slidepay.clientt.R
import com.slidepay.clientt.application.utils.ConnectionHelper
import kotlinx.android.synthetic.main.terms_of_use_web_fragment.view.*
import java.util.regex.Pattern

class TermsOfUseWebFragment : Fragment() {
    private lateinit var cancelBtn: ImageView
    private lateinit var webView: WebView
    private var currentRunnable: Runnable? = null
    private val handler: Handler = Handler()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.terms_of_use_web_fragment, container, false)
        cancelBtn = view.terms_use_activity_text_close_btn
        webView = view.terms_use_activity_webview
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initWebView(webView)
        cancelBtn.setOnClickListener { activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit() }

    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView(webView: WebView) {
        // initProgress()
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = object : WebViewClient() {


            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                //   progress?.dismiss()
            }

            /**
             * if we got an error from server
             * */
            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                // progress?.dismiss()
            }


            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                return if (url.toString().startsWith("mailto:")) {
                    startEmailIntent(url.toString())
                    true
                } else {
                    view?.loadUrl(url.toString())
                    true
                }


            }

            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    return if (request.url.toString().startsWith("mailto:")) {
                        startEmailIntent(request.url.toString())
                        true
                    } else {
                        view.loadUrl(request.url.toString())
                        true
                    }
                }
                return true

            }
        }
        currentRunnable = Runnable {
            activity?.let {

                val url: String? = "https://www.google.com/"
                if (urlIsValid(url!!)) {
                    webView.loadUrl(url)
                } else {
                    Toast.makeText(activity, "Url is not valid", Toast.LENGTH_SHORT).show()
                }
            }
        }

        if (!ConnectionHelper().isOnline()) {
            Toast.makeText(activity, "No  connection", Toast.LENGTH_SHORT).show()

            return
        }

        handler.post(currentRunnable)


    }

    fun startEmailIntent(address: String) {
        val emailIntent = Intent(Intent.ACTION_SENDTO)
        emailIntent.data = Uri.parse(address)
        activity?.startActivity(Intent.createChooser(emailIntent, activity?.getString(R.string.lbl_send_mail)))
    }

    private fun urlIsValid(url: String): Boolean {
        return Pattern.compile(
                "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"
        ).matcher(url).matches()
    }

    companion object {
        fun create(): Fragment {
            return TermsOfUseWebFragment()
        }
    }
}