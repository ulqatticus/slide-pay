package com.slidepay.clientt.view.start_app.registration.password

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import android.widget.ImageView
import cn.refactor.multistatelayout.MultiStateLayout
import com.jakewharton.rxbinding2.widget.RxTextView
import com.slidepay.clientt.R
import com.slidepay.clientt.application.utils.Config
import com.slidepay.clientt.view.BaseActivity
import com.slidepay.clientt.view.mainpage.PageActivity
import com.slidepay.clientt.view.start_app.registration.CheckCodeActivity
import com.slidepay.clientt.view.widgets.EditTextTypeface
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.info_no_internet.view.*
import kotlinx.android.synthetic.main.password_activity.*

class PasswordActivity : BaseActivity(), PinCodeContract.View {


    private lateinit var presenter: PinCodeContract.Presenter


    private val passwordBlock: ConstraintLayout by lazy { create_password_type_pass_block }
    private val passwordLabel: TextTypeface by lazy { create_password_label }
    private val errorText: TextTypeface by lazy { create_password_error_text }
    private val forgotPass: TextTypeface by lazy { forgot_password_text }
    private val typePass: EditTextTypeface by lazy { create_password_type_value }
    private val fingerPrintBtn: ImageView by lazy { create_password_fingerdetect_btn }
    private val dividerInTypeFace: FrameLayout by lazy { divider_in_typeface }


    companion object {
        fun createPinCode(context: Context) {
            startActivity(context, Config.PinCodeMode.CREATE)
        }

        fun checkPinCode(context: Context) {
            startActivity(context, Config.PinCodeMode.CHECK)
        }

        fun restorePinCode(context: Context) {
            startActivity(context, Config.PinCodeMode.RESTORE)
        }


        private fun startActivity(context: Context, pinCodeMode: Config.PinCodeMode) {
            val intent = Intent(context, PasswordActivity::class.java)
            intent.putExtra(Config.EXTRA_MODE, pinCodeMode)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.password_activity)
        val pinCodeMode = intent.getSerializableExtra(Config.EXTRA_MODE) as Config.PinCodeMode
        presenter = PasswordPresenterChooser(pinCodeMode, this).providePinCodePresenter()!!
        presenter.viewIsReady()
    }


    override fun clearTextInEditText() {
        typePass.setText("")
    }


    override fun initStateListener() {
       /* multiStateLayout.setOnStateViewCreatedListener { view, state ->

            when (state) {
                MultiStateLayout.State.NETWORK_ERROR -> {
                    val  retry = view.info_no_internet_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }

            }
        }*/
    }



    override fun setListenerToEditText() {
        RxTextView.afterTextChangeEvents(typePass)
                .skipInitialValue()
                .map { it.editable().toString() }
                .filter { it.length == 4 }
                .subscribe { presenter?.next(it) }

        typePass.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                presenter.next(typePass.text.toString())
            }
            false
        }

        RxTextView.textChanges(typePass)
                .skipInitialValue()
                .subscribe {
                    hideError()
                }
    }


    override fun getActivity(): BaseActivity {
        return this
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()

    }


    override fun startConfirmCodeForRestorePass(phoneNumber: String) {
        val intent = Intent(this, CheckCodeActivity::class.java)
        val extras = Bundle()
        extras.putSerializable("type", CheckCodeActivity.VereficationType.RESTORE_PASS)
        extras.putString("phone", phoneNumber)
        intent.putExtras(extras)
        startActivity(intent)
    }

    override fun startNextActivity() {
        finishAffinity()
        hideKeyboard()
        startActivity(Intent(this, PageActivity::class.java))
    }


    override fun hideError() {
        errorText.visibility = View.GONE
        passwordBlock.setBackgroundResource(R.drawable.backround_field_blue)
    }

    override fun showError(error: String) {
        errorText.text = error
        typePass.setText("")
        errorText.visibility = View.VISIBLE
        passwordBlock.setBackgroundResource(R.drawable.background_error_edit)

    }

    override fun showFingerPrintBtn() {
        fingerPrintBtn.visibility = View.VISIBLE
        fingerPrintBtn.setOnClickListener {
            hideKeyboard()
            presenter.startScanFingerPrint()

        }
    }


    override fun hideForgotPass() {
        forgotPass.visibility = View.GONE
    }

    override fun showLoading() {
        super.showProgress()
    }

    override fun hideLoading() {
        super.hideProgress()
    }

    override fun showKeyBoard() {
        super.showKeyboard()
    }

    override fun hideKeyBoard() {
        super.hideKeyboard()
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun showForgotPass() {
        forgotPass.visibility = View.VISIBLE
        forgotPass.setOnClickListener {
            presenter.forgotPassClick()
        }
    }

    override fun finishActivity() {
        finishAffinity()
        startActivity(Intent(this, PageActivity::class.java))
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    override fun hideFingerPrintBtn() {
        fingerPrintBtn.visibility = View.GONE
    }

    override fun showDivider() {
        dividerInTypeFace.visibility = View.VISIBLE

    }

    override fun showSomethingWentWrong() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showNoInternetConnection() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showContent() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun hideDivider() {
        dividerInTypeFace.visibility = View.GONE
    }

    override fun setLabelText(text: String) {
        passwordLabel.text = text
    }


    override fun closeActivity() {
        finishAffinity()
    }


}