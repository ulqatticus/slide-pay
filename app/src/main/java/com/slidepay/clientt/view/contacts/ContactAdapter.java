package com.slidepay.clientt.view.contacts;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.slidepay.clientt.R;
import com.slidepay.clientt.model.objects.main.ContactsBody;
import com.slidepay.clientt.presenter.contacts.ContactsPresenter;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ContactAdapter extends RecyclerView.Adapter<ContactItemViewHolder> implements StickyRecyclerHeadersAdapter<ContactsHeaderViewHolder> {
    private List<ContactsBody> contactsBodyList = new ArrayList<>();
    private ContactsPresenter presenter;

    public ContactAdapter(ContactsPresenter presenter) {
        this.presenter = presenter;
    }

    public void addContacts(List<ContactsBody> list) {
        contactsBodyList.clear();
        contactsBodyList.addAll(list);
        notifyDataSetChanged();

    }

    //init item
    @Override
    public ContactItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContactItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contacts, parent, false), this);
    }

    @Override
    public void onBindViewHolder(final ContactItemViewHolder holder, final int position) {
        holder.bind(contactsBodyList.get(position));
    }

    @Override
    public int getItemCount() {
        return contactsBodyList.size();
    }

    //init header
    @Override
    public long getHeaderId(int position) {
        if (position == 0) {
            return -1;
        } else {
            return Objects.requireNonNull(contactsBodyList.get(position).getName()).toUpperCase().charAt(0);
        }
    }

    @Override
    public ContactsHeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return new ContactsHeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contacts_heder, parent, false));
    }

    @Override
    public void onBindHeaderViewHolder(ContactsHeaderViewHolder holder, int position) {
        holder.bind(String.valueOf(Objects.requireNonNull(contactsBodyList.get(position).getName()).toUpperCase().charAt(0)));
    }

    public void pickedContact(@Nullable String number) {
        presenter.pickedContact(number);
    }
}