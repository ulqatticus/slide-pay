package com.slidepay.clientt.view.start_app.registration.create_card

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.LinearLayout
import android.widget.Toast
import com.slidepay.clientt.R
import com.slidepay.clientt.contracts.start.AddCardContract
import com.slidepay.clientt.presenter.start.registration.create_card.CreateCardPresenter
import com.slidepay.clientt.view.BaseActivity
import com.slidepay.clientt.view.modals.ModalBenefit
import com.slidepay.clientt.view.modals.PaymentModalFragment
import com.slidepay.clientt.view.start_app.registration.password.PasswordActivity
import com.slidepay.clientt.view.widgets.ButtonTypeface
import com.slidepay.clientt.view.widgets.EditTextTypeface
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.create_card_activity.*
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.text.TextUtils
import cn.refactor.multistatelayout.MultiStateLayout
import com.slidepay.clientt.util.stringText
import kotlinx.android.synthetic.main.info_no_internet.view.*
import kotlinx.android.synthetic.main.info_something_went_wrong.view.*


class CreateCardActivity : BaseActivity(), AddCardContract.View {


    private var presenter = CreateCardPresenter(this)
    private val skipAddCard: TextTypeface by lazy { skip_card_close_web_view_btn }
    private val webView: WebView by lazy { add_card_web_view }
    private val firstBlock: LinearLayout by lazy { id_add_card_first_block }
    private val secondBlock: ConstraintLayout by lazy { id_add_card_second_block }
    private val typeCardNumber: EditTextTypeface by lazy { add_card_type_edit_number }
    private val addCardBtn: ButtonTypeface by lazy { add_card_btn }
    private val multiStateLayout: MultiStateLayout by lazy { multi_state_layout }

    private lateinit var type: Type


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.create_card_activity)
        type = intent.getSerializableExtra("type") as CreateCardActivity.Type

        presenter.viewCreated(type)

    }

    override fun startCreatePasswordActivity() {
        when (type) {
            CreateCardActivity.Type.CREATE_ON_START_APP -> {
                PasswordActivity.createPinCode(this)

            }
            CreateCardActivity.Type.CREATE_FROM_MENU_CARD -> {
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }


    override fun initCloseBtn() {

        skipAddCard.setOnClickListener {
            when (type) {
                Type.CREATE_ON_START_APP -> {
                    startCreatePasswordActivity()
                }
                Type.CREATE_FROM_MENU_CARD -> {
                    setResult(Activity.RESULT_CANCELED)
                    finish()
                }
            }
        }
    }


    override fun initTextWatcher() {


        add_card_type_edit_number.addTextChangedListener(object : TextWatcher {
            private val EMPTY_STRING = ""
            private val WHITE_SPACE = " "
            private var lastSource = EMPTY_STRING
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                hideErrorCardName()

            }


            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                /*  val len = s.toString().length

                  if (before == 0 && (len == 4 || len == 9 || len == 14))
                      add_card_type_edit_number.append(" ")*/
            }

            override fun afterTextChanged(s: Editable) {
                /* var source = s.toString();
                 if (!lastSource.equals(source)) {
                     source = source.replace(WHITE_SPACE, EMPTY_STRING);
                     val stringBuilder =  StringBuilder();
                     for (int i = 0; i < source.length; i++) {
                         if (i > 0 && i % 4 == 0) {
                             stringBuilder.append(WHITE_SPACE);
                         }
                         stringBuilder.append(source[i]);
                     }
                     lastSource = stringBuilder.toString();
                     s.replace(0, s.length(), lastSource);
                 }
 */
            }
        })

        add_card_type_edit_number.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if ((event != null && (event.keyCode == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
                presenter.sendRequest(getCardNumber())
            }
            return@OnEditorActionListener false
        })

    }

    override fun initAddCardBtn() {
        addCardBtn.setOnClickListener {
            presenter.sendRequest(getCardNumber())
        }
    }

    override fun hideError() {
        typeCardNumber.setBackgroundResource(R.drawable.backround_field_blue)
    }

    override fun showValidationError() {
        typeCardNumber.setBackgroundResource(R.drawable.background_error_edit)
    }

    override fun showLoading() {
        super.showProgress()
    }

    override fun hideLoading() {
        super.hideProgress()
    }

    override fun showKeyBoard() {
        super.showKeyboard()
    }

    override fun hideKeyBoard() {
        super.hideKeyboard()
    }

    override fun showFirstBlock() {
        firstBlock.visibility = View.VISIBLE
    }

    override fun hideFirstBlock() {
        firstBlock.visibility = View.GONE

    }


    override fun showSecondBlock() {
        secondBlock.visibility = View.VISIBLE
    }

    override fun hideSecondBlock() {
        secondBlock.visibility = View.GONE
    }


    override fun getCardNumber(): String {
        return typeCardNumber.stringText().replace(" ", "")
    }

    override fun showInvalidUrlError() {
        Toast.makeText(this, "url is not valid", Toast.LENGTH_SHORT).show()
    }


    override fun loadWebView(url: String) {
        presenter.loadWebView(url)
    }


    override fun hideErrorCardName() {
        add_card_type_edit_number.background = ContextCompat.getDrawable(this, R.drawable.style_linnerlayout)
    }

    override fun showErrorCardName() {
        add_card_type_edit_number.background = ContextCompat.getDrawable(this, R.drawable.background_error_edit)
    }


    @SuppressLint("SetJavaScriptEnabled")
    override fun configWebView() {
        webView.settings.javaScriptEnabled = true
        webView.settings?.javaScriptCanOpenWindowsAutomatically = true
        webView.settings?.setSupportMultipleWindows(true)
        webView.webViewClient = WebClient()


        if (Build.VERSION.SDK_INT >= 26) {
            webView.settings.safeBrowsingEnabled = false
        }
        webView.setOnTouchListener { p0, event -> (event.action == MotionEvent.ACTION_MOVE); }

    }

    override fun loadUrl(url: String) {
        webView.loadUrl(url)
    }


    override fun startSuccessFragment() {
        firstBlock.visibility = View.INVISIBLE
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .add(R.id.add_card_fragment, PaymentModalFragment.create(ModalBenefit.CARD_ADDED) { finish() })
                .commit()
    }


    inner class WebClient : WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            hideProgress()
        }

        override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
            hideProgress()
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            presenter.checkUrlToRedirect(request?.url.toString())
            return false
        }

        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            presenter.checkUrlToRedirect(url.toString())
            return false
        }
    }


    override fun showTextToSkip(skipText: String?) {
        skipAddCard.text = skipText
    }

    override fun getActivity(): Activity {
        return this
    }

    override fun onDestroy() {
        super.onDestroy()
        webView.webViewClient = null
        presenter.viewDestroyed()
    }


    enum class Type {
        CREATE_ON_START_APP, CREATE_FROM_MENU_CARD
    }


    override fun showSomethingWentWrong() {
        multiStateLayout.state = MultiStateLayout.State.ERROR
    }

    override fun showContent() {
        multiStateLayout.state = MultiStateLayout.State.CONTENT
    }

    override fun showNoInternetConnection() {
        multiStateLayout.state = MultiStateLayout.State.NETWORK_ERROR
    }

    override fun initStateListener() {
        multiStateLayout.setOnStateViewCreatedListener { view, state ->

            when (state) {
                MultiStateLayout.State.NETWORK_ERROR -> {
                    val retry = view.info_no_internet_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }
                MultiStateLayout.State.ERROR -> {
                    val retry = view.something_went_wrong_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }

            }
        }
    }


}