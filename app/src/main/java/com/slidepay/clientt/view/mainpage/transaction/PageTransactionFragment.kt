package com.slidepay.clientt.view.mainpage.transaction

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.refactor.multistatelayout.MultiStateLayout
import com.slidepay.clientt.R
import com.slidepay.clientt.contracts.main.transaction.TransactionContract
import com.slidepay.clientt.model.objects.main.ScannedBody
import com.slidepay.clientt.model.objects.main.TransactionBody
import com.slidepay.clientt.presenter.mainpage.transaction.TransactionPresenter
import com.slidepay.clientt.view.mainpage.PageActivity
import com.slidepay.clientt.view.sendmoney.SendMoneyFragment
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.fragment_transaction.view.*
import kotlinx.android.synthetic.main.info_no_internet.view.*
import kotlinx.android.synthetic.main.info_something_went_wrong.view.*


class PageTransactionFragment : Fragment(), TransactionContract.View, SwipeRefreshLayout.OnRefreshListener {



    private lateinit var transactionsAdapter: TransactionsAdapter

    private lateinit var transactionRecycler: RecyclerView

    private lateinit var transactionSwipeContainer: SwipeRefreshLayout
    private lateinit var multiStateLayout: MultiStateLayout
    private lateinit var retry: TextTypeface


    private var presenter: TransactionPresenter = TransactionPresenter(this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_transaction, container, false)
        transactionRecycler = rootView.transaction_recycler
        transactionSwipeContainer = rootView.transaction_item_swipe_container
        multiStateLayout = rootView.multi_state_layout
        transactionSwipeContainer.setOnRefreshListener(this)
        transactionSwipeContainer.setColorSchemeResources(R.color.green, R.color.blue, R.color.black)
        transactionSwipeContainer.setDistanceToTriggerSync(REFRESH_TRIGGER_DISTANCE)
        return rootView
    }


    override fun onDestroyView() {
        super.onDestroyView()
        presenter.viewDestroyed()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.viewCreated()
    }


    override fun onRefresh() {
        getData()
    }


    override fun addItems(list: ArrayList<TransactionBody>) {
       /* val manager = CustomLayoutManager(view, LinearLayoutManager.VERTICAL, false)

        val dataSource = MyPositionalDataSource(list)
        adapter = EmployeeAdapter(diffCallback);
        adapter.submitList(getPagedList(dataSource));

        transactionRecycler.layoutManager = manager
        transactionRecycler.adapter = adapter
*/

         transactionsAdapter.addItem(list)
    }


    private val diffCallback = object : DiffUtil.ItemCallback<TransactionBody>() {
        override fun areItemsTheSame(oldItem: TransactionBody, newItem: TransactionBody): Boolean {
//                Log.i("DiffUtil", "SameItem? old status: ${oldItem.status}, new Status: ${newItem.status}")
            return oldItem.number == newItem.number
        }

        override fun areContentsTheSame(oldItem: TransactionBody, newItem: TransactionBody): Boolean {
//                Log.i("DiffUtil", "Checking status: new: ${newItem.status}, old ${oldItem.status}")
            return oldItem.status == newItem.status
        }

        override fun getChangePayload(oldItem: TransactionBody, newItem: TransactionBody): Any? {
            if (oldItem.status != newItem.status) {
                return true
            }
            return null
        }

    }


    private fun getData() {
        presenter.updateData()
        transactionSwipeContainer.isRefreshing = false
    }


    override fun initStateListener() {
        multiStateLayout.setOnStateViewCreatedListener { view, state ->

            when (state) {
                MultiStateLayout.State.NETWORK_ERROR -> {
                    retry = view.info_no_internet_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }
                MultiStateLayout.State.ERROR -> {
                    val retry = view.something_went_wrong_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }

            }
        }

    }

    override fun startPayActivity(payInvoiceBody: ScannedBody) {
        val intent = Intent(activity, SendMoneyFragment::class.java)
        intent.putExtra("content", payInvoiceBody)
        startActivity(intent)
    }


    override fun scrollToPosition(position: Int) {
        transactionRecycler.scrollToPosition(position)
        transactionRecycler.invalidateItemDecorations()
    }

    override fun showLoading() {
        multiStateLayout.state = MultiStateLayout.State.LOADING
    }


    override fun showContent() {
        multiStateLayout.state = MultiStateLayout.State.CONTENT
    }


    override fun showSomethingWentWrong() {
        multiStateLayout.state = MultiStateLayout.State.ERROR
    }

    override fun showNoInternetConnection() {
        multiStateLayout.state = MultiStateLayout.State.NETWORK_ERROR
    }


    override fun initTransactionsView() {
        val manager = CustomLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        transactionsAdapter = TransactionsAdapter(presenter)
        transactionRecycler.layoutManager = manager
        transactionRecycler.adapter = transactionsAdapter
    }






    override fun getTContext(): AppCompatActivity {
        return activity as PageActivity
    }

    companion object {
        private const val REFRESH_TRIGGER_DISTANCE = 400
    }



   /* internal inner class MainThreadExecutor : Executor {
        private val mHandler = Handler(Looper.getMainLooper())

        override fun execute(command: Runnable) {
            mHandler.post(command)
        }
    }*/

}