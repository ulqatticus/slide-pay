package com.slidepay.clientt.view.modals

import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import com.slidepay.clientt.R


enum class ModalBenefit constructor(@param:DrawableRes @get:DrawableRes val drawableId: Int,
                                    @param:StringRes @get:StringRes val textId: Int) {

    SENT_MONEY(R.drawable.ic_modal_payment_sent, R.string.payment_sent_modal_text),
    GET_MONEY(R.drawable.ic_modal_payment_get, R.string.payment_get_modal_text),
    RESEND_CODE(R.drawable.ic_modal_resend_code, R.string.resend_code_modal_text),
    CARD_ADDED(R.drawable.ic_modal_card_added, R.string.card_added_modal_text),
    PASSWORD_WAS_CHANGED(R.drawable.ic_password_activity, R.string.password_was_changed_modal_text)

}