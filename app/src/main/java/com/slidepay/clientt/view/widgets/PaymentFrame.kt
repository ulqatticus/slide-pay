package com.slidepay.clientt.view.widgets

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import com.slidepay.clientt.R
import kotlinx.android.synthetic.main.invoice_payment_frame.view.*

class PaymentFrame : FrameLayout, View.OnTouchListener {

    private var halfOfWidth = 0
    private var startMoveX: Float = 0f
    private var indicator = 1
    private var paymentEvent: PaymentEvents? = null


    companion object {
        const val ALPHA = 300
    }

    fun subscribePaymentEvent(callback: PaymentEvents) {
        paymentEvent = callback
    }

    constructor(context: Context) : super(context) {
        initialize(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context, attrs)
    }


    private fun initialize(context: Context, attrs: AttributeSet?) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomLoader)
        var view: View? = null

        when (typedArray.getInt(R.styleable.CustomLoader_paymentType, 0)) {
            0 -> {
                view = inflate(context, R.layout.send_money_payment_frame, this);

            }
            1 -> {
                view = inflate(context, R.layout.invoice_payment_frame, this);
                indicator = -1

            }
        }
        view?.slide_btn?.setOnTouchListener(this)


    }


    override fun onTouch(view: View, event: MotionEvent?): Boolean {
        val x: Float = event?.rawX!!
        val deltaX = x - startMoveX
        halfOfWidth = view.width / 2
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                startMoveX = x
                return true
            }

            MotionEvent.ACTION_MOVE -> {
                Log.d("s", (deltaX / (ALPHA * indicator)).toString())
                view.alpha = deltaX / (ALPHA * indicator)
                return true
            }
            MotionEvent.ACTION_UP -> {
                if (deltaX * indicator > halfOfWidth) {
                    paymentEvent?.createEvent()
                }
                view.alpha = 0f
                return true
            }
        }
        return false

    }

    interface PaymentEvents {
        fun createEvent()
    }

}