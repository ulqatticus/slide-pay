package com.slidepay.clientt.view.scanner

interface MainPageListener {
    fun openScanner()
    fun openSendMoney()
}