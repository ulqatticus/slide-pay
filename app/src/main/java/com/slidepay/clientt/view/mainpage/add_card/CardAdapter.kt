package com.slidepay.clientt.view.mainpage.add_card

import android.support.annotation.IntDef
import android.support.annotation.LongDef
import android.support.annotation.StringDef
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.slidepay.clientt.R
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.presenter.mainpage.add_card.CardPresenter
import java.util.*

class CardAdapter(private val presenter: CardPresenter) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    companion object {

        @IntDef(EXIST, CREATE)
        @Retention(AnnotationRetention.SOURCE)
        annotation class CardType
        const val EXIST = 1
        const val CREATE = 2
    }

    private var cardList = ArrayList<CardBody>(0)

    var lastSelectedPosition = 0


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        Log.d("CardAdapter", "bind, position = $position")

        when (holder.itemViewType) {
            EXIST -> {
                val mHolder = holder as CardViewHolder
                holder.primaryRadioBtn.isChecked = lastSelectedPosition == position
                mHolder.bind(cardList[position])
            }
            CREATE -> {
                val mHolder = holder as AddCardViewHolder
                mHolder.bind()
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        when (viewType) {
            EXIST -> return CardViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_item, parent, false), this)
            CREATE -> return AddCardViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_item_add, parent, false), this)
        }
        return CardViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_item, parent, false), this)

    }

    override fun getItemViewType(position: Int): Int {
        return when (cardList[position].type) {
            1 -> EXIST
            2 -> CREATE
            else -> {
                EXIST
            }
        }
    }


    override fun getItemCount() = cardList.size


    fun addItem(list: List<CardBody>?) {
        list?.let {
            cardList.clear()
            cardList.addAll(it)
            notifyDataSetChanged()
        }
    }

    fun updateData(list: List<CardBody>?) {
        list?.let {
            cardList.clear()
            cardList.addAll(it)
        }
    }

    fun removeCard(id: Long, position: Int) {
        presenter.removeCard(id, position)
    }

    fun setDefaultCard(id: Long) {
        presenter.setDefaultCard(id)
    }

    fun renameCard(id: Long, text: String?) {
        presenter.renameCard(id, text)

    }

    fun createCard() {
        presenter.createCard()
    }

    fun acceptRemoveCard(position: Int) {
        cardList.removeAt(position)
        notifyDataSetChanged()
    }

    fun showKeyboard() {
        presenter.showKeyboard()

    }

    fun hideKeyboard() {
        presenter.hideKeyboard()


    }

    fun getData(): List<CardBody> {
        return cardList
    }




}