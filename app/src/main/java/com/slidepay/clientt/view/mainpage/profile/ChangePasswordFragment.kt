package com.slidepay.clientt.view.mainpage.profile

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.slidepay.clientt.R
import com.slidepay.clientt.contracts.main.profile.ChangePasswordContract
import com.slidepay.clientt.presenter.mainpage.profile.ChangePasswordPresenter
import com.slidepay.clientt.util.stringText
import com.slidepay.clientt.view.BaseFragment
import com.slidepay.clientt.view.modals.ModalBenefit
import com.slidepay.clientt.view.modals.PaymentModalFragment
import com.slidepay.clientt.view.widgets.EditTextTypeface
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.fragmnet_change_password.*


class ChangePasswordFragment : BaseFragment(), ChangePasswordContract.View {

    private var presenter = ChangePasswordPresenter(this)


    private val closeBtn: ImageView by lazy { change_pass_close }

    private val oldPass: EditTextTypeface by lazy { change_pass_old_password_type_value }
    private val newPass: EditTextTypeface by lazy { change_pass_type_new_pass_value }
    private val repeatNewPass: EditTextTypeface by lazy { change_pass_type_repeat_new_pass_value }
    private val errorText: TextTypeface by lazy { change_pass_error_block }


    companion object {
        fun create(): Fragment {
            return ChangePasswordFragment()
        }

        const val PASSWORD_LENGTH = 4
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragmnet_change_password, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.viewCreated()
        showFirst()
        showKeyboard()
    }


    private val oldPassText: String
        get() = oldPass.stringText()

    private val newPassText: String
        get() = newPass.stringText()

    private val newPassTextRepeat: String
        get() = repeatNewPass.stringText()


    override fun initKeyListeners() {

        RxTextView.afterTextChangeEvents(oldPass)
                .skipInitialValue()
                .map { it.editable().toString() }
                .filter { it.length == PASSWORD_LENGTH }
                .subscribe { showSecond() }

        RxTextView.afterTextChangeEvents(newPass)
                .skipInitialValue()
                .map { it.editable().toString() }
                .filter { it.length == PASSWORD_LENGTH }
                .subscribe { showThird() }

        RxTextView.afterTextChangeEvents(repeatNewPass)
                .skipInitialValue()
                .map { it.editable().toString() }
                .filter { it.length == PASSWORD_LENGTH }
                .subscribe { next() }

    }

    private fun showFirst() {
        oldPass.requestFocus()
        oldPass.isFocusableInTouchMode = true
        oldPass.isFocusable = true
        oldPass.isCursorVisible = true
    }


    private fun showSecond() {
        newPass.requestFocus()
        newPass.isFocusableInTouchMode = true
        newPass.isFocusable = true
    }

    private fun showThird() {
        repeatNewPass.requestFocus()
        repeatNewPass.isFocusableInTouchMode = true
        repeatNewPass.isFocusable = true
    }


    override fun showErrorMessage(message: String) {
        errorText.visibility = View.VISIBLE
        errorText.text = message
        clearAll()

    }

    private fun next() {
        hideKeyboard()
        errorText.visibility = View.GONE
        presenter.next(oldPassText, newPassText, newPassTextRepeat)
    }

    private fun clearAll() {
        oldPass.setText("")
        newPass.setText("")
        repeatNewPass.setText("")
    }

    override fun startSuccessFragment() {
        childFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .add(R.id.change_password_fragment_sent, PaymentModalFragment.create(ModalBenefit.PASSWORD_WAS_CHANGED) { close() })
                .commit()

    }

    override fun initCloseBtn() {
        closeBtn.setOnClickListener {
            hideKeyboard()
            close()
        }
    }

    override fun close() {
        fragmentManager?.popBackStackImmediate()
    }


}