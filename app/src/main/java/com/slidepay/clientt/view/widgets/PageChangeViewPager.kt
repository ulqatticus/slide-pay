package com.slidepay.clientt.view.widgets

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.support.v4.view.ViewPager
import android.support.v4.view.animation.LinearOutSlowInInterpolator
import android.util.AttributeSet
import android.view.MotionEvent

class PageChangeViewPager : ViewPager {

    private var mListener: PagerStateListener? = null

    private val mChangeListener: ViewPager.OnPageChangeListener

    private var mDragAnimator: ValueAnimator? = null

    private var isTouch: Boolean = true

    constructor(context: Context) : super(context) {
        mChangeListener = PagerListener()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mChangeListener = PagerListener()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        addOnPageChangeListener(mChangeListener)
    }

    override fun onDetachedFromWindow() {
        removeOnPageChangeListener(mChangeListener)
        super.onDetachedFromWindow()
    }

    override fun setCurrentItem(position: Int, smoothScroll: Boolean) {
        super.setCurrentItem(position, smoothScroll)
        if (mListener != null) {
            mListener!!.onPageChanged(position, false)
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (isTouch) {
            super.onTouchEvent(event)
        } else false

    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return if (isTouch) {
            super.onInterceptTouchEvent(event)
        } else false

    }

    override fun setCurrentItem(position: Int) {
        super.setCurrentItem(position)
        if (mListener != null) {
            mListener!!.onPageChanged(position, false)
        }
    }

    fun setOnPageChangedListener(listener: PagerStateListener?) {
        mListener = listener
    }

    fun getPageChangedListener(): ViewPager.OnPageChangeListener = mChangeListener


    fun smoothScrollNext(duration: Int) {
        if (mDragAnimator != null) {
            mDragAnimator!!.end()
        }
        mDragAnimator = ValueAnimator.ofInt(0, width)
        mDragAnimator!!.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                endFakeDrag()
            }

            override fun onAnimationCancel(animation: Animator) {
                endFakeDrag()
            }
        })

        mDragAnimator!!.interpolator = LinearOutSlowInInterpolator()
        mDragAnimator!!.addUpdateListener(FakeDragUpdateListener())

        mDragAnimator!!.duration = duration.toLong()
        beginFakeDrag()
        mDragAnimator!!.start()
    }

    override fun endFakeDrag() {
        if (isFakeDragging) {
            super.endFakeDrag()
        }
    }

    override fun beginFakeDrag(): Boolean = isFakeDragging || super.beginFakeDrag()




    private inner class PagerListener : ViewPager.OnPageChangeListener {

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            // Do nothing
        }


        override fun onPageSelected(position: Int) {
            if (mListener != null) {
                mListener!!.onPageChanged(position, true)
            }

        }

        override fun onPageScrollStateChanged(state: Int) {
            // Do nothing
        }
    }

    private inner class FakeDragUpdateListener : ValueAnimator.AnimatorUpdateListener {

        private var mOldDragPosition = 0

        override fun onAnimationUpdate(animation: ValueAnimator) {
            val dragPosition = animation.animatedValue as Int
            val dragOffset = dragPosition - mOldDragPosition
            mOldDragPosition = dragPosition
            if (!isFakeDragging) {
                beginFakeDrag()
            }
            fakeDragBy((-dragOffset).toFloat())
        }
    }

    interface PagerStateListener {
        fun onPageChanged(selectedPage: Int, fromUser: Boolean)
    }
}
