package com.slidepay.clientt.view.start_app

import android.os.Bundle
import com.slidepay.clientt.presenter.start.StartPresenter
import com.slidepay.clientt.view.BaseActivity

class StartActivity : BaseActivity() {
    private var presenter: StartPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = StartPresenter(this)
    }
}