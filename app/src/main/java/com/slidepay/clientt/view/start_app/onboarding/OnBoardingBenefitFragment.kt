package com.slidepay.clientt.view.start_app.onboarding


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.slidepay.clientt.R
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.fragment_benefit.view.*


class OnBoardingBenefitFragment : Fragment() {
    private lateinit var mBenefitIcon: ImageView
    private lateinit var mBenefitText: TextTypeface

    private var mBenefit: OnBoardingBenefit? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val benefit = arguments?.getString(BENEFIT_KEY, OnBoardingBenefit.ADD_CARD.name)
        mBenefit = OnBoardingBenefit.valueOf(benefit!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_benefit, container, false)
        mBenefitIcon = view.onboarding_item_icon
        mBenefitText = view.onboarding_item_text
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mBenefit?.drawableId != null) {
            mBenefitIcon.setImageResource(mBenefit!!.drawableId!!)
            mBenefitText.setText(mBenefit!!.textId!!)
        }
    }

    companion object {
        private const val BENEFIT_KEY = "benefit"

        fun create(benefit: OnBoardingBenefit): OnBoardingBenefitFragment {
            val bundle = Bundle()
            bundle.putString(BENEFIT_KEY, benefit.name)
            val fragment = OnBoardingBenefitFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
