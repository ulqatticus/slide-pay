package com.slidepay.clientt.view.mainpage.transaction

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.slidepay.clientt.R
import com.slidepay.clientt.model.objects.main.TransactionBody
import com.slidepay.clientt.presenter.mainpage.transaction.TransactionPresenter
import java.util.*


class TransactionsAdapter(private var presenter: TransactionPresenter) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var transactionsList: ArrayList<TransactionBody> = ArrayList(0)
    var previousExpandedPosition = -1
    var mExpandedPosition = -1
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            0 -> {
                val mHolder = holder as TransactionInvoiceViewHolder
                mHolder.bind(transactionsList[position])
            }
            1 -> {
                val mHolder = holder as TransactionSendMoneyViewHolder
                mHolder.bind(transactionsList[position])
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            0 -> return TransactionInvoiceViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_transaction_blue, parent, false), this)

            1 -> return TransactionSendMoneyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_transaction_green, parent, false), this)
        }
        return TransactionInvoiceViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_transaction_blue, parent, false), this)
       // return  return TransactionInvoiceViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_transaction_green, parent, false), EmployeeAdapter())
    }


    override fun getItemCount() = transactionsList.size

    override fun getItemViewType(position: Int): Int {
        return when (transactionsList[position].isInvoice) {
            false -> 1 // invoice
            true -> 0     // sendmoney

            else -> 3
        }
    }

    fun addItem(list: List<TransactionBody>?) {
        previousExpandedPosition = -1
        mExpandedPosition = -1
        list?.let {
            transactionsList.clear()
            transactionsList.addAll(it)
            notifyDataSetChanged()
        }
    }

    fun scrollToPosition(position: Int) {
        presenter.scrollToPosition(position)

    }

    fun retryInvoice(id: String?) {
        presenter.retryInvoice(id)
        previousExpandedPosition = -1
        mExpandedPosition = -1
    }

    fun cancelInvoice(id: String?) {
        presenter.cancelInvoice(id)
        previousExpandedPosition = -1
        mExpandedPosition = -1

    }

    fun cancelGetMoney(number: String?) {
        presenter.cancelGetMoney(number)
        previousExpandedPosition = -1
        mExpandedPosition = -1
    }

    fun retryGetMoney(number: String?) {
        presenter.retryGetMoney(number)
        previousExpandedPosition = -1
        mExpandedPosition = -1

    }


    fun payInvoice(transaction: TransactionBody) {
        presenter.payInvoice(transaction)
        previousExpandedPosition = -1
        mExpandedPosition = -1
    }

    fun changeInvoice(body: TransactionBody) {
        presenter.changeInvoice(body)
        previousExpandedPosition = -1
        mExpandedPosition = -1
    }


}