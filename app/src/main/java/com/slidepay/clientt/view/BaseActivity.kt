package com.slidepay.clientt.view

import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.slidepay.clientt.R
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.view.widgets.ProgressDialog

abstract class BaseActivity : AppCompatActivity() {

    private var isShowKeyboard = false
    private var progressDialog: ProgressDialog = ProgressDialog()


    fun showKeyboard() {
        if (!isShowKeyboard) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
            isShowKeyboard = true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (progressDialog.isVisible) {
            progressDialog.dismiss()
        }
        hideKeyboard()
    }

    fun hideActionBar() {
        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.cancelAll()
        supportActionBar?.hide()
    }


    fun hideKeyboard() {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = currentFocus
        if (view == null) {
            view = View(this)
        }
        isShowKeyboard = false
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }


    fun showProgress() {
        progressDialog.start(supportFragmentManager!!)
    }

    fun hideProgress() {
        progressDialog.dismiss()
    }

    override fun onStop() {
        super.onStop()
        hideKeyboard()
    }


}