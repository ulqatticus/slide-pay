package com.slidepay.clientt.view.invoice

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Spinner
import cn.refactor.multistatelayout.MultiStateLayout
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.slidepay.clientt.R
import com.slidepay.clientt.application.utils.Config
import com.slidepay.clientt.contracts.invoice.InvoiceContract
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.model.objects.main.InvoiceBodyResponse
import com.slidepay.clientt.model.objects.main.ScannedBody
import com.slidepay.clientt.presenter.invoice.InvoicePresenter
import com.slidepay.clientt.view.BaseActivity
import com.slidepay.clientt.view.alert_fragment.DescriptionDialogFragment
import com.slidepay.clientt.view.contacts.ContactFragment
import com.slidepay.clientt.view.invoice.bill.BillActivity
import com.slidepay.clientt.view.modals.ModalBenefit
import com.slidepay.clientt.view.modals.PaymentModalFragment
import com.slidepay.clientt.view.scanner.ScannerFragment
import com.slidepay.clientt.view.widgets.EditTextTypeface
import com.slidepay.clientt.view.widgets.PaymentFrame
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.info_no_internet.view.*
import kotlinx.android.synthetic.main.info_something_went_wrong.view.*
import kotlinx.android.synthetic.main.invoice_activity.*


class InvoiceActivity : BaseActivity(), InvoiceContract.View, PaymentFrame.PaymentEvents {


    val presenter = InvoicePresenter(this)

    private val spinner: Spinner by lazy { invoice_spinner }
    private val contactBtn: ImageView by lazy { invoice_contacts_img_btn }
    private val closeBtn: ImageView by lazy { invoice_close_scanner_btn }
    private val scannerBtn: TextTypeface by lazy { invoice_scanner_btn }
    private val addDescBtn: TextTypeface by lazy { invoice_add_description_btn }
    private val clintFieldNumber: EditTextTypeface by lazy { invoice_contacts_enter_number_field }
    private val amountField: EditTextTypeface by lazy { invoice_summ_field }
    private val paymentFrame: PaymentFrame by lazy { payment_frame }
    private val multiStateLayout: MultiStateLayout by lazy { multi_state_layout }

    private var scannedBody: ScannedBody? = null

    private var description: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.invoice_activity)
        presenter.viewCreated()

    }


    override fun startBillActivity(body: InvoiceBodyResponse) {
        val intent = Intent(this, BillActivity::class.java)
        intent.putExtra("body", body)
        startActivity(intent)
    }

    override fun initStateListener() {
        multiStateLayout.setOnStateViewCreatedListener { view, state ->

            when (state) {
                MultiStateLayout.State.NETWORK_ERROR -> {
                    val retry = view.info_no_internet_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }
                MultiStateLayout.State.ERROR -> {
                    val retry = view.something_went_wrong_btn
                    retry.setOnClickListener {
                        presenter.retryBtnClick()
                    }
                }

            }
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun initViews() {
        paymentFrame.subscribePaymentEvent(this)
        contactBtn.setOnClickListener { startContactActivity() }
        closeBtn.setOnClickListener { finish() }
        scannerBtn.setOnClickListener { startScanner() }
        addDescBtn.setOnClickListener { startDescriptionActivity() }
        clintFieldNumber.setOnClickListener { setCursorVisible(true) }


    }

    //region description
    override fun setDescription(description: String?) {

        if (description.isNullOrBlank()) {
            addDescBtn.text = "Добавить описание"
        } else {
            this.description = description
            addDescBtn.text = "Редактировать описание"
        }

    }


    private fun startDescriptionActivity() {
        DescriptionDialogFragment.newInstance(description) {
            setDescription(it)
        }.show(supportFragmentManager)
    }

    override fun getDescription(): String? {
        return description
    }

    //endregion

    override fun initUserCard(userCards: ArrayList<CardBody>) {
        val spinnerArrayAdapter = ArrayAdapter(this, R.layout.spinner_item, userCards)
        spinner.adapter = spinnerArrayAdapter
    }


    override fun startSuccessFragment() {
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .add(R.id.invoice_container, PaymentModalFragment.create(ModalBenefit.SENT_MONEY) {closeActivity()})
                .addToBackStack(PaymentModalFragment::class.java.name)
                .commit()

    }

    override fun getExtra() {
        scannedBody = intent.getParcelableExtra("content")
        if (scannedBody != null) {
            setInfo(scannedBody)
        }

    }

    private fun setInfo(scannedBody: ScannedBody?) {
        clintFieldNumber.setText(scannedBody?.phoneNumber)
    }

    private fun startScanner() {


        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        openScannerFragment()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                    }

                    override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {/* ... */
                        token.continuePermissionRequest()
                    }
                }).check()


    }

    private fun openScannerFragment() {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.invoice_container, ScannerFragment.create { presenter.getScannedContent(it) }, ScannerFragment::class.java.name)
                .addToBackStack(ScannerFragment::class.java.name)
                .commit()
    }

    private fun startContactActivity() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.READ_CONTACTS)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        openContactFragment()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                    }

                    override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {/* ... */
                        token.continuePermissionRequest()
                    }
                }).check()
    }

    private fun openContactFragment() {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.invoice_container, ContactFragment.create { setPhone(it) }, ContactFragment::class.java.name)
                .addToBackStack(ContactFragment::class.java.name)
                .commit()
    }


    private fun setCursorVisible(isVisible: Boolean) {
        clintFieldNumber.isCursorVisible = isVisible
    }


    override fun setPhone(phone: String) {
        clintFieldNumber.setText(phone)
        clintFieldNumber.setSelection(phone.length)
    }

    override fun getContentResolver(): ContentResolver {
        return super.getContentResolver()
    }


    override fun createInvoice() {
        presenter.createSendMoney()
    }

    override fun showError() {
        amountField.setBackgroundResource(R.drawable.background_error_edit)
        //  invoiceCreateBtn.alpha = 0f
    }

    override fun getAmount(): String? {
        return amountField.text.toString()
    }


    override fun getCardId(): Long? {
        val st = spinner.selectedItem as CardBody
        return st.id
    }


    override fun getPhoneNumber(): String? {
        return clintFieldNumber.text.toString()
    }

    override fun showLoading() {

        super.showProgress()
    }

    override fun hideLoading() {
        super.hideProgress()

    }


    override fun createEvent() {
        createInvoice()
    }

    override fun closeActivity() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.viewDestroyed()
    }

    override fun showSomethingWentWrong() {
        multiStateLayout.state = MultiStateLayout.State.ERROR
    }

    override fun showContent() {
        multiStateLayout.state = MultiStateLayout.State.CONTENT
    }

    override fun showNoInternetConnection() {
        multiStateLayout.state = MultiStateLayout.State.NETWORK_ERROR
    }

    companion object {
        fun startInvoiceActivity(activity: AppCompatActivity, content: ScannedBody) {
            val intent = Intent(activity, InvoiceActivity::class.java)
            intent.putExtra("content", content)
            activity.startActivityForResult(intent, Config.RequestCode.CREATE_INVOICE_REQUEST_CODE)
        }

        fun startInvoiceActivity(activity: AppCompatActivity) {
            val intent = Intent(activity, InvoiceActivity::class.java)
            activity.startActivityForResult(intent, Config.RequestCode.CREATE_INVOICE_REQUEST_CODE)
        }

    }

}