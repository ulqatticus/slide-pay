package com.slidepay.clientt.view.start_app.onboarding

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.slidepay.clientt.R
import com.slidepay.clientt.contracts.start.OnBoardingContract
import com.slidepay.clientt.presenter.start.onboarding.OnBoardingPresenter
import com.slidepay.clientt.view.BaseActivity
import com.slidepay.clientt.view.start_app.registration.type_number.TypeNumberActivity
import com.slidepay.clientt.view.widgets.PageChangeViewPager
import com.slidepay.clientt.view.widgets.TextTypeface
import kotlinx.android.synthetic.main.onboarding_activity.*
import me.relex.circleindicator.CircleIndicator


class OnBoardingActivity : BaseActivity(), OnBoardingContract.View, PageChangeViewPager.PagerStateListener {


    val presenter = OnBoardingPresenter(this)

    private val mPager: PageChangeViewPager by lazy { onboarding_view_pager }
    private val indicator: CircleIndicator by lazy { onboarding_indicators }
    private val mActionButton: TextTypeface by lazy { onboarding_view_btn_walkthrough }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.onboarding_activity)
        hideActionBar()

        presenter.viewCreated()

        mActionButton.setOnClickListener { presenter.btnClick() }
        mPager.adapter = OnBoardingAdapter(supportFragmentManager, presenter.benefits)
        mPager.setOnPageChangedListener(this)
        indicator.setViewPager(mPager)

    }


    override fun onPageChanged(selectedPage: Int, fromUser: Boolean) {
        if (fromUser) {
            presenter.showBenefit(selectedPage)
        }
    }

    override fun showSkipBtn() {
        mActionButton.visibility = View.VISIBLE
    }

    override fun hideSkipBtn() {
        mActionButton.visibility = View.GONE
    }

    override fun getCurrentItemPager(): Int = mPager.currentItem


    override fun showBenefit() {
        mPager.smoothScrollNext(resources.getInteger(android.R.integer.config_mediumAnimTime))
    }



    override fun nextActivity() {
        startActivity(Intent(this, TypeNumberActivity::class.java))
        finish()
    }

    companion object {
        const val PAGES_COUNT = 4
    }
}