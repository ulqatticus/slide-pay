package com.slidepay.clientt.view.scanner

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.google.gson.annotations.SerializedName
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.CaptureManager
import com.slidepay.clientt.R
import com.slidepay.clientt.contracts.main.scanner.ScannerContract
import com.slidepay.clientt.presenter.scanner.ScannerPresenter
import kotlinx.android.synthetic.main.fragment_scanner.*

class ScannerFragment : Fragment(), ScannerContract.View {


    private lateinit var captureManager: CaptureManager

    private val closeBtn: ImageView by lazy { camera_activity_close_scanner_btn }
    private val flashlightBtn: ImageView by lazy { camera_activity_flashlight_btn }

    private var presenter: ScannerPresenter = ScannerPresenter(this)
    private var isFlashOn = false


    var getContent: (String) -> Unit = {}


    companion object {
        fun create(getContent: (String) -> Unit = {}): Fragment {
            return ScannerFragment().apply {
                this.getContent = getContent
            }
        }

        const val CAMERA_REQUEST = 1001
        const val VIBRATION_DURATION = 200L

    }
    //region Start


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_scanner, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        captureManager = CaptureManager(requireActivity(), camera_activity_scan)
        captureManager.initializeFromIntent(requireActivity().intent, savedInstanceState)


        val icon = BitmapFactory.decodeResource(
                resources,
                R.drawable.ic_scanner_frame
        )
        camera_activity_scan.viewFinder.drawResultBitmap(icon)
        camera_activity_scan.setStatusText("")

        camera_activity_scan.decodeSingle(object : BarcodeCallback {
            override fun barcodeResult(result: BarcodeResult?) {
                result?.let {
                    vibrate()
                    presenter.getQrCode(it.text)
                }
            }

            override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
            }
        })

        presenter.viewCreated()

    }



    //endregion


    //region initComponent
    override fun initCloseBtn() {
        closeBtn.setOnClickListener { finishView() }
    }


    override fun initFlashlightBtn() {
        flashlightBtn.setOnClickListener {
            onOffLight()
        }
    }
    //endregion

    //region Permission
    override fun checkPermission() {
        if (ActivityCompat.checkSelfPermission(requireContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(android.Manifest.permission.CAMERA), CAMERA_REQUEST)
            return
        }
    }

    //endregion


    //region FlashLight
    override fun isFlashLightAvailable(): Boolean = requireContext().packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)


    override fun hideFlashLightBtn() {
        flashlightBtn.visibility = View.GONE
    }

    override fun flashLightDeactivated() {
        flashlightBtn.isActivated = false

    }

    override fun flashLightActivate() {
        flashlightBtn.isActivated = true
    }
    //endregion

    //region Finish
    override fun finishWithSuccessResult(value: String) {
        getContent(value)
        finishView()
    }


    private fun vibrate() {
        val vib: Vibrator = requireContext().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if (vib.hasVibrator()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vib.vibrate(VibrationEffect.createOneShot(VIBRATION_DURATION, VibrationEffect.DEFAULT_AMPLITUDE))
            } else {
                // This method was deprecated in API level 26
                vib.vibrate(VIBRATION_DURATION)
            }
        }
    }


    override fun finishView() {
        fragmentManager?.popBackStackImmediate()
    }


    //endregion


    override fun onPause() {
        super.onPause()
        captureManager.onPause()
    }

    override fun onResume() {
        super.onResume()
        captureManager.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        captureManager.onDestroy()
        presenter.viewDestroyed()

    }


    override fun initFlashLight() {
        if (isFlashLightAvailable()) {
            initFlashlightBtn()
        } else {
            hideFlashLightBtn()
        }
    }


    private fun onOffLight() {
        if (isFlashOn) {
            isFlashOn = false
            camera_activity_scan.setTorchOff()
        } else {
            isFlashOn = true
            camera_activity_scan.setTorchOn()
        }
        flashlightBtn.isActivated = isFlashOn

    }


    class ContentBody(@SerializedName("Content") var content: String)

}
