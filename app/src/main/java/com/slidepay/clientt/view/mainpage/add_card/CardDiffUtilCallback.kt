package com.slidepay.clientt.view.mainpage.add_card

import android.support.v7.util.DiffUtil
import com.slidepay.clientt.model.objects.main.CardBody


class CardDiffUtilCallback(private val oldList: List<CardBody>, private val newList: List<CardBody>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldCard = oldList[oldItemPosition]
        val newCard = newList[newItemPosition]
        return oldCard.id == newCard.id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldCard = oldList[oldItemPosition]
        val newCard = newList[newItemPosition]
        return oldCard.isPrimary == newCard.isPrimary && oldCard.cardBankName === newCard.cardBankName && oldCard.name === newCard.name
    }
}