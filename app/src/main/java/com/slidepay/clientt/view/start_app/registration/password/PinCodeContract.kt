package com.slidepay.clientt.view.start_app.registration.password

import com.slidepay.clientt.contracts.BaseViewInterface
import com.slidepay.clientt.view.BaseActivity

interface PinCodeContract {
    interface View:BaseViewInterface {
        fun clearTextInEditText()
        fun hideError()
        fun showError(error: String)
        fun showFingerPrintBtn()
        fun hideFingerPrintBtn()
        fun hideDivider()
        fun showDivider()
        fun setListenerToEditText()
        fun setLabelText(text: String)
        fun startNextActivity()
        fun closeActivity()
        fun showForgotPass()
        fun showLoading()
        fun hideLoading()
        fun startConfirmCodeForRestorePass(phoneNumber: String)
        fun showKeyBoard()
        fun hideKeyBoard()
        fun getActivity(): BaseActivity
        fun finishActivity()
        fun hideForgotPass()
    }

    interface Presenter {
        fun viewIsReady()
        fun onResume()
        fun next(password: String)
        fun startScanFingerPrint()
        fun onBackPressed()
        fun onStop()
        fun forgotPassClick()
    }
}