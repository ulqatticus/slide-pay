package com.slidepay.clientt.util

import android.widget.TextView

fun TextView.stringText() = text.toString()
