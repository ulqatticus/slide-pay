package com.slidepay.clientt.application

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import cn.refactor.multistatelayout.MultiStateConfiguration
import cn.refactor.multistatelayout.MultiStateLayout
import com.crashlytics.android.Crashlytics
import com.slidepay.clientt.BuildConfig
import com.slidepay.clientt.R
import com.slidepay.clientt.application.interceptors.DefaultAuthInterceptor
import com.slidepay.clientt.application.interceptors.ExpiredTokenInterceptor
import com.slidepay.clientt.application.utils.Config
import com.slidepay.clientt.model.database.TokenDataBase
import io.fabric.sdk.android.Fabric
import io.realm.Realm
import io.realm.RealmConfiguration
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class App : Application() {
    override fun onCreate() {
        super.onCreate()
        app = this
        Realm.init(applicationContext)
        Realm.setDefaultConfiguration(RealmConfiguration.Builder().name(Config.Database.name).build())
        configStateLayout()
        initCrashlitic()
    }

    private fun initCrashlitic() {
        Fabric.with(this, Crashlytics())
    }

    private fun configStateLayout() {
        val builder = MultiStateConfiguration.Builder()
        builder.setCommonErrorLayout(R.layout.info_something_went_wrong)
                .setCommonNetworkErrorLayout(R.layout.info_no_internet)
                .setCommonLoadingLayout(R.layout.preloader)
        MultiStateLayout.setConfiguration(builder)
    }


    companion object {
        lateinit var app: App

        fun getContext(): Context = app.applicationContext

        fun getPreferences(): SharedPreferences = getContext().getSharedPreferences(Config.Preferences.baseName, Context.MODE_PRIVATE)

        fun getRealm(): Realm {
            return try {
                Realm.getDefaultInstance()
            } catch (e: Exception) {
                Realm.deleteRealm(Realm.getDefaultConfiguration())
                Realm.getDefaultInstance()
            }
        }

        fun retrofitNotToken(baseUrl: String): Retrofit {
            return Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(createDefaultClient())
                    .build()
        }

        fun retrofitExpiredToken(baseUrl: String): Retrofit {
            return Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(createExpiredTokenClient())
                    .build()
        }

        private fun createExpiredTokenClient(): OkHttpClient {
            var token = TokenDataBase.getUserTokenToken()?.token

            /*   if (token == null) {
                   token = TokenDataBase.getTempUserToken()?.token
               }*/

            val builder = OkHttpClient.Builder()
            // builder.connectionSpecs(Collections.singletonList(connectionSpec()))

            builder.retryOnConnectionFailure(true)
            builder.interceptors().add(DefaultAuthInterceptor())
            builder.interceptors().add(ExpiredTokenInterceptor(token!!))
            builder.connectTimeout(Config.HttpTimeConnect.default_connect, TimeUnit.SECONDS)
            builder.readTimeout(Config.HttpTimeConnect.default_connect, TimeUnit.SECONDS)
            builder.writeTimeout(Config.HttpTimeConnect.default_connect, TimeUnit.SECONDS)
            if (BuildConfig.DEBUG) {
                val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message -> Log.d("OkHttp", message) })
                logger.level = HttpLoggingInterceptor.Level.BODY
                builder.addInterceptor(logger)
            }

            return builder.build()
        }

        private fun createDefaultClient(): OkHttpClient {
            val builder = OkHttpClient.Builder()
            builder.retryOnConnectionFailure(true)
            builder.interceptors().add(DefaultAuthInterceptor())
            builder.connectTimeout(Config.HttpTimeConnect.default_connect, TimeUnit.SECONDS)
            builder.readTimeout(Config.HttpTimeConnect.default_connect, TimeUnit.SECONDS)
            builder.writeTimeout(Config.HttpTimeConnect.default_connect, TimeUnit.SECONDS)

            if (BuildConfig.DEBUG) {
                val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message -> Log.d("OkHttp", message) })
                logger.level = HttpLoggingInterceptor.Level.BODY
                builder.addInterceptor(logger)
            }

            return builder.build()
        }


    }
}