package com.slidepay.clientt.application.utils

object Config {

    object Server {

        const val serverUrl = "https://api.slidepay.com.ua"

        const val businessAuth = "$serverUrl/business/acl/auth"
        const val businessIntegration = "$serverUrl/business/integration/"
        const val businessSecurity = "$serverUrl/business/security/"

        const val clientAuth = "$serverUrl/client/auth/"
        const val clientBill = "$serverUrl/client/bill/"

        const val clientProfile = "$serverUrl/client/profile/"
        const val clientCard = "$serverUrl/client/card/"
        const val clientInvoice = "$serverUrl/client/invoice/"

        const val clientPayment = "$serverUrl/client/payment/"
        const val clientTransaction = "$serverUrl/client/transaction/"
        const val clientSecurity = "$serverUrl/client/security/"
    }

    object Database {
        const val name = "slidepay"
    }

    object Preferences {
        const val baseName = "slide_pay"
    }

    object OkhttpHeaders {
        const val contentTypeNameJson = "Content-Type"
        const val contentTypeValueJson = "application/json"
        const val contentTypeNameToken = "Token"
    }

    object HttpTimeConnect {
        const val default_connect: Long = 30
    }


    enum class PinCodeMode {
        CREATE, CHECK, RESTORE
    }

    val EXTRA_MODE = "mode"

    object RequestCode {

        const val CREATE_INVOICE_REQUEST_CODE = 2000
        const val CREATE_SEND_MONEY_REQUEST_CODE = 2001


        const val CREATE_NEW_CARD_REQUEST_CODE = 3001
        const val LOAD_IMG_REQUEST_CODE = 4000


    }

    object ErrorCode {
        const val EXPIRE_TOKEN_CODE = 8
        const val LIMIT_PASS_ENTER_TRYING = 7
        const val ERROR_CHECK_CODE = 1
    }
}