package com.slidepay.clientt.application

import io.realm.Realm

inline fun Any.realm(call: (realm: Realm) -> Unit) {
    val realm = App.getRealm()
    realm.beginTransaction()
    call.invoke(realm)
    realm.commitTransaction()
}