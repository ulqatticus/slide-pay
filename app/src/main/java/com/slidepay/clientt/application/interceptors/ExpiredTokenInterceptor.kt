package com.slidepay.clientt.application.interceptors

import com.google.gson.Gson
import com.slidepay.clientt.model.api.ServerApi
import com.slidepay.clientt.model.database.TokenDataBase
import com.slidepay.clientt.model.objects.ErrorData
import com.slidepay.clientt.application.utils.Config

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody

import com.slidepay.clientt.application.utils.Config.ErrorCode.EXPIRE_TOKEN_CODE
import com.slidepay.clientt.model.objects.RefreshTokenBody


class ExpiredTokenInterceptor(private val token: String) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var originalRequest = chain.request()

        originalRequest = originalRequest.newBuilder()
                .header(Config.OkhttpHeaders.contentTypeNameJson, Config.OkhttpHeaders.contentTypeValueJson)
                .header(Config.OkhttpHeaders.contentTypeNameToken, token)
                .build()

        val response = chain.proceed(originalRequest)

        if (!response.isSuccessful) {
            synchronized(this) {
                if (response.body() != null) {
                    val gson = Gson()
                    try {
                        val reader = BufferedReader(InputStreamReader(response.body()!!.byteStream()))
                        val sb = StringBuilder()

                        reader.forEachLine {
                            sb.append(it)
                        }

                        try {
                            val errorData = gson.fromJson(sb.toString(), ErrorData::class.java)
                            if (errorData != null && errorData.errorCode == EXPIRE_TOKEN_CODE) {

                                val refreshToken = TokenDataBase.getRefreshToken()


                                val refreshTokenBody = RefreshTokenBody()
                                refreshTokenBody.refreshToken = refreshToken

                                val call = ServerApi.token.getNewAccessToken(refreshTokenBody)
                                val newToken = call.execute().body()

                                saveNewTokenToDB(newToken!!.token!!)

                                val secondModifiedRequest = originalRequest.newBuilder()
                                        .header(Config.OkhttpHeaders.contentTypeNameToken, newToken.token)
                                        .build()

                                return chain.proceed(secondModifiedRequest)
                            } else {
                                return response.newBuilder().body(ResponseBody.create(response.body()!!.contentType(), sb.toString())).build()
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }

                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
            }
        }

        return response
    }

    private fun saveNewTokenToDB(token: String) {
        if (TokenDataBase.getUserTokenToken() != null) {
            TokenDataBase.updateClientToken(token)
        } /*else {
            TokenDataBase.updateTempUserAccessToken(token)
        }*/
    }
}