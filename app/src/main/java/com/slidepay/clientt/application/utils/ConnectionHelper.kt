package com.slidepay.clientt.application.utils

import android.content.Context
import android.net.ConnectivityManager
import com.slidepay.clientt.application.App


class ConnectionHelper {

    fun isOnline(): Boolean {
        val cm = App.getContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = cm.activeNetworkInfo

        var isConnectedWifi = false
        var isConnectedMobile = false


        if (info?.type == ConnectivityManager.TYPE_WIFI) {
            isConnectedWifi = info.isConnectedOrConnecting
        }

        if (info?.type == ConnectivityManager.TYPE_MOBILE) {
            isConnectedMobile = info.isConnectedOrConnecting
        }

        var isInternetConnect = false

        if (isConnectedMobile || isConnectedWifi) {
            isInternetConnect = true
        }


        return isInternetConnect
    }




}