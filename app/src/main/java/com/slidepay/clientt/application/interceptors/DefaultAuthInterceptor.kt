package com.slidepay.clientt.application.interceptors


import com.slidepay.clientt.application.utils.Config

import java.io.IOException

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class DefaultAuthInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val newRequest: Request
        newRequest = request.newBuilder()
                .addHeader(Config.OkhttpHeaders.contentTypeNameJson, Config.OkhttpHeaders.contentTypeValueJson)
                .build()

        return chain.proceed(newRequest)
    }
}