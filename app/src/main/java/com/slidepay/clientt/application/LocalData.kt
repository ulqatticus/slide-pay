package com.slidepay.clientt.application

object LocalData {

    var isWalkthroughPassed: Boolean
        get() = App.getPreferences().getBoolean("isWalkthroughPassed", false)
        set(value) = App.getPreferences().edit().putBoolean("isWalkthroughPassed", value).apply()

    var isAuthorizationIsEnded: Boolean
        get() = App.getPreferences().getBoolean("isAuthorizationIsEnded", false)
        set(value) = App.getPreferences().edit().putBoolean("isAuthorizationIsEnded", value).apply()

    var pinCode: String
        get() = App.getPreferences().getString("pass", "")
        set(value) = App.getPreferences().edit().putString("pass", value).apply()

    var qrContent: String
        get() = App.getPreferences().getString("qrContent", "")
        set(value) = App.getPreferences().edit().putString("qrContent", value).apply()

    var isProfileWrite: Boolean
        get() = App.getPreferences().getBoolean("isProfileWrite", false)
        set(value) = App.getPreferences().edit().putBoolean("isProfileWrite", value).apply()

}