package com.bottlemessage.btlmsg.application.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.slidepay.clientt.BuildConfig
import com.slidepay.clientt.R
import com.slidepay.clientt.view.start_app.StartActivity


class FBMessgService : FirebaseMessagingService() {

    companion object {
        const val NOTIFICATION_CHANEL = "slidepay_push"
    }

    /*value[0] = "normal"
value[1] = {Long@7724} 1562488567148
value[2] = {Integer@6454} 108
value[3] = "normal"
 value[4] = "И описание"
value[5] = "726685624424"
value[6] = "07.07.2019 11:36:07"
value[7] = "0"   -- type
value[8] = "Вам отправили деньги"
value[9] = "0:1562488567152978%ac698d3f66d6cf16"*/


    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        remoteMessage?.let {
            sendNotification(remoteMessage)
        }
    }

    /**
     * Sent notification to device
     * @param remoteMessage - message which get from server
     * @see getBuilder
     * @see getPendingIntent
     */
    private fun sendNotification(remoteMessage: RemoteMessage?) {

        val id = System.currentTimeMillis().toInt()
        // for android V >= 8
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channelID = BuildConfig.APPLICATION_ID
            val channel = NotificationChannel(NOTIFICATION_CHANEL, BuildConfig.APPLICATION_ID, importance)
            channel.description = channelID
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager!!.createNotificationChannel(channel)
        }

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(id, getBuilder(remoteMessage).build())
    }

    /**
     * create builder for notification
     * @return NotificationCompat.Builder
     * @param remoteMessage - message which get from server
     * @see sendNotification
     */
    private fun getBuilder(remoteMessage: RemoteMessage?): NotificationCompat.Builder {
        val messageTitle = remoteMessage?.notification?.title
        val messageBody = remoteMessage?.notification?.body
        return NotificationCompat.Builder(this, NOTIFICATION_CHANEL)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setContentIntent(getPendingIntent())
    }


    /**
     * notification click  action. Launch StartActivity
     * @return PendingIntent
     * @see getBuilder
     */
    private fun getPendingIntent(): PendingIntent? {
        val resultIntent = Intent(this, StartActivity::class.java)
        return PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT)
    }


    enum class MessageType(val type: Int) {
        BASIC(0),
        GETMONEY(1),
        INVOCIE(2),
    }


/*    override fun onDeletedMessages() {
        Toast.makeText(this, "delete", Toast.LENGTH_SHORT).show()
    }*/
}