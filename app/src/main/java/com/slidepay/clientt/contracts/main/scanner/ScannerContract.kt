package com.slidepay.clientt.contracts.main.scanner

import com.slidepay.clientt.contracts.BasePresenterInterface

interface ScannerContract {
    interface View {
        fun initFlashlightBtn()
        fun initCloseBtn()
        fun finishView()
        fun isFlashLightAvailable(): Boolean
        fun hideFlashLightBtn()
        fun finishWithSuccessResult(value: String)
        fun checkPermission()
        fun flashLightActivate()
        fun flashLightDeactivated()
        fun initFlashLight()

    }

    interface Presenter : BasePresenterInterface {
        fun getQrCode(value: String?)
    }


}