package com.slidepay.clientt.contracts.invoice

import android.graphics.Bitmap
import com.slidepay.clientt.contracts.BasePresenterInterface
import com.slidepay.clientt.contracts.BaseViewInterface
import com.slidepay.clientt.model.objects.main.InvoiceBodyResponse

interface BillContract {
    interface View  {

        fun showLoading()
        fun hideLoading()
        fun getQrImgWidth(): Int
        fun getQrImgHeight(): Int
        fun setDescription(description: String?)
        fun setQrImage(qrImage: Bitmap?)
        fun setName(payerFullName: String?)
        fun setSum(sum: Double?)
        fun initCloseBtn()
    }

    interface Presenter : BasePresenterInterface {

        fun viewCreated(invoiceBody: InvoiceBodyResponse)
    }
}