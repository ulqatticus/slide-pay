package com.slidepay.clientt.contracts.main.profile

import com.slidepay.clientt.contracts.BasePresenterInterface

interface ChangePasswordContract {

    interface View {
        fun initCloseBtn()
        fun initKeyListeners()
        fun showErrorMessage(message: String)
        fun close()
        fun startSuccessFragment()
    }

    interface Presenter : BasePresenterInterface {
        fun next(oldPassText: String, newPasstext: String, newPassTextRepeat: String)

    }
}