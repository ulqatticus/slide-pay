package com.slidepay.clientt.contracts

interface BaseViewInterface {
    fun showSomethingWentWrong()
    fun showNoInternetConnection()
    fun showContent()
    fun initStateListener()
}