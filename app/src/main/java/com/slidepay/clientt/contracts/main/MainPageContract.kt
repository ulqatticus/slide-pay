package com.slidepay.clientt.contracts.main

import android.support.design.widget.TabLayout
import com.slidepay.clientt.contracts.BasePresenterInterface
import com.slidepay.clientt.contracts.BaseViewInterface
import com.slidepay.clientt.model.objects.main.ScannedBody

interface MainPageContract {
    interface View : BaseViewInterface {
        fun initUIComponent()
        fun setDivider(tabLayout: TabLayout, padding: Int)
        fun closeApp()
        fun setTabItem(tab: TabLayout.Tab?)
        fun startPaymentActivity(content: ScannedBody)
        fun startPayActivity(content: ScannedBody)
        fun showLoader()
        fun hideLoader()
        fun showScannError()
    }

    interface Presenter : BasePresenterInterface {
        fun retryBtnClick()
        fun getScannedContent(it: String)
    }


}