package com.slidepay.clientt.contracts.start

import android.content.Context
import android.text.SpannableString
import com.slidepay.clientt.contracts.BasePresenterInterface
import com.slidepay.clientt.contracts.BaseViewInterface

interface CheckCodeContract {
    interface View : BaseViewInterface{
        fun nextActivity()
        fun hideKeyBoard()
        fun showKeyBoard()
        fun showLoading()
        fun hideLoading()
        fun showError()
        fun hideError()
        fun editCodeSetText(newText: StringBuilder)
        fun editCodeSetSelection(length: Int)
        fun setTimerSeconds(spannable: SpannableString)
        fun setTimerVisible()
        fun setTimerInvisible()
        fun setResendBtnVisible()
        fun setResendBtnInvisible()
        fun getContext(): Context?
        fun startSuccessFragment()
        fun getPhoneNUmber(): String
        fun closeApp()
    }

    interface Presenter : BasePresenterInterface {
        fun afterTextChanged(text: String)
        fun closeActivity()
        fun retryBtnClick()
        fun onBackPressed()
    }


}