package com.slidepay.clientt.contracts.main.payment

import com.slidepay.clientt.contracts.BasePresenterInterface

interface PaymentContract {

    interface View {
        fun initClickListeners()
        fun setClientInformation()
        fun closePaymentFragment()
    }

    interface Presenter : BasePresenterInterface
}