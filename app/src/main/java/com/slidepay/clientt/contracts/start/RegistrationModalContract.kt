package com.slidepay.clientt.contracts.start

import android.content.Context
import com.slidepay.clientt.contracts.BasePresenterInterface
import com.slidepay.clientt.view.start_app.registration.RegistrationModalActivity

interface RegistrationModalContract {
    interface View {
        fun setRuleImage(imageResource: Int)
        fun setRuleText(ruleText: String)
        fun checkAppPermission(permission: String)
        fun acceptButtonPermission()
        fun laterButtonPermission()
        fun getContext(): Context
        fun startAddCardActivity()
        fun startCreateCardActivity()
        fun startCreatePasswordActivity()
        fun setButtonText(text: String)
    }

    interface Presenter : BasePresenterInterface {
        fun setRule(rule: RegistrationModalActivity.Rules)
        fun clickAcceptBtn()
        fun clickNextBtn()

    }


}