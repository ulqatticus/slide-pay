package com.slidepay.clientt.contracts.main.qrcode

import android.graphics.Bitmap
import com.slidepay.clientt.contracts.BasePresenterInterface
import com.slidepay.clientt.contracts.BaseViewInterface
import com.slidepay.clientt.model.objects.main.ScannedBody

interface QrCodeContract {

    interface View : BaseViewInterface {
        fun generateQRImage()
        fun getQrImgWidth(): Int
        fun getQrImgHeight(): Int
        fun startScannerActivity()
        fun showError()
        fun showLoading()

    }

    interface Presenter : BasePresenterInterface {
        fun createQrBitmapImg(): Bitmap?
        fun retryBtnClick()
    }
}