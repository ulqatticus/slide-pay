package com.slidepay.clientt.contracts.start

import com.slidepay.clientt.contracts.BasePresenterInterface

interface OnBoardingContract {
    interface View {
        fun nextActivity()
        fun showBenefit()
        fun getCurrentItemPager(): Int
        fun showSkipBtn()
        fun hideSkipBtn()
    }

    interface Presenter : BasePresenterInterface {
        fun showBenefit(selectedPage: Int)

    }


}