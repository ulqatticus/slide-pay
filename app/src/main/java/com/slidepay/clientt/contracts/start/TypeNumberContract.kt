package com.slidepay.clientt.contracts.start

import android.content.Context
import android.text.SpannableString
import android.view.KeyEvent
import com.slidepay.clientt.contracts.BasePresenterInterface
import com.slidepay.clientt.contracts.BaseViewInterface
import com.slidepay.clientt.view.widgets.TextTypeface

interface TypeNumberContract  {

    interface View: BaseViewInterface {
        fun initNextBtn()
        fun showErrorPhone()
        fun hideErrorPhone()
        fun hideKeyBoard()
        fun showKeyBoard()
        fun getNumber(): String
        fun nextActivity()
        fun showLoading()
        fun hideLoading()
        fun checkBoxIsChecked(): Boolean
        fun checkBoxShowError()
        fun initSpanText(spanText: TextTypeface)
        fun startWebFragment()
        fun getContext(): Context

    }

    interface Presenter : BasePresenterInterface {
        fun watchOnTextView(text: String)
        fun clickNextBtn()
        fun settingNumberField(event: KeyEvent?, actionId: Int)
        fun startWebFragment()
        fun getSpanString(): SpannableString
        fun retryBtnClick()
    }


}