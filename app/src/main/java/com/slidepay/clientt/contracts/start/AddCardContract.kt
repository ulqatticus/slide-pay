package com.slidepay.clientt.contracts.start

import android.app.Activity
import com.slidepay.clientt.contracts.BasePresenterInterface
import com.slidepay.clientt.contracts.BaseViewInterface
import com.slidepay.clientt.view.start_app.registration.create_card.CreateCardActivity

interface AddCardContract {
    interface View : BaseViewInterface {
        fun startCreatePasswordActivity()
        fun hideKeyBoard()
        fun showKeyBoard()
        fun loadWebView(url: String)
        fun showLoading()
        fun hideLoading()
        fun configWebView()
        fun showInvalidUrlError()
        fun loadUrl(url: String)
        fun initAddCardBtn()
        fun showFirstBlock()
        fun hideFirstBlock()
        fun showSecondBlock()
        fun hideSecondBlock()
        fun initCloseBtn()
        fun showValidationError()
        fun hideError()
        fun hideErrorCardName()
        fun showErrorCardName()
        fun initTextWatcher()
        fun startSuccessFragment()
        fun getActivity(): Activity
        fun showTextToSkip(skipText: String?)
        fun getCardNumber(): String
    }

    interface Presenter : BasePresenterInterface {
        fun startSuccessFragment()
        fun loadWebView(url: String)
        fun sendRequest(cardNumber: String)
        fun checkUrlToRedirect(url: String)
        fun viewCreated(type: CreateCardActivity.Type)
        fun retryBtnClick()

    }


}