package com.slidepay.clientt.contracts

interface BasePresenterInterface {
    fun viewDestroyed()
    fun viewCreated()

}