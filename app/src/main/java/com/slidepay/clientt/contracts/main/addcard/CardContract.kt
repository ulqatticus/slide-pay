package com.slidepay.clientt.contracts.main.addcard

import com.slidepay.clientt.contracts.BasePresenterInterface
import com.slidepay.clientt.contracts.BaseViewInterface
import com.slidepay.clientt.model.objects.main.CardBody

interface CardContract {

    interface View: BaseViewInterface {
        fun addItems(list: List<CardBody>)
        fun initRecyclerView()
        fun showLoading()
        fun createCard()
        fun removeCard(position: Int)
        fun updateAdapter()


        fun showKeyboard()
        fun hideKeyboard()
        fun updateItems(body: ArrayList<CardBody>)
    }

    interface Presenter : BasePresenterInterface {
        fun removeCard(id: Long, position: Int)
        fun setDefaultCard(id: Long)
        fun renameCard(id: Long, text: String?)
        fun createCard()
        fun retryBtnClick()

    }
}