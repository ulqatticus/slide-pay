package com.slidepay.clientt.contracts.contacts

import com.slidepay.clientt.contracts.BasePresenterInterface
import com.slidepay.clientt.contracts.BaseViewInterface
import com.slidepay.clientt.model.objects.main.ContactsBody
import com.slidepay.clientt.model.objects.main.ContactsBodySend

interface ContactContract {
    interface View : BaseViewInterface {

        fun initContactList()
        fun addItems(arrayList: ArrayList<ContactsBody>)
        fun pickedContact(number: String)
        fun initCloseBtnListener()
        fun showLoading()
        fun hideLoading()
        fun initMultiStateListener()
        fun getContacts()
    }

    interface Presenter : BasePresenterInterface {
        fun retryBtnClick()
        fun getContacts(contactList: ArrayList<ContactsBodySend>)
    }
}