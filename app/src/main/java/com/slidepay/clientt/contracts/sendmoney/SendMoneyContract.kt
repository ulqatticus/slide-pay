package com.slidepay.clientt.contracts.sendmoney

import com.slidepay.clientt.contracts.BasePresenterInterface
import com.slidepay.clientt.contracts.BaseViewInterface
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.model.objects.main.ScannedBody
import com.slidepay.clientt.presenter.sendmoney.SendMoneyType

interface SendMoneyContract {
    interface View: BaseViewInterface {
        fun initViews(body: ScannedBody?)
        fun getAmount(): String?
        fun getDescription():String?
        fun closeFragment()
        fun showError()
        fun initUserCard(userCards: ArrayList<CardBody>)
        fun getCardId(): Long?
        fun getBillNumber(): String?
        fun showLoading()
        fun hideLoading()
        fun initMultiStateListener()
        fun startSuccessFragment()
        fun isFromTransaction(): Boolean
        fun getRecipientNumber(): String?
        fun showEnterCardNumber()
        fun hideEnterCardNumber()
        fun setDescription(description: String?)
    }

    interface Presenter : BasePresenterInterface {
        fun viewCreated(body: ScannedBody?)
        fun sendMoney(sendMoneyType: SendMoneyType)
    }
}