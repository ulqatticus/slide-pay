package com.slidepay.clientt.contracts.invoice

import android.content.ContentResolver
import android.content.Intent
import android.view.MotionEvent
import com.slidepay.clientt.contracts.BasePresenterInterface
import com.slidepay.clientt.contracts.BaseViewInterface
import com.slidepay.clientt.model.objects.main.CardBody
import com.slidepay.clientt.model.objects.main.InvoiceBodyResponse

interface InvoiceContract {
    interface View: BaseViewInterface {
        fun setPhone(phone: String)
        fun getContentResolver(): ContentResolver
        fun startSuccessFragment()
        fun initViews()
        fun createInvoice()
        fun closeActivity()
        fun getAmount(): String?
        fun showError()
        fun getExtra()
        fun initUserCard(userCards: ArrayList<CardBody>)
        fun getCardId(): Long?
        fun getDescription(): String?
        fun getPhoneNumber(): String?
        fun showLoading()
        fun hideLoading()
        fun startBillActivity(body: InvoiceBodyResponse)
        fun setDescription(s: String?)
    }

    interface Presenter : BasePresenterInterface {
        fun createSendMoney()
        fun retryBtnClick()
        fun getScannedContent(content: String)
    }
}