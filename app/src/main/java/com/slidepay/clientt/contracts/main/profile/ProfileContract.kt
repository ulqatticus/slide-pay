package com.slidepay.clientt.contracts.main.profile

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import com.slidepay.clientt.contracts.BasePresenterInterface
import com.slidepay.clientt.contracts.BaseViewInterface
import com.slidepay.clientt.model.objects.main.ProfileBody

interface ProfileContract {

    interface View: BaseViewInterface {
        fun setInformation(body: ProfileBody)
        fun startLogoutFragment()
        fun setListenerToLogout()
        fun getActivityContext(): Activity
        fun setImageToProfile(bitmap: Bitmap?)
        fun showLoading()
        fun showKeyboard()
        fun hideKeyboard()
        fun startChangePassFragmnet()
    }

    interface Presenter : BasePresenterInterface {
        fun addImageVia()
        fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
        fun saveChangesToServer(body: ProfileBody)
        fun retryBtnClick()
    }
}