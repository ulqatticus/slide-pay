package com.slidepay.clientt.contracts.main.transaction

import android.support.v7.app.AppCompatActivity
import com.slidepay.clientt.contracts.BasePresenterInterface
import com.slidepay.clientt.contracts.BaseViewInterface
import com.slidepay.clientt.model.objects.main.ScannedBody
import com.slidepay.clientt.model.objects.main.TransactionBody

interface TransactionContract {

    interface View : BaseViewInterface {
        fun initTransactionsView()
        fun addItems(list: ArrayList<TransactionBody>)
        fun showLoading()
        fun scrollToPosition(position: Int)
        fun startPayActivity(payInvoiceBody: ScannedBody)
        fun getTContext(): AppCompatActivity
    }

    interface Presenter : BasePresenterInterface {
        fun updateData()
        fun retryBtnClick()
        fun scrollToPosition(position: Int)
        fun retryInvoice(id: String?)
        fun cancelInvoice(id: String?)
        fun cancelGetMoney(number: String?)
        fun retryGetMoney(number: String?)
    }
}